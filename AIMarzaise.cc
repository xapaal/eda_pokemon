#include "Player.hh"
#include <math.h> 

#include <sys/times.h>
#include <sys/resource.h>
using namespace std;

#define PLAYER_NAME Marzaise

struct PLAYER_NAME : public Player {

const int memory_frames_ilegal_position = 10;


typedef int celda;
typedef vector<celda> fila;
typedef vector<fila> matriz;

typedef pair<Pos, int> P;

int incx[4] = {0, 0, 1, -1};
int incy[4] = {1, -1, 0, 0};

float max_time = 0;



//values based in points
int HEURISTIC_VALUE_STONE = 1000;
int HEURISTIC_VALUE_ATTACK = 500;//for every attack we have it decreases 10%total
int HEURISTIC_VALUE_DEFENSE = 500;//for every defense we have it decreases 10%total
int HEURISTIC_VALUE_SCOPE = 500;//for every scope we have it decreases 10%total

int MAX_ATTACK = 20;
int MAX_DENFENSE = 20;
int MAX_STONE = 20;
int number_stones = 0;

pair<int, int> prohibitedDirection = make_pair(-1, -1); //x, y

Pos pIni;
	  static Player* factory () {
		return new PLAYER_NAME;
	  }

	  virtual void play () {

		const Poquemon& p = poquemon(me());
		pIni = p.pos;

		if (p.alive) {

			bool isDangerWall = false;
			//search if we have some ghost wall near

			int reamingWallPos =  ghost_wall(pIni);

			PRINT("ACTUAL WALL VALUE: "+i2s(reamingWallPos));

			if (reamingWallPos==2) {
				PRINT("DANGEROUS SITUATION: "+i2s(reamingWallPos));
				isDangerWall=true;
				for (int i = 0; i<4; i++){
					Pos near = dest(pIni, num2dir(i));
					PRINT("DIRECTION... "+i2s(i));

					if (ghost_wall(near)==-1 && cell_type(near) != Wall){
						PRINT("IS OKKKK!!");
						isDangerWall = false;

						if (reamingWallPos==1){
							PRINT("LEAVING THIS; PLZ");
							move(num2dir(i));//IF IT IS GOING TO CONVERT TO WALL.. LEAVE!
						}
					}
				}
			}
			if (reamingWallPos==1){//if our position is going to be wall
				for (int i = 0; i<4; i++){
					Pos near = dest(pIni, num2dir(i));
					if (ghost_wall(near)==1){//and it's dangerous to move arround it
						for (int j = 0; j<4; j++){
							Pos near2 = dest(pIni, num2dir(j));
							if (ghost_wall(near2)==-1 && cell_type(near2) != Wall)
								move(num2dir(j));
						}
					}
				}
			}

			decrementPosIlegal();

			int dir = search_item(p, isDangerWall);

			if (ghost_wall(dest(pIni, num2dir(dir))) == 1)
				dir = -2;

			PRINT("DIR VALUE IS: "+i2s(dir));

			if (dir>=0)//>0->ok, move there		-1 -> don't know where to move (rare)	-2 -> I'm attacking (dont do nothing)
				move(num2dir(dir));

			move(None);

		}
		else{
			PRINT("DIED :(");
		}


	}

	Dir rand_dir (Dir notd) {
	  Dir DIRS[] = {Top, Bottom, Left, Right};
	  while (true) {
		Dir d = DIRS[randomize() % 4];
		if (d != notd) return d;
	  }
	}

	int search_item(const Poquemon& p, bool leaveGhostWall){

		Dir DIRS[] = {Top, Bottom, Left, Right};

		matriz dist(rows(), fila(cols(), -1));
		queue<P> Q;
		P ins = P(pIni, -1);
  		Q.push(ins);
  		dist[pIni.i][pIni.j] = 0;
  		int my_number = cell_id(pIni);

  		pair<int, int> selected; //first->heuristic value			second->direction
  		selected.second = -1;

  		int direction_ghost_wall = -1;
  		
  		while(!Q.empty()){
  			P aux_pair = Q.front(); Q.pop(); 
  			Pos aux = aux_pair.first;

  			//here!!!
  			if (leaveGhostWall){
	  			if (ghost_wall(aux)==-1)
	  				return aux_pair.second;
	  		}

  			int idCell = cell_id(aux);

			if ((idCell==0 || idCell==1 || idCell == 2 || idCell == 3) && idCell != my_number){
				PRINT("======ENEMY HERE!!=========");
				PRINT("id cell "+i2s(idCell));
				PRINT(" "+i2s(aux.j)+", "+i2s(aux.i));

				Poquemon p1 = poquemon(idCell);
				PRINT("attack: "+i2s(p1.attack));
				PRINT("defense: "+i2s(p1.defense));
				PRINT("scope: "+i2s(p1.scope));
				PRINT("=========================");

					int res = attack_pokemon(p, p1, aux_pair.second);
					if (res!=-1)
						return res;
			}
  			
	  		int distance=-1;
	  		int value = -1;
	  		distance = dist[aux.i][aux.j];
	  		switch(cell_type(aux)){
	  			case Point:
	  				value = points_value(aux);
	  				//value = 1000;
	  				//return aux_pair.second;

	  				value /= (distance*distance);

	  				PRINT("FOUND POINT ON DIRECTION "+i2s(aux_pair.second)+". H-VALUE: "+i2s(value));
	  				break;
	  			case Stone:

	  				if (number_stones>=max_stone()){ 
	  					PRINT("FOUND STONE =FUCK IT= ON DIRECTION "+i2s(aux_pair.second)+". H-VALUE: "+i2s(value));
	  					break;
	  				}
	  				else{
		  				value = 500;

		  				if (rows()==cols())//if is maze1
		  					value = 10000;

		  				value /= (distance*distance);
		  				PRINT("FOUND STONE ON DIRECTION "+i2s(aux_pair.second)+". H-VALUE: "+i2s(value));
		  			}

	  				break;
	  			case Scope:
	  				value = ((max_scope()-p.scope)*HEURISTIC_VALUE_SCOPE)/max_scope();
	  				
	  				value /= (distance*distance);
	  				PRINT("FOUND SCOPE ON DIRECTION "+i2s(aux_pair.second)+". H-VALUE: "+i2s(value));

	  				break;
	  			case Attack:

			  		value = (((MAX_ATTACK-p.attack)*HEURISTIC_VALUE_ATTACK)/MAX_ATTACK);
			  		value /= (distance*distance);
	  				PRINT("FOUND ATTACK ON DIRECTION "+i2s(aux_pair.second)+". H-VALUE: "+i2s(value));

	  				break;
	  			case Defense:

			  		value = (((MAX_DENFENSE-p.defense)*HEURISTIC_VALUE_DEFENSE)/MAX_DENFENSE);
			  		value /= (distance*distance);
	  				PRINT("FOUND DEFENSE ON DIRECTION "+i2s(aux_pair.second)+". H-VALUE: "+i2s(value));

	  				break;
	  		}
	  		
	  		if (value > 0 && value > selected.first){
	  			bool regist = checkPosIsLegal(aux);
	  			if (regist){
		  			selected.first = value;//heuristic value
		  			selected.second = aux_pair.second;//direction
	  			}
	  		}

  			for (int d = 0; d<4; ++d){//0->right	1->Left		2->Down		3->Up

  				int x = aux.i+incx[d];
  				int y = aux.j+incy[d];

  				if (ghost_wall(Pos(x, y)) && checkPosIsLegal(Pos(x, y)))//if there's no interesting thing, go next to ghost wall
  					direction_ghost_wall = aux_pair.second;

  				if (x>=0 && x<rows() && y>=0 && y<cols() && dist[x][y] == -1){
  					if ((cell_type(Pos(x, y)) == Wall && ghost_wall(Pos(x, y))!=-1 && ghost_wall(Pos(x, y))<=distance+1) ||  //if we have time to go there after it changed from wall
		  				 (cell_type(Pos(x, y)) != Wall && ghost_wall(Pos(x, y))!=-1 && ghost_wall(Pos(x, y))>distance+1) ||  //if we have time to go there before it changed to wall
		  				 (cell_type(Pos(x, y)) != Wall && ghost_wall(Pos(x, y))==-1)){										 //if that position is normal
			  					Pos pos = Pos(x, y);
			  					P insert = P(pos, -1);
			  					if (aux_pair.second==-1)
			  						insert.second = d;
			  					else
			  						insert.second = aux_pair.second;

			  					Q.push(insert);
			  					dist[x][y] = dist[aux.i][aux.j] + 1;
  					}
  				}
  			} 
  		}

  		if (selected.second!=-1){
  			if (cell_type(dest(pIni, num2dir(selected.second))) == Stone){ 
  				number_stones++; PRINT("STONE HERE! STONES TAKEN: "+i2s(number_stones))
  				PRINT(i2s(number_stones)+">="+i2s(max_stone()));}
  			return selected.second;
  		}
		else
			return direction_ghost_wall;

	}

	//function that check if there's was a dangerous enemy between me and the object aux
	bool checkPosIsLegal(Pos aux){
		//return true;
		if (prohibitedDirection.second>0){
	  		if ((aux.j>= prohibitedDirection.second && prohibitedDirection.second >pIni.j)
	  			|| (aux.j<= prohibitedDirection.second && prohibitedDirection.second < pIni.j)){//if the item is under the prohibited col
	  			//PRINT("WE CANT GO TO THE POSITION X "+i2s(aux.i)+", "+i2s(aux.j));
	  			//PRINT("BECAUSE OF THE VALUE "+i2s(prohibitedDirection.first)+", "+i2s(prohibitedDirection.second));
 						return false;
 			}
  		}
  		else if (prohibitedDirection.first>0){
  			if ((aux.i >= prohibitedDirection.first && prohibitedDirection.first > pIni.i)
  				|| (aux.i <= prohibitedDirection.first && prohibitedDirection.first < pIni.i)){
  				//PRINT("WE CANT GO TO THE POSITION Y "+i2s(aux.i)+", "+i2s(aux.j));
  				//PRINT("BECAUSE OF THE VALUE "+i2s(prohibitedDirection.first)+", "+i2s(prohibitedDirection.second));
					return false;
  			}
		}

		return true;
	}

	void decrementPosIlegal(){
		if (prohibitedDirection.first > 0){//
			if (prohibitedDirection.second<0)
				prohibitedDirection.second++;
			if (prohibitedDirection.second==0){
				prohibitedDirection.first = -1;
				prohibitedDirection.second = -1;
			}
		}

		if (prohibitedDirection.second > 0){//
			if (prohibitedDirection.first<0)
				prohibitedDirection.first++;
			if (prohibitedDirection.first==0){
				prohibitedDirection.first = -1;
				prohibitedDirection.second = -1;
			}
		}
	}


	void print_obj(int i, Pos aux){
		return;//comment this for 
		PRINT("type: "+i2s(cell_id(aux)));

		switch(i){
			case 0:
				PRINT("<><><><>POINT<><><><>"); break;
			case 1:
				PRINT("<><><><>STONE<><><><>");	break;
			case 3:
				PRINT("<><><><>SCOPE<><><><>");	break;
			case 4:
				PRINT("<><><><>ATTACK<><><><>"); break;
			case 5:
				PRINT("<><><><>DEFENS<><><><>"); break;
		}

		PRINT(" "+i2s(aux.j)+", "+i2s(aux.i));
		PRINT("<><><><><><><><><><>");
	}

	int attack_pokemon(const Poquemon& me, const Poquemon& enemy, int direction){
		
		//PRINT("inside attack_pokemon");
		//PRINT("scope of mine: "+i2s(me.scope));
		//PRINT("scope of enemy: "+i2s(enemy.scope));
		//PRINT("--------------------------");
		//PRINT("distance in x: "+i2s(abs(me.pos.i-enemy.pos.i)));
		//PRINT("pos x of mine: "+i2s(me.pos.i));
		//PRINT("pos x of enemy: "+i2s(enemy.pos.i));
		//PRINT("--------------------------");
		//PRINT("distance in x: "+i2s(abs(me.pos.j-enemy.pos.j)));
		//PRINT("pos y of mine: "+i2s(me.pos.j));
		//PRINT("pos y of enemy: "+i2s(enemy.pos.j));
		
		int distance_i = abs(me.pos.i-enemy.pos.i);
		int distance_j = abs(me.pos.j-enemy.pos.j);

		if (me.attack >= enemy.defense){						//if i'm better
			if ((me.scope < distance_i && enemy.scope < distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope < distance_j && enemy.scope < distance_j && enemy.pos.i == me.pos.i)){//and we have worse scope
				PRINT("=====ATACKING!!!!!=====I'm stronger. I cant attack. He cant. direction: " + i2s(direction));
				if (enemy.pos.j==me.pos.j){						//FOR VERTICAL
					if (abs(enemy.pos.i-me.pos.i)==(1+me.scope) 
					&& me.attack>enemy.defense && enemy.defense<me.attack)	//i'm going to take proffit of my scope
						return direction;						//go to him!
					else if (abs(enemy.pos.i-me.pos.i)>=(1+me.scope))
						return -1;								//too far, peace!
				}
				if (enemy.pos.i==me.pos.i )						//FOR HORITZONTAL
					if (abs(enemy.pos.j-me.pos.j)==(1+me.scope) 
					&& me.attack>enemy.defense && enemy.defense<me.attack)	//i'm going to take proffit of my scope
						return direction;						//go to him!
					else if (abs(enemy.pos.j-me.pos.j)>=(1+me.scope))
						return -1;								//too far, peace!
			}
			else if ((me.scope >= distance_i && enemy.scope < distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope >= distance_j && enemy.scope < distance_j && enemy.pos.i == me.pos.i)){//and have enought scope...
				PRINT("=====ATACKING!!!!!=====I'm stronger. I can attack. He cant. direction: " + i2s(direction));
				attack(num2dir(direction));
			}
			else if ((me.scope < distance_i && enemy.scope >= distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope < distance_j && enemy.scope >=distance_j && enemy.pos.i == me.pos.i)){//and we have worse scope
				PRINT("=====ATACKING!!!!!=====I'm stronger. I can't attack. He can. direction: " + i2s(direction));
				return leaveEnemy(direction, me, enemy);		//TODO: should we leave?
			}
			else if ((me.scope >= distance_i && enemy.scope >= distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope >= distance_j && enemy.scope >=distance_j && enemy.pos.i == me.pos.i)){//and we have worse scope
				PRINT("=====ATACKING!!!!!=====I'm stronger. I can attack. He can. direction: " + i2s(direction));
				attack(num2dir(direction));						//attack! I will hit!
			}
		}else{	
			if ((me.scope < distance_i && enemy.scope < distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope < distance_j && enemy.scope < distance_j && enemy.pos.i == me.pos.i)){//and we have worse scope
				PRINT("=====ATACKING!!!!!=====He is stronger. I cant attack. He cant. direction: " + i2s(direction));
				/*
				if (enemy.pos.j==me.pos.j)						//FOR VERTICAL
					if (abs(enemy.pos.i-me.pos.i)==(1+enemy.scope))
						leaveEnemy(direction, me, enemy);		//enemy it's pretty far... dont move to him

				if (enemy.pos.i==me.pos.i )						//FOR HORITZONTAL
					if (abs(enemy.pos.j-me.pos.j)==(1+enemy.scope))
						leaveEnemy(direction, me, enemy);		//enemy it's pretty far... dont move to him
				*/
				return -1;										
			}
			else if ((me.scope >= distance_i && enemy.scope < distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope >= distance_j && enemy.scope < distance_j && enemy.pos.i == me.pos.i)){//and have enought scope...
				PRINT("=====ATACKING!!!!!=====He is stronger. I can attack. He cant. direction: " + i2s(direction));
				attack(num2dir(direction));						//attack!
			}
			else if ((me.scope < distance_i && enemy.scope >= distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope < distance_j && enemy.scope >=distance_j && enemy.pos.i == me.pos.i)){//and we have worse scope
				PRINT("=====ATACKING!!!!!=====He is stronger. I can't attack. He can. direction: " + i2s(direction));
				return leaveEnemy(direction, me, enemy);		//LEAVE!!
			}
			else if ((me.scope >= distance_i && enemy.scope >= distance_i && enemy.pos.j == me.pos.j)//if we are in the same direction
			|| (me.scope >= distance_j && enemy.scope >=distance_j && enemy.pos.i == me.pos.i)){//and we have worse scope
				PRINT("=====ATACKING!!!!!=====He is stronger. I can attack. He can. direction: " + i2s(direction));
				return leaveEnemy(direction, me, enemy);		//LEAVE!!
			}
		}
		return -1;
	}

	int leaveEnemy(int num, const Poquemon& me, const Poquemon& enemy){
		PRINT("======we seem to be going with mum=========");

		//setting this column/row as dangerous for 10 frames
		if (num==2 || num==3){
			prohibitedDirection.second=pIni.j;
			prohibitedDirection.first = -memory_frames_ilegal_position;
		}
		else if (num==0 || num==1){
			prohibitedDirection.first=pIni.i;
			prohibitedDirection.second = -memory_frames_ilegal_position;
		}
		
		if (enemy.pos.j == me.pos.j){//it's up or down
			PRINT("let's leave on up down");
			if (cell_type(dest(pIni, num2dir(0))) != Wall) return 0;
			if (cell_type(dest(pIni, num2dir(1))) != Wall) return 1;
			int distance_i = abs(me.pos.i-enemy.pos.i);
			if (distance_i + 1 <= enemy.scope) return invertDirection(num);//if we cannot go to one side... let's go away
			attack(num2dir(num));
		}

		if (enemy.pos.i == me.pos.i){//it's right or left
			PRINT("let's leave on right left");
			if (cell_type(dest(pIni, num2dir(2))) != Wall) return 2;
			if (cell_type(dest(pIni, num2dir(3))) != Wall) return 3;
			int distance_j = abs(me.pos.j-enemy.pos.j);
			if (distance_j + 1 <= enemy.scope) return invertDirection(num);//if we cannot go to one side... let's go away
			attack(num2dir(num));
		}
	}

	int invertDirection(int num){
		switch(num){
			case 0: 
				if (cell_type(dest(pIni, num2dir(1))) != Wall) return 1;
				if (cell_type(dest(pIni, num2dir(2))) != Wall) return 2;
				return 3;
			case 1: 
				if (cell_type(dest(pIni, num2dir(0))) != Wall) return 0;
				if (cell_type(dest(pIni, num2dir(2))) != Wall) return 2;
				return 3;
			case 2: 
				if (cell_type(dest(pIni, num2dir(3))) != Wall) return 3;
				if (cell_type(dest(pIni, num2dir(0))) != Wall) return 0;
				return 1;
			case 3:
				if (cell_type(dest(pIni, num2dir(2))) != Wall) return 2;
				if (cell_type(dest(pIni, num2dir(0))) != Wall) return 0; 
				return 1;
		}
		return -1;
	}


	Dir num2dir(int num){
		switch(num){
			case 0: return Right;
			case 1: return Left;
			case 2: return Bottom;
			case 3: return Top;

		}
		return Right;
	}

	float GetTime(void)        
	{
	  struct timeval tim;
	  struct rusage ru;
	  getrusage(RUSAGE_SELF, &ru);
	  tim=ru.ru_utime;
	  return ((double)tim.tv_sec + (double)tim.tv_usec / 1000000.0)*1000.0;
	}

inline string f2s (float f) {
    ostringstream oss;
    oss << f;
    return oss.str();
}


};



/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);