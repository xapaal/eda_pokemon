#include "Player.hh"
#include <vector>
#include <queue>
#include <list>
#include <iostream>
using namespace std;


#define PLAYER_NAME Omar

struct zelda {
  int d = -1;
  char item = 'R'; //Wall, Stone(Ous), Point, Attack, Scope, Defense, Res
};

struct item_pos {
  int pos_i;
  int pos_j;
  char item;
  Dir caponvaig;
};

struct enemic {
  int pos_i;
  int pos_j;
  int atk;
  int def;
  int la_cope;
};

struct PLAYER_NAME : public Player {

    static Player* factory () {
        return new PLAYER_NAME;
    }
      
    virtual void play () {
      Dir DIRS[] = {Top, Bottom, Left, Right};
      vector<vector<zelda> > mat_wanax (rows(), vector<zelda>(cols()));
      vector<item_pos> ous;
      vector<item_pos> atk;
      vector<item_pos> def;
      vector<item_pos> pasta;
      vector<item_pos> la_cope;
      const Poquemon& p = poquemon(me());
      Pos pos_actual = p.pos;
      int id_enemic;
      Dir on_atac;
      Pos on_es;
      char quinmesaprop;

      if (p.alive) {
        digger(pos_actual, mat_wanax, ous, atk, def, pasta, la_cope, quinmesaprop);
        if (ifeelapresence(pos_actual, id_enemic, on_atac, on_es) && (get_rekeroni(id_enemic, p, on_atac, on_es, pos_actual)) ) attack(on_atac);
        else {
          //Dir quetencaprop = donam_posicio_aprop(quinmesaprop, ous, atk, def, pasta, la_cope);
          Dir quetencaprop = donam_posicio_aprop(quinmesaprop,ous,atk,def,pasta,la_cope);
          if (ghost_wall(dest(pos_actual, quetencaprop)) != 1 ) {
           if (noetdeixisres(pos_actual) == None) move(quetencaprop);
           else move(noetdeixisres(pos_actual));
          }
          else move(None);
        }
      }
    }
  Dir noetdeixisres(Pos pos_actual) {
    Dir DIRS[] = {Top, Bottom, Left, Right};   
    for (int i = 0; i < 4; i++) {
      if (cell_type(dest(pos_actual, DIRS[i])) != Empty && cell_type(dest(pos_actual, DIRS[i])) != Wall) return DIRS[i];
    }
  return None;
}


  Dir donam_posicio_aprop(const char quinmesaprop, const vector<item_pos>& ous, const vector<item_pos>& atk, const vector<item_pos>& def, const vector<item_pos>& pasta, const 
    vector<item_pos>& la_cope) {
    Dir DIRS[] = {Top, Bottom, Left, Right};
    if (quinmesaprop == 'O') return ous[0].caponvaig;
    if (quinmesaprop == 'A') return atk[0].caponvaig;
    if (quinmesaprop == 'D') return def[0].caponvaig;
    if (quinmesaprop == 'P') return pasta[0].caponvaig;
    if (quinmesaprop == 'S') return la_cope[0].caponvaig;
    return None;
  }

  int dif (int a, int b) {
    int res = a - b;
    if (res < 0) return -res;
    else return res;
  }

  bool get_rekeroni(int id_enemic,const Poquemon& p,const  Dir on_atac, const Pos on_es, const Pos pos_actual) {
    Poquemon p_enemic = poquemon(id_enemic);
    if (p_enemic.defense < p.attack && p_enemic.attack < p.defense) {
      if (on_es.i == pos_actual.i && p.scope >= dif(on_es.i, pos_actual.i)) return true;
      if (on_es.j == pos_actual.j && p.scope >= dif(on_es.j, pos_actual.j)) return true;
      return false;
    }
    else return false;
  }


  Dir gimmie_pos(const Dir& p) {
    if (p == Right) return Left;
    else if (p == Left) return Right;
    else if (p == Top) return Bottom;
    else if (p == Bottom) return Top;
    else return None;
  }


  bool ifeelapresence(Pos p, int& enemic, Dir& on_atac, Pos& on_es) {
    //si false -> no puc vore cap poquemon enemic
    //si true -> enemic te l'id del poquemon enemic
    Pos aux = p;
    aux.i = 0;
    for (int i = 0; i < rows(); i++) {
      if (cell_id(aux) != -1 && cell_id(aux) != poquemon(me()).id) {
        /*cout << "-------------------" << endl;
        cout << "He trobat un enemic!" << endl;
        cout << "-------------------" << endl << endl;*/
        enemic = cell_id(aux);
        if (aux.i < p.i) on_atac = Top;
        else on_atac = Bottom;
        on_es.i = aux.i;
        on_es.j = aux.j;
        return true;
      }
      aux.i++;
    }

    aux = p;
    aux.j = 0;
    for (int j = 0; j < cols(); j++) {
      if (cell_id(aux) != -1 && cell_id(aux) != poquemon(me()).id){
        enemic = cell_id(aux);
        /*cout << "-------------------" << endl;
        cout << "He trobat un enemic!" << endl;
        cout << "-------------------" << endl << endl;*/
        if (aux.j < p.j) on_atac = Left;
        else on_atac = Right;
        on_es.i = aux.i;
        on_es.j = aux.j;
        return true;
      }
      aux.j++;
    }
    return false;
  }

  
  void caminet(const vector<vector<zelda> > mat, vector<item_pos>& aux, Pos t) {
    Pos aux2 = t;
    bool fi = false;
    int i = 0, j = 0;
    Dir DIRS[] = {Top, Bottom, Left, Right};
    while (mat[aux2.i][aux2.j].d != 0) {
      for (int i = 0; i < 4 and !fi; i++) {
        Pos aux1 = dest(aux2, DIRS[i]);
        j++;
        if (mat[aux1.i][aux1.j].d == mat[aux2.i][aux2.j].d - 1)  aux2 = aux1;
        if (mat[aux2.i][aux2.j].d == 0)  {
          aux[aux.size()-1].caponvaig = gimmie_pos(DIRS[i]);
          fi = true;
        }
      }

    }
  }
   
   
  void digger (const Pos& pos_actual, vector<vector<zelda> >& mat, vector<item_pos>& ous, vector<item_pos>& atk, vector<item_pos>& def, vector<item_pos>& pasta,
   vector<item_pos>& cope, char& quinmesaprop) {
    
    Dir DIRS[] = {Top, Bottom, Left, Right};
    Pos t;
    queue<Pos> inem;
    inem.push(pos_actual);
    mat[pos_actual.i][pos_actual.j].d = 0;
    item_pos quehetrobat;
    bool primer = false;
    quinmesaprop = 'N';

    while (!inem.empty()) {
        t = inem.front();
        inem.pop();
        if (cell_type(t) == Point) {
          mat[t.i][t.j].item = 'P';
          quehetrobat.item = 'P';
          quehetrobat.pos_i = t.i;
          quehetrobat.pos_j = t.j;
          pasta.push_back(quehetrobat);
          caminet(mat,pasta,t);
          /*cout << "-----------" << endl;
          cout << "mesaprop P" << endl;
          cout << "-----------" << endl;*/
          if (!primer) {
            quinmesaprop = 'P';
            primer = true;
          }
        }
        else if (cell_type(t) == Stone) {
          mat[t.i][t.j].item = 'O';
          quehetrobat.item = 'O';
          quehetrobat.pos_i = t.i;
          quehetrobat.pos_j = t.j;
          ous.push_back(quehetrobat);
          caminet(mat,ous,t);
          /*cout << "-----------" << endl;
          cout << "mesaprop O" << endl;
          cout << "-----------" << endl;*/
          if (!primer) {
            quinmesaprop = 'O';
            //cout << "VAIG A PER OUS" << endl;
            primer = true;
          }
      
        }
        else if (cell_type(t) == Attack) {
          mat[t.i][t.j].item = 'A';
          quehetrobat.item = 'A';
          quehetrobat.pos_i = t.i;
          quehetrobat.pos_j = t.j;
          atk.push_back(quehetrobat);
          caminet(mat,atk,t); 
          if (!primer) {
            quinmesaprop = 'A';
            //cout << "VAIG A PER OUS" << endl;
            primer = true;
          }

        }
        else if (cell_type(t) == Defense) {
          mat[t.i][t.j].item = 'D';
          quehetrobat.item = 'D';
          quehetrobat.pos_i = t.i;
          quehetrobat.pos_j = t.j;
          def.push_back(quehetrobat);
          caminet(mat,def,t); 
          if (!primer) {
            quinmesaprop = 'D';
            //cout << "VAIG A PER UN DEFENSE" << endl;
            primer = true;
          }


        }
        else if (cell_type(t) == Scope) {
          mat[t.i][t.j].item = 'S';
          quehetrobat.item = 'S';
          quehetrobat.pos_i = t.i;
          quehetrobat.pos_j = t.j;
          cope.push_back(quehetrobat);
          caminet(mat,cope,t);
          if (!primer) {
            quinmesaprop = 'S';
            primer = true;
          }

        }
            
        else {
            for (int i = 0; i < 4; i++) {
                Pos aux2 = dest(t, DIRS[i]); 
                if (cell_type(aux2) != Wall and mat[aux2.i][aux2.j].d == -1) {
                    mat[aux2.i][aux2.j].d = mat[t.i][t.j].d + 1;
                    inem.push(aux2);
                }
            }
        }
    }
  }
   
};

RegisterPlayer(PLAYER_NAME);

