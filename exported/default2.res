Game

Seed 1

Poquemon v1

nb_players 4
nb_poquemon 1
nb_rounds 400
nb_ghost_wall 6
nb_point 20
nb_stone 5
nb_scope 2
nb_attack 4
nb_defense 4

player_regen_time 40
wall_change_time 30
point_regen_time 50
stone_regen_time 85
scope_regen_time 65
attack_regen_time 55
defense_regen_time 55
battle_reward 15

max_scope 8
max_stone 2

rows 15
cols 31

names Marzaise Marzaise Marzaise Marzaise


round 0

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  6  1  1  1  0  0  0  a
1  1  2  24  1  1  1  0  0  0  a
2  2  12  6  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m r

actions_done
player action direction
0 m l
1 m r
2 m l
3 m r


round 1

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  1  1  1  0  0  0  a
1  1  2  25  1  1  1  0  0  0  a
2  2  12  5  1  1  1  0  0  0  a
3  3  12  25  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m r

actions_done
player action direction
0 m l
1 m r
2 m l
3 m r


round 2

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  0  0  a
1  1  2  26  1  1  1  0  0  0  a
2  2  12  4  1  1  1  0  0  0  a
3  3  12  26  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m r

actions_done
player action direction
0 m l
1 m r
2 m l
3 m r


round 3

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  3  1  1  1  0  0  0  a
3  3  12  27  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m t
2 m b
3 m b

actions_done
player action direction
0 m t
1 m t
2 m b
3 m b


round 4

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  3  1  1  1  0  500  0  a
1  1  1  27  1  1  1  0  500  0  a
2  2  13  3  1  1  1  0  500  0  a
3  3  13  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  0  49 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  49 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  49 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  49 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m b
2 m t
3 m t

actions_done
player action direction
0 m b
1 m b
2 m t
3 m t


round 5

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  1  1  1  0  500  0  a
1  1  2  27  1  1  1  0  500  0  a
2  2  12  3  1  1  1  0  500  0  a
3  3  12  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  0  48 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  48 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  48 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  48 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m b
2 m t
3 m t

actions_done
player action direction
0 m b
1 m b
2 m t
3 m t


round 6

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  500  0  a
1  1  3  27  1  1  1  0  500  0  a
2  2  11  3  1  1  1  0  500  0  a
3  3  11  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  0  47 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  47 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  47 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  47 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m b
2 m t
3 m t

actions_done
player action direction
0 m b
1 m b
2 m t
3 m t


round 7

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  500  0  a
1  1  4  27  1  1  1  0  500  0  a
2  2  10  3  1  1  1  0  500  0  a
3  3  10  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  0  46 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  46 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  46 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  46 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m b
2 m t
3 m t

actions_done
player action direction
0 m b
1 m b
2 m t
3 m t


round 8

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  500  0  a
1  1  5  27  1  1  1  0  500  0  a
2  2  9  3  1  1  1  0  500  0  a
3  3  9  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  0  45 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  45 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  45 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  45 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m b
2 m t
3 m t

actions_done
player action direction
0 m b
1 m b
2 m t
3 m t


round 9

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  0  a
1  1  6  27  1  1  1  0  500  0  a
2  2  8  3  1  1  1  0  500  0  a
3  3  8  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  0  44 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  44 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  44 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  44 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m b
2 m t
3 m t

actions_done
player action direction
0 u n
1 m b
2 m t
3 u n


round 10

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  0  a
1  1  7  27  1  1  1  0  800  0  a
2  2  7  3  1  1  1  0  800  0  a
3  3  8  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  0  43 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  43 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  49 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  49 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  43 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  43 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 a b
1 a b
2 a t
3 a t

actions_done
player action direction
0 u n
1 a b
2 u n
3 u n


round 11

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  0  a
1  1  7  27  1  1  1  0  875  0  a
2  2  7  3  1  1  1  0  800  0  a
3  3  8  27  1  1  1  0  500  39  d

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  0  42 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  42 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  48 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  48 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  42 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  42 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 a b
1 m l
2 a t
3 u n

actions_done
player action direction
0 u n
1 m l
2 a t
3 u n


round 12

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  39  d
1  1  7  26  1  1  1  0  875  0  a
2  2  7  3  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  38  d

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  0  41 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  41 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  47 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  47 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  41 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  41 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m b
2 m r
3 u n

actions_done
player action direction
0 u n
1 m b
2 m r
3 u n


round 13

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  38  d
1  1  8  26  1  1  1  0  875  0  a
2  2  7  4  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  37  d

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  0  40 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  40 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  46 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  46 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  40 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  40 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m b
2 m b
3 u n

actions_done
player action direction
0 u n
1 m b
2 m b
3 u n


round 14

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  37  d
1  1  9  26  1  1  1  0  875  0  a
2  2  8  4  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  36  d

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  0  39 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  39 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  45 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  45 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  39 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  39 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m b
2 m b
3 u n

actions_done
player action direction
0 u n
1 m b
2 m b
3 u n


round 15

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  36  d
1  1  10  26  1  1  1  0  875  0  a
2  2  9  4  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  35  d

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  0  38 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  38 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  44 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  44 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  38 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  38 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m b
3 u n

actions_done
player action direction
0 u n
1 m l
2 m b
3 u n


round 16

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  35  d
1  1  10  25  1  1  1  0  875  0  a
2  2  10  4  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  34  d

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  0  37 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  37 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  43 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  43 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  37 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  37 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m r
3 u n

actions_done
player action direction
0 u n
1 m l
2 m r
3 u n


round 17

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  34  d
1  1  10  24  1  1  1  0  875  0  a
2  2  10  5  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  33  d

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  0  36 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  36 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  42 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  42 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  36 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  36 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m r
3 u n

actions_done
player action direction
0 u n
1 m l
2 m r
3 u n


round 18

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  33  d
1  1  10  23  1  1  1  0  875  0  a
2  2  10  6  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  32  d

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  0  35 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  35 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  41 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  41 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  35 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  35 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m r
3 u n

actions_done
player action direction
0 u n
1 m l
2 m r
3 u n


round 19

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  32  d
1  1  10  22  1  1  1  0  875  0  a
2  2  10  7  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  31  d

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  0  34 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  34 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  40 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  40 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  0  34 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  34 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m t
2 m r
3 u n

actions_done
player action direction
0 u n
1 m t
2 m r
3 u n


round 20

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  31  d
1  1  9  22  1  1  1  0  975  0  a
2  2  10  8  1  1  1  0  875  0  a
3  3  8  27  1  1  1  0  500  30  d

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  0  33 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  33 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  39 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  39 0
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  0  49 0
P    10  29  500  0 1
P    13  3  0  33 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  33 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m t
2 m t
3 u n

actions_done
player action direction
0 u n
1 m t
2 m t
3 u n


round 21

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  30  d
1  1  8  22  1  1  1  0  975  0  a
2  2  9  8  1  1  1  0  975  0  a
3  3  8  27  1  1  1  0  500  29  d

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  0  32 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  32 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  38 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  38 0
P    9  8  0  49 0
P    9  15  300  0 1
P    9  22  0  48 0
P    10  29  500  0 1
P    13  3  0  32 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  32 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m r
2 m t
3 u n

actions_done
player action direction
0 u n
1 m r
2 m t
3 u n


round 22

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  29  d
1  1  8  23  1  1  1  0  975  0  a
2  2  8  8  1  1  1  0  975  0  a
3  3  8  27  1  1  1  0  500  28  d

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  0  31 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  31 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  37 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  37 0
P    9  8  0  48 0
P    9  15  300  0 1
P    9  22  0  47 0
P    10  29  500  0 1
P    13  3  0  31 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  31 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 m r
2 m r
3 u n


round 23

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA...DX.X.X.X.XD....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  28  d
1  1  8  24  2  1  1  0  975  0  a
2  2  8  9  1  1  1  0  975  0  a
3  3  8  27  1  1  1  0  500  27  d

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  0  30 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  30 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  36 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  36 0
P    9  8  0  47 0
P    9  15  300  0 1
P    9  22  0  46 0
P    10  29  500  0 1
P    13  3  0  30 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  30 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 54 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m r
3 u n

actions_done
player action direction
0 u n
1 m l
2 m r
3 u n


round 24

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA....X.X.X.X.XD....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  27  d
1  1  8  23  2  1  1  0  975  0  a
2  2  8  10  1  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  26  d

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  0  29 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  29 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  35 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  35 0
P    9  8  0  46 0
P    9  15  300  0 1
P    9  22  0  45 0
P    10  29  500  0 1
P    13  3  0  29 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  29 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 53 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 54 0
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m l
3 u n

actions_done
player action direction
0 u n
1 m l
2 m l
3 u n


round 25

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA....X.X.X.X.XD....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  26  d
1  1  8  22  2  1  1  0  975  0  a
2  2  8  9  1  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  25  d

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  0  28 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  28 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  34 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  34 0
P    9  8  0  45 0
P    9  15  300  0 1
P    9  22  0  44 0
P    10  29  500  0 1
P    13  3  0  28 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  28 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 52 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 53 0
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m l
3 u n

actions_done
player action direction
0 u n
1 m l
2 m l
3 u n


round 26

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA....X.X.X.X.XD....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  25  d
1  1  8  21  2  1  1  0  975  0  a
2  2  8  8  1  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  24  d

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  0  27 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  27 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  33 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  33 0
P    9  8  0  44 0
P    9  15  300  0 1
P    9  22  0  43 0
P    10  29  500  0 1
P    13  3  0  27 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  27 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 51 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 52 0
D    8  20 0 0 1

actions_asked
player action direction
0 u n
1 m l
2 m l
3 u n

actions_done
player action direction
0 u n
1 m l
2 m l
3 u n


round 27

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..XA....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  24  d
1  1  8  20  2  2  1  0  975  0  a
2  2  8  7  1  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  23  d

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  0  26 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  26 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  32 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  32 0
P    9  8  0  43 0
P    9  15  300  0 1
P    9  22  0  42 0
P    10  29  500  0 1
P    13  3  0  26 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  26 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 50 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 51 0
D    8  20 0 54 0

actions_asked
player action direction
0 u n
1 m r
2 m l
3 u n

actions_done
player action direction
0 u n
1 m r
2 m l
3 u n


round 28

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  23  d
1  1  8  21  2  2  1  0  975  0  a
2  2  8  6  2  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  22  d

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  0  25 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  25 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  31 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  31 0
P    9  8  0  42 0
P    9  15  300  0 1
P    9  22  0  41 0
P    10  29  500  0 1
P    13  3  0  25 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  25 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 54 0
A    8  24 0 49 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 50 0
D    8  20 0 53 0

actions_asked
player action direction
0 u n
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 m r
2 m r
3 u n


round 29

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  22  d
1  1  8  22  2  2  1  0  975  0  a
2  2  8  7  2  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  21  d

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  0  24 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  24 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  30 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  30 0
P    9  8  0  41 0
P    9  15  300  0 1
P    9  22  0  40 0
P    10  29  500  0 1
P    13  3  0  24 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  24 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 53 0
A    8  24 0 48 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 49 0
D    8  20 0 52 0

actions_asked
player action direction
0 u n
1 m b
2 m r
3 u n

actions_done
player action direction
0 u n
1 m b
2 m r
3 u n


round 30

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  21  d
1  1  9  22  2  2  1  0  975  0  a
2  2  8  8  2  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  20  d

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  0  23 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  23 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  29 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  29 0
P    9  8  0  40 0
P    9  15  300  0 1
P    9  22  0  39 0
P    10  29  500  0 1
P    13  3  0  23 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  23 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 52 0
A    8  24 0 47 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 48 0
D    8  20 0 51 0

actions_asked
player action direction
0 u n
1 m t
2 m t
3 u n

actions_done
player action direction
0 u n
1 m t
2 m t
3 u n


round 31

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  20  d
1  1  8  22  2  2  1  0  975  0  a
2  2  7  8  2  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  19  d

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  0  22 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  22 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  28 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  28 0
P    9  8  0  39 0
P    9  15  300  0 1
P    9  22  0  38 0
P    10  29  500  0 1
P    13  3  0  22 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  22 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 51 0
A    8  24 0 46 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 47 0
D    8  20 0 50 0

actions_asked
player action direction
0 u n
1 m t
2 m t
3 u n

actions_done
player action direction
0 u n
1 m t
2 m t
3 u n


round 32

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  19  d
1  1  7  22  2  2  1  0  975  0  a
2  2  6  8  2  2  1  0  975  0  a
3  3  8  27  1  1  1  0  500  18  d

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  0  21 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  21 0
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  27 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  27 0
P    9  8  0  38 0
P    9  15  300  0 1
P    9  22  0  37 0
P    10  29  500  0 1
P    13  3  0  21 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  21 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 50 0
A    8  24 0 45 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 46 0
D    8  20 0 49 0

actions_asked
player action direction
0 u n
1 m t
2 m t
3 u n

actions_done
player action direction
0 u n
1 m t
2 m t
3 u n


round 33

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  18  d
1  1  6  22  2  2  1  0  975  0  a
2  2  5  8  2  2  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  17  d

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  0  20 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  20 0
P    4  1  500  0 1
P    5  8  0  49 0
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  26 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  26 0
P    9  8  0  37 0
P    9  15  300  0 1
P    9  22  0  36 0
P    10  29  500  0 1
P    13  3  0  20 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  20 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 49 0
A    8  24 0 44 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 45 0
D    8  20 0 48 0

actions_asked
player action direction
0 u n
1 m t
2 m b
3 u n

actions_done
player action direction
0 u n
1 m t
2 m b
3 u n


round 34

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  17  d
1  1  5  22  2  2  1  0  1075  0  a
2  2  6  8  2  2  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  16  d

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  0  19 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  19 0
P    4  1  500  0 1
P    5  8  0  48 0
P    5  15  300  0 1
P    5  22  0  49 0
P    7  3  0  25 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  25 0
P    9  8  0  36 0
P    9  15  300  0 1
P    9  22  0  35 0
P    10  29  500  0 1
P    13  3  0  19 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  19 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 48 0
A    8  24 0 43 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 44 0
D    8  20 0 47 0

actions_asked
player action direction
0 u n
1 m b
2 m r
3 u n

actions_done
player action direction
0 u n
1 m b
2 m r
3 u n


round 35

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  16  d
1  1  6  22  2  2  1  0  1075  0  a
2  2  6  9  2  2  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  15  d

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  0  18 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  18 0
P    4  1  500  0 1
P    5  8  0  47 0
P    5  15  300  0 1
P    5  22  0  48 0
P    7  3  0  24 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  24 0
P    9  8  0  35 0
P    9  15  300  0 1
P    9  22  0  34 0
P    10  29  500  0 1
P    13  3  0  18 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  18 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 47 0
A    8  24 0 42 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 43 0
D    8  20 0 46 0

actions_asked
player action direction
0 u n
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 m r
2 m r
3 u n


round 36

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  15  d
1  1  6  23  2  2  1  0  1075  0  a
2  2  6  10  3  2  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  14  d

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  0  17 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  17 0
P    4  1  500  0 1
P    5  8  0  46 0
P    5  15  300  0 1
P    5  22  0  47 0
P    7  3  0  23 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  23 0
P    9  8  0  34 0
P    9  15  300  0 1
P    9  22  0  33 0
P    10  29  500  0 1
P    13  3  0  17 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  17 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 54 0
A    6  20 0 0 1
A    8  6 0 46 0
A    8  24 0 41 0
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 42 0
D    8  20 0 45 0

actions_asked
player action direction
0 u n
1 m r
2 m l
3 u n

actions_done
player action direction
0 u n
1 m r
2 m l
3 u n


round 37

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..XD....X.X.X.X.XA....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  14  d
1  1  6  24  2  3  1  0  1075  0  a
2  2  6  9  3  2  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  13  d

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  0  16 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  16 0
P    4  1  500  0 1
P    5  8  0  45 0
P    5  15  300  0 1
P    5  22  0  46 0
P    7  3  0  22 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  22 0
P    9  8  0  33 0
P    9  15  300  0 1
P    9  22  0  32 0
P    10  29  500  0 1
P    13  3  0  16 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  16 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 53 0
A    6  20 0 0 1
A    8  6 0 45 0
A    8  24 0 40 0
D    6  6 0 0 1
D    6  24 0 54 0
D    8  10 0 41 0
D    8  20 0 44 0

actions_asked
player action direction
0 u n
1 m l
2 m l
3 u n

actions_done
player action direction
0 u n
1 m l
2 m l
3 u n


round 38

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..XD....X.X.X.X.XA....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  13  d
1  1  6  23  2  3  1  0  1075  0  a
2  2  6  8  3  2  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  12  d

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  0  15 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  15 0
P    4  1  500  0 1
P    5  8  0  44 0
P    5  15  300  0 1
P    5  22  0  45 0
P    7  3  0  21 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  21 0
P    9  8  0  32 0
P    9  15  300  0 1
P    9  22  0  31 0
P    10  29  500  0 1
P    13  3  0  15 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  15 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 52 0
A    6  20 0 0 1
A    8  6 0 44 0
A    8  24 0 39 0
D    6  6 0 0 1
D    6  24 0 53 0
D    8  10 0 40 0
D    8  20 0 43 0

actions_asked
player action direction
0 u n
1 m l
2 m l
3 u n

actions_done
player action direction
0 u n
1 m l
2 m l
3 u n


round 39

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..XD....X.X.X.X.XA....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  12  d
1  1  6  22  2  3  1  0  1075  0  a
2  2  6  7  3  2  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  11  d

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  0  14 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  14 0
P    4  1  500  0 1
P    5  8  0  43 0
P    5  15  300  0 1
P    5  22  0  44 0
P    7  3  0  20 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  20 0
P    9  8  0  31 0
P    9  15  300  0 1
P    9  22  0  30 0
P    10  29  500  0 1
P    13  3  0  14 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  14 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 51 0
A    6  20 0 0 1
A    8  6 0 43 0
A    8  24 0 38 0
D    6  6 0 0 1
D    6  24 0 52 0
D    8  10 0 39 0
D    8  20 0 42 0

actions_asked
player action direction
0 u n
1 m l
2 m l
3 u n

actions_done
player action direction
0 u n
1 m l
2 m l
3 u n


round 40

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.XA....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  11  d
1  1  6  21  2  3  1  0  1075  0  a
2  2  6  6  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  10  d

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  0  13 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  13 0
P    4  1  500  0 1
P    5  8  0  42 0
P    5  15  300  0 1
P    5  22  0  43 0
P    7  3  0  19 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  19 0
P    9  8  0  30 0
P    9  15  300  0 1
P    9  22  0  29 0
P    10  29  500  0 1
P    13  3  0  13 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  13 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 50 0
A    6  20 0 0 1
A    8  6 0 42 0
A    8  24 0 37 0
D    6  6 0 54 0
D    6  24 0 51 0
D    8  10 0 38 0
D    8  20 0 41 0

actions_asked
player action direction
0 u n
1 m l
2 m r
3 u n

actions_done
player action direction
0 u n
1 m l
2 m r
3 u n


round 41

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  10  d
1  1  6  20  3  3  1  0  1075  0  a
2  2  6  7  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  9  d

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  3  0  12 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  12 0
P    4  1  500  0 1
P    5  8  0  41 0
P    5  15  300  0 1
P    5  22  0  42 0
P    7  3  0  18 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  18 0
P    9  8  0  29 0
P    9  15  300  0 1
P    9  22  0  28 0
P    10  29  500  0 1
P    13  3  0  12 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  12 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 49 0
A    6  20 0 54 0
A    8  6 0 41 0
A    8  24 0 36 0
D    6  6 0 53 0
D    6  24 0 50 0
D    8  10 0 37 0
D    8  20 0 40 0

actions_asked
player action direction
0 u n
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 m r
2 m r
3 u n


round 42

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  9  d
1  1  6  21  3  3  1  0  1075  0  a
2  2  6  8  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  8  d

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  3  0  11 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  11 0
P    4  1  500  0 1
P    5  8  0  40 0
P    5  15  300  0 1
P    5  22  0  41 0
P    7  3  0  17 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  17 0
P    9  8  0  28 0
P    9  15  300  0 1
P    9  22  0  27 0
P    10  29  500  0 1
P    13  3  0  11 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  11 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 48 0
A    6  20 0 53 0
A    8  6 0 40 0
A    8  24 0 35 0
D    6  6 0 52 0
D    6  24 0 49 0
D    8  10 0 36 0
D    8  20 0 39 0

actions_asked
player action direction
0 u n
1 m r
2 m t
3 u n

actions_done
player action direction
0 u n
1 m r
2 m t
3 u n


round 43

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  8  d
1  1  6  22  3  3  1  0  1075  0  a
2  2  5  8  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  7  d

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  3  0  10 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  10 0
P    4  1  500  0 1
P    5  8  0  39 0
P    5  15  300  0 1
P    5  22  0  40 0
P    7  3  0  16 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  16 0
P    9  8  0  27 0
P    9  15  300  0 1
P    9  22  0  26 0
P    10  29  500  0 1
P    13  3  0  10 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  10 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 47 0
A    6  20 0 52 0
A    8  6 0 39 0
A    8  24 0 34 0
D    6  6 0 51 0
D    6  24 0 48 0
D    8  10 0 35 0
D    8  20 0 38 0

actions_asked
player action direction
0 u n
1 m t
2 m t
3 u n

actions_done
player action direction
0 u n
1 m t
2 m t
3 u n


round 44

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  7  d
1  1  5  22  3  3  1  0  1075  0  a
2  2  4  8  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  6  d

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  3  0  9 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  9 0
P    4  1  500  0 1
P    5  8  0  38 0
P    5  15  300  0 1
P    5  22  0  39 0
P    7  3  0  15 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  15 0
P    9  8  0  26 0
P    9  15  300  0 1
P    9  22  0  25 0
P    10  29  500  0 1
P    13  3  0  9 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  9 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 46 0
A    6  20 0 51 0
A    8  6 0 38 0
A    8  24 0 33 0
D    6  6 0 50 0
D    6  24 0 47 0
D    8  10 0 34 0
D    8  20 0 37 0

actions_asked
player action direction
0 u n
1 m t
2 m r
3 u n

actions_done
player action direction
0 u n
1 m t
2 m r
3 u n


round 45

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  6  d
1  1  4  22  3  3  1  0  1075  0  a
2  2  4  9  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  5  d

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  3  0  8 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  8 0
P    4  1  500  0 1
P    5  8  0  37 0
P    5  15  300  0 1
P    5  22  0  38 0
P    7  3  0  14 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  14 0
P    9  8  0  25 0
P    9  15  300  0 1
P    9  22  0  24 0
P    10  29  500  0 1
P    13  3  0  8 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  8 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 45 0
A    6  20 0 50 0
A    8  6 0 37 0
A    8  24 0 32 0
D    6  6 0 49 0
D    6  24 0 46 0
D    8  10 0 33 0
D    8  20 0 36 0

actions_asked
player action direction
0 u n
1 m l
2 m t
3 u n

actions_done
player action direction
0 u n
1 m l
2 m t
3 u n


round 46

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  5  d
1  1  4  21  3  3  1  0  1075  0  a
2  2  3  9  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  4  d

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  3  0  7 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  7 0
P    4  1  500  0 1
P    5  8  0  36 0
P    5  15  300  0 1
P    5  22  0  37 0
P    7  3  0  13 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  13 0
P    9  8  0  24 0
P    9  15  300  0 1
P    9  22  0  23 0
P    10  29  500  0 1
P    13  3  0  7 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  7 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 44 0
A    6  20 0 49 0
A    8  6 0 36 0
A    8  24 0 31 0
D    6  6 0 48 0
D    6  24 0 45 0
D    8  10 0 32 0
D    8  20 0 35 0

actions_asked
player action direction
0 u n
1 m t
2 m t
3 u n

actions_done
player action direction
0 u n
1 m t
2 m t
3 u n


round 47

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  4  d
1  1  3  21  3  3  1  0  1075  0  a
2  2  2  9  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  3  d

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  3  0  6 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  6 0
P    4  1  500  0 1
P    5  8  0  35 0
P    5  15  300  0 1
P    5  22  0  36 0
P    7  3  0  12 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  12 0
P    9  8  0  23 0
P    9  15  300  0 1
P    9  22  0  22 0
P    10  29  500  0 1
P    13  3  0  6 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  6 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 43 0
A    6  20 0 48 0
A    8  6 0 35 0
A    8  24 0 30 0
D    6  6 0 47 0
D    6  24 0 44 0
D    8  10 0 31 0
D    8  20 0 34 0

actions_asked
player action direction
0 u n
1 m t
2 m t
3 u n

actions_done
player action direction
0 u n
1 m t
2 m t
3 u n


round 48

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  3  d
1  1  2  21  3  3  1  0  1075  0  a
2  2  1  9  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  2  d

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  3  0  5 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  5 0
P    4  1  500  0 1
P    5  8  0  34 0
P    5  15  300  0 1
P    5  22  0  35 0
P    7  3  0  11 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  11 0
P    9  8  0  22 0
P    9  15  300  0 1
P    9  22  0  21 0
P    10  29  500  0 1
P    13  3  0  5 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  5 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 42 0
A    6  20 0 47 0
A    8  6 0 34 0
A    8  24 0 29 0
D    6  6 0 46 0
D    6  24 0 43 0
D    8  10 0 30 0
D    8  20 0 33 0

actions_asked
player action direction
0 u n
1 m t
2 m r
3 u n

actions_done
player action direction
0 u n
1 m t
2 m r
3 u n


round 49

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX........P.......P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  2  d
1  1  1  21  3  3  1  0  1075  0  a
2  2  1  10  3  3  1  0  1075  0  a
3  3  8  27  1  1  1  0  500  1  d

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  3  0  4 0
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  0  4 0
P    4  1  500  0 1
P    5  8  0  33 0
P    5  15  300  0 1
P    5  22  0  34 0
P    7  3  0  10 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  10 0
P    9  8  0  21 0
P    9  15  300  0 1
P    9  22  0  20 0
P    10  29  500  0 1
P    13  3  0  4 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  4 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 41 0
A    6  20 0 46 0
A    8  6 0 33 0
A    8  24 0 28 0
D    6  6 0 45 0
D    6  24 0 42 0
D    8  10 0 29 0
D    8  20 0 32 0

actions_asked
player action direction
0 u n
1 m l
2 m r
3 u n

actions_done
player action direction
0 u n
1 m l
2 m r
3 u n


round 50

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX................P........XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  500  1  d
1  1  1  20  3  3  1  0  1075  0  a
2  2  1  11  3  3  1  0  1275  0  a
3  3  10  26  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  3  0  3 0
P    1  11  0  49 0
P    1  19  200  0 1
P    1  27  0  3 0
P    4  1  500  0 1
P    5  8  0  32 0
P    5  15  300  0 1
P    5  22  0  33 0
P    7  3  0  9 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  9 0
P    9  8  0  20 0
P    9  15  300  0 1
P    9  22  0  19 0
P    10  29  500  0 1
P    13  3  0  3 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  3 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 40 0
A    6  20 0 45 0
A    8  6 0 32 0
A    8  24 0 27 0
D    6  6 0 44 0
D    6  24 0 41 0
D    8  10 0 28 0
D    8  20 0 31 0

actions_asked
player action direction
0 u n
1 m l
2 m l
3 m r

actions_done
player action direction
0 u n
1 m l
2 m l
3 m r


round 51

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  18  1  1  1  0  500  0  a
1  1  1  19  3  3  1  0  1275  0  a
2  2  1  10  3  3  1  0  1275  0  a
3  3  10  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  3  0  2 0
P    1  11  0  48 0
P    1  19  0  49 0
P    1  27  0  2 0
P    4  1  500  0 1
P    5  8  0  31 0
P    5  15  300  0 1
P    5  22  0  32 0
P    7  3  0  8 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  8 0
P    9  8  0  19 0
P    9  15  300  0 1
P    9  22  0  18 0
P    10  29  500  0 1
P    13  3  0  2 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  2 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 39 0
A    6  20 0 44 0
A    8  6 0 31 0
A    8  24 0 26 0
D    6  6 0 43 0
D    6  24 0 40 0
D    8  10 0 27 0
D    8  20 0 30 0

actions_asked
player action direction
0 m b
1 m r
2 m l
3 m t

actions_done
player action direction
0 m b
1 m r
2 m l
3 m t


round 52

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  18  1  1  1  0  500  0  a
1  1  1  20  3  3  1  0  1275  0  a
2  2  1  9  3  3  1  0  1275  0  a
3  3  9  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    1  3  0  1 0
P    1  11  0  47 0
P    1  19  0  48 0
P    1  27  0  1 0
P    4  1  500  0 1
P    5  8  0  30 0
P    5  15  300  0 1
P    5  22  0  31 0
P    7  3  0  7 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  7 0
P    9  8  0  18 0
P    9  15  300  0 1
P    9  22  0  17 0
P    10  29  500  0 1
P    13  3  0  1 0
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  0  1 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 38 0
A    6  20 0 43 0
A    8  6 0 30 0
A    8  24 0 25 0
D    6  6 0 42 0
D    6  24 0 39 0
D    8  10 0 26 0
D    8  20 0 29 0

actions_asked
player action direction
0 m b
1 m r
2 m b
3 m t

actions_done
player action direction
0 m b
1 m r
2 m b
3 m t


round 53

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......XPX
11 X.X.......X..XXMXXP.X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  18  1  1  1  0  500  0  a
1  1  1  21  3  3  1  0  1275  0  a
2  2  2  9  3  3  1  0  1275  0  a
3  3  8  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  46 0
P    1  19  0  47 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  29 0
P    5  15  300  0 1
P    5  22  0  30 0
P    7  3  0  6 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  0  6 0
P    9  8  0  17 0
P    9  15  300  0 1
P    9  22  0  16 0
P    10  29  500  0 1
P    11  18  400  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 37 0
A    6  20 0 42 0
A    8  6 0 29 0
A    8  24 0 24 0
D    6  6 0 41 0
D    6  24 0 38 0
D    8  10 0 25 0
D    8  20 0 28 0

actions_asked
player action direction
0 m b
1 m b
2 m b
3 m t

actions_done
player action direction
0 m b
1 m b
2 m b
3 m t


round 54

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......XPX
11 X.X.......X..XXMXXP.X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  18  1  1  1  0  900  0  a
1  1  2  21  3  3  1  0  1275  0  a
2  2  3  9  3  3  1  0  1275  0  a
3  3  7  27  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  45 0
P    1  19  0  46 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  28 0
P    5  15  300  0 1
P    5  22  0  29 0
P    7  3  0  5 0
P    7  12  400  0 1
P    7  18  0  49 0
P    7  27  0  5 0
P    9  8  0  16 0
P    9  15  300  0 1
P    9  22  0  15 0
P    10  29  500  0 1
P    11  18  400  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 36 0
A    6  20 0 41 0
A    8  6 0 28 0
A    8  24 0 23 0
D    6  6 0 40 0
D    6  24 0 37 0
D    8  10 0 24 0
D    8  20 0 27 0

actions_asked
player action direction
0 m b
1 m r
2 m b
3 m r

actions_done
player action direction
0 m b
1 m r
2 m b
3 m r


round 55

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......XPX
11 X.X.......X..XXMXXP.X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  18  1  1  1  0  900  0  a
1  1  2  22  3  3  1  0  1275  0  a
2  2  4  9  3  3  1  0  1275  0  a
3  3  7  28  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  44 0
P    1  19  0  45 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  27 0
P    5  15  300  0 1
P    5  22  0  28 0
P    7  3  0  4 0
P    7  12  400  0 1
P    7  18  0  48 0
P    7  27  0  4 0
P    9  8  0  15 0
P    9  15  300  0 1
P    9  22  0  14 0
P    10  29  500  0 1
P    11  18  400  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 35 0
A    6  20 0 40 0
A    8  6 0 27 0
A    8  24 0 22 0
D    6  6 0 39 0
D    6  24 0 36 0
D    8  10 0 23 0
D    8  20 0 26 0

actions_asked
player action direction
0 m b
1 m b
2 m r
3 m r

actions_done
player action direction
0 m b
1 m b
2 m r
3 m r


round 56

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......XPX
11 X.X.......X..XXMXXP.X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  9  18  1  1  1  0  900  0  a
1  1  3  22  3  3  1  0  1275  0  a
2  2  4  10  3  3  1  0  1275  0  a
3  3  7  29  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  43 0
P    1  19  0  44 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  26 0
P    5  15  300  0 1
P    5  22  0  27 0
P    7  3  0  3 0
P    7  12  400  0 1
P    7  18  0  47 0
P    7  27  0  3 0
P    9  8  0  14 0
P    9  15  300  0 1
P    9  22  0  13 0
P    10  29  500  0 1
P    11  18  400  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 34 0
A    6  20 0 39 0
A    8  6 0 26 0
A    8  24 0 21 0
D    6  6 0 38 0
D    6  24 0 35 0
D    8  10 0 22 0
D    8  20 0 25 0

actions_asked
player action direction
0 m b
1 m b
2 m r
3 m b

actions_done
player action direction
0 m b
1 m b
2 m r
3 m b


round 57

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......XPX
11 X.X.......X..XXMXXP.X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  18  1  1  1  0  900  0  a
1  1  4  22  3  3  1  0  1275  0  a
2  2  4  11  3  3  1  0  1275  0  a
3  3  8  29  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  42 0
P    1  19  0  43 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  25 0
P    5  15  300  0 1
P    5  22  0  26 0
P    7  3  0  2 0
P    7  12  400  0 1
P    7  18  0  46 0
P    7  27  0  2 0
P    9  8  0  13 0
P    9  15  300  0 1
P    9  22  0  12 0
P    10  29  500  0 1
P    11  18  400  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 33 0
A    6  20 0 38 0
A    8  6 0 25 0
A    8  24 0 20 0
D    6  6 0 37 0
D    6  24 0 34 0
D    8  10 0 21 0
D    8  20 0 24 0

actions_asked
player action direction
0 m b
1 m b
2 m r
3 m b

actions_done
player action direction
0 m b
1 m b
2 m r
3 m b


round 58

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX........P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  18  1  1  1  0  1300  0  a
1  1  5  22  3  3  1  0  1275  0  a
2  2  4  12  3  3  1  0  1275  0  a
3  3  9  29  1  1  1  0  500  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  41 0
P    1  19  0  42 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  24 0
P    5  15  300  0 1
P    5  22  0  25 0
P    7  3  0  1 0
P    7  12  400  0 1
P    7  18  0  45 0
P    7  27  0  1 0
P    9  8  0  12 0
P    9  15  300  0 1
P    9  22  0  11 0
P    10  29  500  0 1
P    11  18  0  49 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 32 0
A    6  20 0 37 0
A    8  6 0 24 0
A    8  24 0 19 0
D    6  6 0 36 0
D    6  24 0 33 0
D    8  10 0 20 0
D    8  20 0 23 0

actions_asked
player action direction
0 m r
1 m b
2 m b
3 m b

actions_done
player action direction
0 m r
1 m b
2 m b
3 m b


round 59

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  19  1  1  1  0  1300  0  a
1  1  6  22  3  3  1  0  1275  0  a
2  2  5  12  3  3  1  0  1275  0  a
3  3  10  29  1  1  1  0  1000  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  40 0
P    1  19  0  41 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  23 0
P    5  15  300  0 1
P    5  22  0  24 0
P    4  27  100  0 1
P    7  12  400  0 1
P    7  18  0  44 0
P    13  6  300  0 1
P    9  8  0  11 0
P    9  15  300  0 1
P    9  22  0  10 0
P    10  29  0  49 0
P    11  18  0  48 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 31 0
A    6  20 0 36 0
A    8  6 0 23 0
A    8  24 0 18 0
D    6  6 0 35 0
D    6  24 0 32 0
D    8  10 0 19 0
D    8  20 0 22 0

actions_asked
player action direction
0 m t
1 m b
2 m b
3 m b

actions_done
player action direction
0 m t
1 m b
2 m b
3 m b


round 60

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXXPX.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  19  1  1  1  0  1300  0  a
1  1  7  22  3  3  1  0  1275  39  d
2  2  6  12  3  3  1  0  1275  0  a
3  3  11  29  1  1  1  0  1000  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  39 0
P    1  19  0  40 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  22 0
P    5  15  300  0 1
P    5  22  0  23 0
P    4  27  100  0 1
P    7  12  400  0 1
P    7  18  0  43 0
P    13  6  300  0 1
P    9  8  0  10 0
P    9  15  300  0 1
P    9  22  0  9 0
P    10  29  0  48 0
P    11  18  0  47 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 30 0
A    6  20 0 35 0
A    8  6 0 22 0
A    8  24 0 17 0
D    6  6 0 34 0
D    6  24 0 31 0
D    8  10 0 18 0
D    8  20 0 21 0

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m b

actions_done
player action direction
0 m r
1 u n
2 m b
3 m b


round 61

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P.......P......X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  20  1  1  1  0  1300  0  a
1  1  7  22  3  3  1  0  1275  38  d
2  2  7  12  3  3  1  0  1675  0  a
3  3  12  29  1  1  1  0  1000  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  38 0
P    1  19  0  39 0
P    10  21  500  0 1
P    4  1  500  0 1
P    5  8  0  21 0
P    5  15  300  0 1
P    5  22  0  22 0
P    4  27  100  0 1
P    7  12  0  49 0
P    7  18  0  42 0
P    13  6  300  0 1
P    9  8  0  9 0
P    9  15  300  0 1
P    9  22  0  8 0
P    10  29  0  47 0
P    11  18  0  46 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 29 0
A    6  20 0 34 0
A    8  6 0 21 0
A    8  24 0 16 0
D    6  6 0 33 0
D    6  24 0 30 0
D    8  10 0 17 0
D    8  20 0 20 0

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m b

actions_done
player action direction
0 m r
1 u n
2 m b
3 m b


round 62

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P..............X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  21  1  1  1  0  1800  0  a
1  1  7  22  3  3  1  0  1275  37  d
2  2  8  12  3  3  1  0  1675  0  a
3  3  13  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  37 0
P    1  19  0  38 0
P    10  21  0  49 0
P    4  1  500  0 1
P    5  8  0  20 0
P    5  15  300  0 1
P    5  22  0  21 0
P    4  27  100  0 1
P    7  12  0  48 0
P    7  18  0  41 0
P    13  6  300  0 1
P    9  8  0  8 0
P    9  15  300  0 1
P    9  22  0  7 0
P    10  29  0  46 0
P    11  18  0  45 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 84 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 28 0
A    6  20 0 33 0
A    8  6 0 20 0
A    8  24 0 15 0
D    6  6 0 32 0
D    6  24 0 29 0
D    8  10 0 16 0
D    8  20 0 19 0

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m t

actions_done
player action direction
0 m r
1 u n
2 m b
3 m t


round 63

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P..............X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  22  1  1  1  0  1800  0  a
1  1  7  22  3  3  1  0  1275  36  d
2  2  9  12  3  3  1  0  1675  0  a
3  3  12  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  36 0
P    1  19  0  37 0
P    10  21  0  48 0
P    4  1  500  0 1
P    5  8  0  19 0
P    5  15  300  0 1
P    5  22  0  20 0
P    4  27  100  0 1
P    7  12  0  47 0
P    7  18  0  40 0
P    13  6  300  0 1
P    9  8  0  7 0
P    9  15  300  0 1
P    9  22  0  6 0
P    10  29  0  45 0
P    11  18  0  44 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 83 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 27 0
A    6  20 0 32 0
A    8  6 0 19 0
A    8  24 0 14 0
D    6  6 0 31 0
D    6  24 0 28 0
D    8  10 0 15 0
D    8  20 0 18 0

actions_asked
player action direction
0 m t
1 u n
2 m r
3 m t

actions_done
player action direction
0 m t
1 u n
2 m r
3 m t


round 64

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X..........P..............X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  9  22  1  1  1  0  1800  0  a
1  1  7  22  3  3  1  0  1275  35  d
2  2  9  13  3  3  1  0  1675  0  a
3  3  11  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    10  13  300  0 1
P    1  11  0  35 0
P    1  19  0  36 0
P    10  21  0  47 0
P    4  1  500  0 1
P    5  8  0  18 0
P    5  15  300  0 1
P    5  22  0  19 0
P    4  27  100  0 1
P    7  12  0  46 0
P    7  18  0  39 0
P    13  6  300  0 1
P    9  8  0  6 0
P    9  15  300  0 1
P    9  22  0  5 0
P    10  29  0  44 0
P    11  18  0  43 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 82 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 26 0
A    6  20 0 31 0
A    8  6 0 18 0
A    8  24 0 13 0
D    6  6 0 30 0
D    6  24 0 27 0
D    8  10 0 14 0
D    8  20 0 17 0

actions_asked
player action direction
0 m t
1 u n
2 m b
3 m t

actions_done
player action direction
0 m t
1 u n
2 m b
3 m t


round 65

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  22  1  1  1  0  1800  0  a
1  1  7  22  3  3  1  0  1275  34  d
2  2  10  13  3  3  1  0  1975  0  a
3  3  10  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    10  13  0  49 0
P    1  11  0  34 0
P    1  19  0  35 0
P    10  21  0  46 0
P    4  1  500  0 1
P    5  8  0  17 0
P    5  15  300  0 1
P    5  22  0  18 0
P    4  27  100  0 1
P    7  12  0  45 0
P    7  18  0  38 0
P    13  6  300  0 1
P    9  8  0  5 0
P    9  15  300  0 1
P    9  22  0  4 0
P    10  29  0  43 0
P    11  18  0  42 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 81 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 25 0
A    6  20 0 30 0
A    8  6 0 17 0
A    8  24 0 12 0
D    6  6 0 29 0
D    6  24 0 26 0
D    8  10 0 13 0
D    8  20 0 16 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m t

actions_done
player action direction
0 m l
1 u n
2 m l
3 m t


round 66

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XP....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  21  1  1  1  0  1800  0  a
1  1  7  22  3  3  1  0  1275  33  d
2  2  10  12  3  3  1  0  1975  0  a
3  3  9  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    10  13  0  48 0
P    1  11  0  33 0
P    1  19  0  34 0
P    10  21  0  45 0
P    4  1  500  0 1
P    5  8  0  16 0
P    5  15  300  0 1
P    5  22  0  17 0
P    4  27  100  0 1
P    7  12  0  44 0
P    7  18  0  37 0
P    13  6  300  0 1
P    9  8  0  4 0
P    9  15  300  0 1
P    9  22  0  3 0
P    10  29  0  42 0
P    11  18  0  41 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  400  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 80 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 24 0
A    6  20 0 29 0
A    8  6 0 16 0
A    8  24 0 11 0
D    6  6 0 28 0
D    6  24 0 25 0
D    8  10 0 12 0
D    8  20 0 15 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m t

actions_done
player action direction
0 m l
1 u n
2 m l
3 m t


round 67

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  20  1  1  1  0  2200  0  a
1  1  7  22  3  3  1  0  1275  32  d
2  2  10  11  3  3  1  0  1975  0  a
3  3  8  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    10  13  0  47 0
P    1  11  0  32 0
P    1  19  0  33 0
P    10  21  0  44 0
P    4  1  500  0 1
P    5  8  0  15 0
P    5  15  300  0 1
P    5  22  0  16 0
P    4  27  100  0 1
P    7  12  0  43 0
P    7  18  0  36 0
P    13  6  300  0 1
P    9  8  0  3 0
P    9  15  300  0 1
P    9  22  0  2 0
P    10  29  0  41 0
P    11  18  0  40 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  0  49 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 79 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 23 0
A    6  20 0 28 0
A    8  6 0 15 0
A    8  24 0 10 0
D    6  6 0 27 0
D    6  24 0 24 0
D    8  10 0 11 0
D    8  20 0 14 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m t

actions_done
player action direction
0 m r
1 u n
2 m l
3 m t


round 68

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  21  1  1  1  0  2200  0  a
1  1  7  22  3  3  1  0  1275  31  d
2  2  10  10  3  3  1  0  1975  0  a
3  3  7  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    10  13  0  46 0
P    1  11  0  31 0
P    1  19  0  32 0
P    10  21  0  43 0
P    4  1  500  0 1
P    5  8  0  14 0
P    5  15  300  0 1
P    5  22  0  15 0
P    4  27  100  0 1
P    7  12  0  42 0
P    7  18  0  35 0
P    13  6  300  0 1
P    9  8  0  2 0
P    9  15  300  0 1
P    9  22  0  1 0
P    10  29  0  40 0
P    11  18  0  39 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  0  48 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 78 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 22 0
A    6  20 0 27 0
A    8  6 0 14 0
A    8  24 0 9 0
D    6  6 0 26 0
D    6  24 0 23 0
D    8  10 0 10 0
D    8  20 0 13 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m t

actions_done
player action direction
0 m r
1 u n
2 m l
3 m t


round 69

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P..P.....X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  22  1  1  1  0  2200  0  a
1  1  7  22  3  3  1  0  1275  30  d
2  2  10  9  3  3  1  0  1975  0  a
3  3  6  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    10  13  0  45 0
P    1  11  0  30 0
P    1  19  0  31 0
P    10  21  0  42 0
P    4  1  500  0 1
P    5  8  0  13 0
P    5  15  300  0 1
P    5  22  0  14 0
P    4  27  100  0 1
P    7  12  0  41 0
P    7  18  0  34 0
P    13  6  300  0 1
P    9  8  0  1 0
P    9  15  300  0 1
P    13  22  500  0 1
P    10  29  0  39 0
P    11  18  0  38 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  0  47 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 77 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 21 0
A    6  20 0 26 0
A    8  6 0 13 0
A    8  24 0 8 0
D    6  6 0 25 0
D    6  24 0 22 0
D    8  10 0 9 0
D    8  20 0 12 0

actions_asked
player action direction
0 m b
1 u n
2 m b
3 m t

actions_done
player action direction
0 m b
1 u n
2 m b
3 m t


round 70

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P..P.....X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  9  22  1  1  1  0  2200  0  a
1  1  7  22  3  3  1  0  1275  29  d
2  2  11  9  3  3  1  0  1975  0  a
3  3  5  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    10  13  0  44 0
P    1  11  0  29 0
P    1  19  0  30 0
P    10  21  0  41 0
P    4  1  500  0 1
P    5  8  0  12 0
P    5  15  300  0 1
P    5  22  0  13 0
P    4  27  100  0 1
P    7  12  0  40 0
P    7  18  0  33 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  500  0 1
P    10  29  0  38 0
P    11  18  0  37 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  0  46 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 76 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 20 0
A    6  20 0 25 0
A    8  6 0 12 0
A    8  24 0 7 0
D    6  6 0 24 0
D    6  24 0 21 0
D    8  10 0 8 0
D    8  20 0 11 0

actions_asked
player action direction
0 m b
1 u n
2 m b
3 m t

actions_done
player action direction
0 m b
1 u n
2 m b
3 m t


round 71

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P..P.....X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  22  1  1  1  0  2200  0  a
1  1  7  22  3  3  1  0  1275  28  d
2  2  12  9  3  3  1  0  1975  0  a
3  3  4  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    10  13  0  43 0
P    1  11  0  28 0
P    1  19  0  29 0
P    10  21  0  40 0
P    4  1  500  0 1
P    5  8  0  11 0
P    5  15  300  0 1
P    5  22  0  12 0
P    4  27  100  0 1
P    7  12  0  39 0
P    7  18  0  32 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  500  0 1
P    10  29  0  37 0
P    11  18  0  36 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  0  45 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 75 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 19 0
A    6  20 0 24 0
A    8  6 0 11 0
A    8  24 0 6 0
D    6  6 0 23 0
D    6  24 0 20 0
D    8  10 0 7 0
D    8  20 0 10 0

actions_asked
player action direction
0 m b
1 u n
2 m b
3 m t

actions_done
player action direction
0 m b
1 u n
2 m b
3 m t


round 72

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P..P.....X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  22  1  1  1  0  2200  0  a
1  1  7  22  3  3  1  0  1275  27  d
2  2  13  9  3  3  1  0  1975  0  a
3  3  3  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    10  13  0  42 0
P    1  11  0  27 0
P    1  19  0  28 0
P    10  21  0  39 0
P    4  1  500  0 1
P    5  8  0  10 0
P    5  15  300  0 1
P    5  22  0  11 0
P    4  27  100  0 1
P    7  12  0  38 0
P    7  18  0  31 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  500  0 1
P    10  29  0  36 0
P    11  18  0  35 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  0  44 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 74 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 18 0
A    6  20 0 23 0
A    8  6 0 10 0
A    8  24 0 5 0
D    6  6 0 22 0
D    6  24 0 19 0
D    8  10 0 6 0
D    8  20 0 9 0

actions_asked
player action direction
0 m b
1 u n
2 m r
3 m t

actions_done
player action direction
0 m b
1 u n
2 m r
3 m t


round 73

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................XSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P....P.......P..P.....X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  12  22  1  1  1  0  2200  0  a
1  1  7  22  3  3  1  0  1275  26  d
2  2  13  10  3  3  1  0  1975  0  a
3  3  2  29  3  3  2  1  1000  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    10  13  0  41 0
P    1  11  0  26 0
P    1  19  0  27 0
P    10  21  0  38 0
P    4  1  500  0 1
P    5  8  0  9 0
P    5  15  300  0 1
P    5  22  0  10 0
P    4  27  100  0 1
P    7  12  0  37 0
P    7  18  0  30 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  500  0 1
P    10  29  0  35 0
P    11  18  0  34 0
P    13  11  200  0 1
P    13  19  200  0 1
P    8  20  0  43 0
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 73 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 17 0
A    6  20 0 22 0
A    8  6 0 9 0
A    8  24 0 4 0
D    6  6 0 21 0
D    6  24 0 18 0
D    8  10 0 5 0
D    8  20 0 8 0

actions_asked
player action direction
0 m b
1 u n
2 m r
3 m t

actions_done
player action direction
0 m b
1 u n
2 m r
3 m t


round 74

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P............P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  22  1  1  1  0  2700  0  a
1  1  7  22  3  3  1  0  1275  25  d
2  2  13  11  3  3  1  0  2175  0  a
3  3  1  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    10  13  0  40 0
P    1  11  0  25 0
P    1  19  0  26 0
P    10  21  0  37 0
P    4  1  500  0 1
P    5  8  0  8 0
P    5  15  300  0 1
P    5  22  0  9 0
P    4  27  100  0 1
P    7  12  0  36 0
P    7  18  0  29 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  49 0
P    10  29  0  34 0
P    11  18  0  33 0
P    13  11  0  49 0
P    13  19  200  0 1
P    8  20  0  42 0
S    1  1 0 0 1
S    1  29 0 84 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 72 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 16 0
A    6  20 0 21 0
A    8  6 0 8 0
A    8  24 0 3 0
D    6  6 0 20 0
D    6  24 0 17 0
D    8  10 0 4 0
D    8  20 0 7 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m r

actions_done
player action direction
0 m l
1 u n
2 m l
3 u n


round 75

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P............P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  21  1  1  1  0  2700  0  a
1  1  7  22  3  3  1  0  1275  24  d
2  2  13  10  3  3  1  0  2175  0  a
3  3  1  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    10  13  0  39 0
P    1  11  0  24 0
P    1  19  0  25 0
P    10  21  0  36 0
P    4  1  500  0 1
P    5  8  0  7 0
P    5  15  300  0 1
P    5  22  0  8 0
P    4  27  100  0 1
P    7  12  0  35 0
P    7  18  0  28 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  48 0
P    10  29  0  33 0
P    11  18  0  32 0
P    13  11  0  48 0
P    13  19  200  0 1
P    8  20  0  41 0
S    1  1 0 0 1
S    1  29 0 83 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 71 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 15 0
A    6  20 0 20 0
A    8  6 0 7 0
A    8  24 0 2 0
D    6  6 0 19 0
D    6  24 0 16 0
D    8  10 0 3 0
D    8  20 0 6 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m t

actions_done
player action direction
0 m l
1 u n
2 m l
3 u n


round 76

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P............P........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  20  1  1  1  0  2700  0  a
1  1  7  22  3  3  1  0  1275  23  d
2  2  13  9  3  3  1  0  2175  0  a
3  3  1  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    10  13  0  38 0
P    1  11  0  23 0
P    1  19  0  24 0
P    10  21  0  35 0
P    4  1  500  0 1
P    5  8  0  6 0
P    5  15  300  0 1
P    5  22  0  7 0
P    4  27  100  0 1
P    7  12  0  34 0
P    7  18  0  27 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  47 0
P    10  29  0  32 0
P    11  18  0  31 0
P    13  11  0  47 0
P    13  19  200  0 1
P    8  20  0  40 0
S    1  1 0 0 1
S    1  29 0 82 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 70 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 14 0
A    6  20 0 19 0
A    8  6 0 6 0
A    8  24 0 1 0
D    6  6 0 18 0
D    6  24 0 15 0
D    8  10 0 2 0
D    8  20 0 5 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m b

actions_done
player action direction
0 m l
1 u n
2 m l
3 m b


round 77

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX........................PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P.....................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  19  1  1  1  0  2900  0  a
1  1  7  22  3  3  1  0  1275  22  d
2  2  13  8  3  3  1  0  2175  0  a
3  3  2  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    10  13  0  37 0
P    1  11  0  22 0
P    1  19  0  23 0
P    10  21  0  34 0
P    4  1  500  0 1
P    5  8  0  5 0
P    5  15  300  0 1
P    5  22  0  6 0
P    4  27  100  0 1
P    7  12  0  33 0
P    7  18  0  26 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  46 0
P    10  29  0  31 0
P    11  18  0  30 0
P    13  11  0  46 0
P    13  19  0  49 0
P    8  20  0  39 0
S    1  1 0 0 1
S    1  29 0 81 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 69 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 13 0
A    6  20 0 18 0
A    8  6 0 5 0
A    9  12 0 0 1
D    6  6 0 17 0
D    6  24 0 14 0
D    8  10 0 1 0
D    8  20 0 4 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m r

actions_done
player action direction
0 m l
1 u n
2 m l
3 u n


round 78

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX................D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...P.....................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  18  1  1  1  0  2900  0  a
1  1  7  22  3  3  1  0  1275  21  d
2  2  13  7  3  3  1  0  2175  0  a
3  3  2  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    10  13  0  36 0
P    1  11  0  21 0
P    1  19  0  22 0
P    10  21  0  33 0
P    4  1  500  0 1
P    5  8  0  4 0
P    5  15  300  0 1
P    5  22  0  5 0
P    4  27  100  0 1
P    7  12  0  32 0
P    7  18  0  25 0
P    13  6  300  0 1
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  45 0
P    10  29  0  30 0
P    11  18  0  29 0
P    13  11  0  45 0
P    13  19  0  48 0
P    8  20  0  38 0
S    1  1 0 0 1
S    1  29 0 80 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 68 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 12 0
A    6  20 0 17 0
A    8  6 0 4 0
A    9  12 0 0 1
D    6  6 0 16 0
D    6  24 0 13 0
D    4  19 0 0 1
D    8  20 0 3 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m b

actions_done
player action direction
0 m l
1 u n
2 m l
3 m b


round 79

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX................D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  17  1  1  1  0  2900  0  a
1  1  7  22  3  3  1  0  1275  20  d
2  2  13  6  3  3  1  0  2475  0  a
3  3  3  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    10  13  0  35 0
P    1  11  0  20 0
P    1  19  0  21 0
P    10  21  0  32 0
P    4  1  500  0 1
P    5  8  0  3 0
P    5  15  300  0 1
P    5  22  0  4 0
P    4  27  100  0 1
P    7  12  0  31 0
P    7  18  0  24 0
P    13  6  0  49 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  44 0
P    10  29  0  29 0
P    11  18  0  28 0
P    13  11  0  44 0
P    13  19  0  47 0
P    8  20  0  37 0
S    1  1 0 0 1
S    1  29 0 79 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 67 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 11 0
A    6  20 0 16 0
A    8  6 0 3 0
A    9  12 0 0 1
D    6  6 0 15 0
D    6  24 0 12 0
D    4  19 0 0 1
D    8  20 0 2 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 u n


round 80

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX................D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  16  1  1  1  0  2900  0  a
1  1  7  22  3  3  1  0  1275  19  d
2  2  13  7  3  3  1  0  2475  0  a
3  3  3  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    10  13  0  34 0
P    1  11  0  19 0
P    1  19  0  20 0
P    10  21  0  31 0
P    4  1  500  0 1
P    5  8  0  2 0
P    5  15  300  0 1
P    5  22  0  3 0
P    4  27  100  0 1
P    7  12  0  30 0
P    7  18  0  23 0
P    13  6  0  48 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  43 0
P    10  29  0  28 0
P    11  18  0  27 0
P    13  11  0  43 0
P    13  19  0  46 0
P    8  20  0  36 0
S    1  1 0 0 1
S    1  29 0 78 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 66 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 10 0
A    6  20 0 15 0
A    8  6 0 2 0
A    9  12 0 0 1
D    6  6 0 14 0
D    6  24 0 11 0
D    4  19 0 0 1
D    8  20 0 1 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m t

actions_done
player action direction
0 m l
1 u n
2 m r
3 m t


round 81

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX................D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSX...................D.....X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  15  1  1  1  0  2900  0  a
1  1  7  22  3  3  1  0  1275  18  d
2  2  13  8  3  3  1  0  2475  0  a
3  3  2  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    10  13  0  33 0
P    1  11  0  18 0
P    1  19  0  19 0
P    10  21  0  30 0
P    4  1  500  0 1
P    5  8  0  1 0
P    5  15  300  0 1
P    5  22  0  2 0
P    4  27  100  0 1
P    7  12  0  29 0
P    7  18  0  22 0
P    13  6  0  47 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  42 0
P    10  29  0  27 0
P    11  18  0  26 0
P    13  11  0  42 0
P    13  19  0  45 0
P    8  20  0  35 0
S    1  1 0 0 1
S    1  29 0 77 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 65 0
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 9 0
A    6  20 0 14 0
A    8  6 0 1 0
A    9  12 0 0 1
D    6  6 0 13 0
D    6  24 0 10 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m t
1 u n
2 m r
3 m l

actions_done
player action direction
0 m t
1 u n
2 m r
3 u n


round 82

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  12  15  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  17  d
2  2  13  9  3  3  1  0  2475  0  a
3  3  2  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    10  13  0  32 0
P    1  11  0  17 0
P    1  19  0  18 0
P    10  21  0  29 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    5  22  0  1 0
P    4  27  100  0 1
P    7  12  0  28 0
P    7  18  0  21 0
P    13  6  0  46 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  41 0
P    10  29  0  26 0
P    11  18  0  25 0
P    13  11  0  41 0
P    13  19  0  44 0
P    8  20  0  34 0
S    1  1 0 0 1
S    1  29 0 76 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 64 0
R    2  15 0 0 1
R    12  15 0 64 0
A    6  10 0 8 0
A    6  20 0 13 0
A    13  25 0 0 1
A    9  12 0 0 1
D    6  6 0 12 0
D    6  24 0 9 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m r

actions_done
player action direction
0 m r
1 u n
2 m t
3 u n


round 83

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  12  16  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  16  d
2  2  12  9  3  3  1  0  2475  0  a
3  3  2  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    10  13  0  31 0
P    1  11  0  16 0
P    1  19  0  17 0
P    10  21  0  28 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  27 0
P    7  18  0  20 0
P    13  6  0  45 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  40 0
P    10  29  0  25 0
P    11  18  0  24 0
P    13  11  0  40 0
P    13  19  0  43 0
P    8  20  0  33 0
S    1  1 0 0 1
S    1  29 0 75 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 63 0
R    2  15 0 0 1
R    12  15 0 63 0
A    6  10 0 7 0
A    6  20 0 12 0
A    13  25 0 0 1
A    9  12 0 0 1
D    6  6 0 11 0
D    6  24 0 8 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m b
1 u n
2 m t
3 m l

actions_done
player action direction
0 m b
1 u n
2 m t
3 u n


round 84

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  16  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  15  d
2  2  11  9  3  3  1  0  2475  0  a
3  3  2  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    10  13  0  30 0
P    1  11  0  15 0
P    1  19  0  16 0
P    10  21  0  27 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  26 0
P    7  18  0  19 0
P    13  6  0  44 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  39 0
P    10  29  0  24 0
P    11  18  0  23 0
P    13  11  0  39 0
P    13  19  0  42 0
P    8  20  0  32 0
S    1  1 0 0 1
S    1  29 0 74 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 62 0
R    2  15 0 0 1
R    12  15 0 62 0
A    6  10 0 6 0
A    6  20 0 11 0
A    13  25 0 0 1
A    9  12 0 0 1
D    6  6 0 10 0
D    6  24 0 7 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m b

actions_done
player action direction
0 m r
1 u n
2 m t
3 m b


round 85

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  17  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  14  d
2  2  10  9  3  3  1  0  2475  0  a
3  3  3  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    10  13  0  29 0
P    1  11  0  14 0
P    1  19  0  15 0
P    10  21  0  26 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  25 0
P    7  18  0  18 0
P    13  6  0  43 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  38 0
P    10  29  0  23 0
P    11  18  0  22 0
P    13  11  0  38 0
P    13  19  0  41 0
P    8  20  0  31 0
S    1  1 0 0 1
S    1  29 0 73 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 61 0
R    2  15 0 0 1
R    12  15 0 61 0
A    6  10 0 5 0
A    6  20 0 10 0
A    13  25 0 0 1
A    9  12 0 0 1
D    6  6 0 9 0
D    6  24 0 6 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m b

actions_done
player action direction
0 m r
1 u n
2 m r
3 m b


round 86

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  18  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  13  d
2  2  10  10  3  3  1  0  2475  0  a
3  3  4  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    10  13  0  28 0
P    1  11  0  13 0
P    1  19  0  14 0
P    10  21  0  25 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  24 0
P    7  18  0  17 0
P    13  6  0  42 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  37 0
P    10  29  0  22 0
P    11  18  0  21 0
P    13  11  0  37 0
P    13  19  0  40 0
P    8  20  0  30 0
S    1  1 0 0 1
S    1  29 0 72 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 60 0
R    2  15 0 0 1
R    12  15 0 60 0
A    6  10 0 4 0
A    6  20 0 9 0
A    13  25 0 0 1
A    9  12 0 0 1
D    6  6 0 8 0
D    6  24 0 5 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m b

actions_done
player action direction
0 m r
1 u n
2 m r
3 m b


round 87

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  19  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  12  d
2  2  10  11  3  3  1  0  2475  0  a
3  3  5  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    10  13  0  27 0
P    1  11  0  12 0
P    1  19  0  13 0
P    10  21  0  24 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  23 0
P    7  18  0  16 0
P    13  6  0  41 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  36 0
P    10  29  0  21 0
P    11  18  0  20 0
P    13  11  0  36 0
P    13  19  0  39 0
P    8  20  0  29 0
S    1  1 0 0 1
S    1  29 0 71 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 59 0
R    2  15 0 0 1
R    12  15 0 59 0
A    6  10 0 3 0
A    6  20 0 8 0
A    13  25 0 0 1
A    9  12 0 0 1
D    6  6 0 7 0
D    6  24 0 4 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m l

actions_done
player action direction
0 m r
1 u n
2 m r
3 u n


round 88

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXXA..P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  20  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  11  d
2  2  10  12  3  3  1  0  2475  0  a
3  3  5  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    10  13  0  26 0
P    1  11  0  11 0
P    1  19  0  12 0
P    10  21  0  23 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  22 0
P    7  18  0  15 0
P    13  6  0  40 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  35 0
P    10  29  0  20 0
P    11  18  0  19 0
P    13  11  0  35 0
P    13  19  0  38 0
P    8  20  0  28 0
S    1  1 0 0 1
S    1  29 0 70 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 58 0
R    2  15 0 0 1
R    12  15 0 58 0
A    6  10 0 2 0
A    6  20 0 7 0
A    13  25 0 0 1
A    9  12 0 0 1
D    6  6 0 6 0
D    6  24 0 3 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m t

actions_done
player action direction
0 m r
1 u n
2 m t
3 m t


round 89

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX...................D..A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  21  1  1  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  10  d
2  2  9  12  4  3  1  0  2475  0  a
3  3  4  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    10  13  0  25 0
P    1  11  0  10 0
P    1  19  0  11 0
P    10  21  0  22 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  21 0
P    7  18  0  14 0
P    13  6  0  39 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  34 0
P    10  29  0  19 0
P    11  18  0  18 0
P    13  11  0  34 0
P    13  19  0  37 0
P    8  20  0  27 0
S    1  1 0 0 1
S    1  29 0 69 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 57 0
R    2  15 0 0 1
R    12  15 0 57 0
A    6  10 0 1 0
A    6  20 0 6 0
A    13  25 0 0 1
A    9  12 0 54 0
D    6  6 0 5 0
D    6  24 0 2 0
D    4  19 0 0 1
D    13  22 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m r

actions_done
player action direction
0 m r
1 u n
2 m t
3 u n


round 90

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX......................A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  22  1  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  9  d
2  2  8  12  4  3  1  0  2475  0  a
3  3  4  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    10  13  0  24 0
P    1  11  0  9 0
P    1  19  0  10 0
P    10  21  0  21 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  20 0
P    7  18  0  13 0
P    13  6  0  38 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  33 0
P    10  29  0  18 0
P    11  18  0  17 0
P    13  11  0  33 0
P    13  19  0  36 0
P    8  20  0  26 0
S    1  1 0 0 1
S    1  29 0 68 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 56 0
R    2  15 0 0 1
R    12  15 0 56 0
A    1  12 0 0 1
A    6  20 0 5 0
A    13  25 0 0 1
A    9  12 0 53 0
D    6  6 0 4 0
D    6  24 0 1 0
D    4  19 0 0 1
D    13  22 0 54 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m b

actions_done
player action direction
0 m r
1 u n
2 m t
3 m b


round 91

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX......................A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  23  1  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  8  d
2  2  7  12  4  3  1  0  2475  0  a
3  3  5  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    10  13  0  23 0
P    1  11  0  8 0
P    1  19  0  9 0
P    10  21  0  20 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  19 0
P    7  18  0  12 0
P    13  6  0  37 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  32 0
P    10  29  0  17 0
P    11  18  0  16 0
P    13  11  0  32 0
P    13  19  0  35 0
P    8  20  0  25 0
S    1  1 0 0 1
S    1  29 0 67 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 55 0
R    2  15 0 0 1
R    12  15 0 55 0
A    1  12 0 0 1
A    6  20 0 4 0
A    13  25 0 0 1
A    9  12 0 52 0
D    6  6 0 3 0
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 53 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m b

actions_done
player action direction
0 m r
1 u n
2 m t
3 m b


round 92

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX......................A..X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  24  1  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  7  d
2  2  6  12  4  3  1  0  2475  0  a
3  3  6  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    10  13  0  22 0
P    1  11  0  7 0
P    1  19  0  8 0
P    10  21  0  19 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  18 0
P    7  18  0  11 0
P    13  6  0  36 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  31 0
P    10  29  0  16 0
P    11  18  0  15 0
P    13  11  0  31 0
P    13  19  0  34 0
P    8  20  0  24 0
S    1  1 0 0 1
S    1  29 0 66 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 54 0
R    2  15 0 0 1
R    12  15 0 54 0
A    1  12 0 0 1
A    6  20 0 3 0
A    13  25 0 0 1
A    9  12 0 51 0
D    6  6 0 2 0
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 52 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m b

actions_done
player action direction
0 m r
1 u n
2 m t
3 m b


round 93

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  25  2  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  6  d
2  2  5  12  4  3  1  0  2475  0  a
3  3  7  29  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    10  13  0  21 0
P    1  11  0  6 0
P    1  19  0  7 0
P    10  21  0  18 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  17 0
P    7  18  0  10 0
P    13  6  0  35 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  30 0
P    10  29  0  15 0
P    11  18  0  14 0
P    13  11  0  30 0
P    13  19  0  33 0
P    8  20  0  23 0
S    1  1 0 0 1
S    1  29 0 65 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 53 0
R    2  15 0 0 1
R    12  15 0 53 0
A    1  12 0 0 1
A    6  20 0 2 0
A    13  25 0 54 0
A    9  12 0 50 0
D    6  6 0 1 0
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 51 0

actions_asked
player action direction
0 m l
1 u n
2 m t
3 m l

actions_done
player action direction
0 m l
1 u n
2 m t
3 m l


round 94

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  24  2  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  5  d
2  2  4  12  4  3  1  0  2475  0  a
3  3  7  28  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    10  13  0  20 0
P    1  11  0  5 0
P    1  19  0  6 0
P    10  21  0  17 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  16 0
P    7  18  0  9 0
P    13  6  0  34 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  29 0
P    10  29  0  14 0
P    11  18  0  13 0
P    13  11  0  29 0
P    13  19  0  32 0
P    8  20  0  22 0
S    1  1 0 0 1
S    1  29 0 64 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 52 0
R    2  15 0 0 1
R    12  15 0 52 0
A    1  12 0 0 1
A    6  20 0 1 0
A    13  25 0 53 0
A    9  12 0 49 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 50 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 95

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX..............A..........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  23  2  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  4  d
2  2  4  11  4  3  1  0  2475  0  a
3  3  7  27  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    10  13  0  19 0
P    1  11  0  4 0
P    1  19  0  5 0
P    10  21  0  16 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  15 0
P    7  18  0  8 0
P    13  6  0  33 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  28 0
P    10  29  0  13 0
P    11  18  0  12 0
P    13  11  0  28 0
P    13  19  0  31 0
P    8  20  0  21 0
S    1  1 0 0 1
S    1  29 0 63 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 51 0
R    2  15 0 0 1
R    12  15 0 51 0
A    1  12 0 0 1
A    13  17 0 0 1
A    13  25 0 52 0
A    9  12 0 48 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 49 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m t

actions_done
player action direction
0 m l
1 u n
2 m l
3 m t


round 96

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX..............A..........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  22  2  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  3  d
2  2  4  10  4  3  1  0  2475  0  a
3  3  6  27  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    10  13  0  18 0
P    1  11  0  3 0
P    1  19  0  4 0
P    10  21  0  15 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  14 0
P    7  18  0  7 0
P    13  6  0  32 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  27 0
P    10  29  0  12 0
P    11  18  0  11 0
P    13  11  0  27 0
P    13  19  0  30 0
P    8  20  0  20 0
S    1  1 0 0 1
S    1  29 0 62 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 50 0
R    2  15 0 0 1
R    12  15 0 50 0
A    1  12 0 0 1
A    13  17 0 0 1
A    13  25 0 51 0
A    9  12 0 47 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 48 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m t

actions_done
player action direction
0 m l
1 u n
2 m l
3 m t


round 97

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D.......PX.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX..............A..........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  21  2  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  2  d
2  2  4  9  4  3  1  0  2475  0  a
3  3  5  27  5  5  3  2  1000  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    10  13  0  17 0
P    1  11  0  2 0
P    1  19  0  3 0
P    10  21  0  14 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  100  0 1
P    7  12  0  13 0
P    7  18  0  6 0
P    13  6  0  31 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  26 0
P    10  29  0  11 0
P    11  18  0  10 0
P    13  11  0  26 0
P    13  19  0  29 0
P    8  20  0  19 0
S    1  1 0 0 1
S    1  29 0 61 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 49 0
R    2  15 0 0 1
R    12  15 0 49 0
A    1  12 0 0 1
A    13  17 0 0 1
A    13  25 0 50 0
A    9  12 0 46 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 47 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m t

actions_done
player action direction
0 m l
1 u n
2 m l
3 m t


round 98

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX...P............D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX..............A..........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  20  2  2  2  0  2900  0  a
1  1  7  22  3  3  1  0  1275  1  d
2  2  4  8  4  3  1  0  2475  0  a
3  3  4  27  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    10  13  0  16 0
P    1  11  0  1 0
P    1  19  0  2 0
P    10  21  0  13 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  49 0
P    7  12  0  12 0
P    7  18  0  5 0
P    13  6  0  30 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  25 0
P    10  29  0  10 0
P    11  18  0  9 0
P    13  11  0  25 0
P    13  19  0  28 0
P    8  20  0  18 0
S    1  1 0 0 1
S    1  29 0 60 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 48 0
R    2  15 0 0 1
R    12  15 0 48 0
A    1  12 0 0 1
A    13  17 0 0 1
A    13  25 0 49 0
A    9  12 0 45 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 46 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 99

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX...P............D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX..............A..........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  19  2  2  2  0  2900  0  a
1  1  8  20  3  3  1  0  1275  0  a
2  2  4  7  4  3  1  0  2475  0  a
3  3  4  26  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    10  13  0  15 0
P    3  12  100  0 1
P    1  19  0  1 0
P    10  21  0  12 0
P    4  1  500  0 1
P    4  6  400  0 1
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  48 0
P    7  12  0  11 0
P    7  18  0  4 0
P    13  6  0  29 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  24 0
P    10  29  0  9 0
P    11  18  0  8 0
P    13  11  0  24 0
P    13  19  0  27 0
P    8  20  0  17 0
S    1  1 0 0 1
S    1  29 0 59 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 47 0
R    2  15 0 0 1
R    12  15 0 47 0
A    1  12 0 0 1
A    13  17 0 0 1
A    13  25 0 48 0
A    9  12 0 44 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 45 0

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 100

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX.............P..D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX..............A..........X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  18  2  2  2  0  2900  0  a
1  1  8  21  3  3  1  0  1275  0  a
2  2  4  6  4  3  1  0  2875  0  a
3  3  4  25  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    10  13  0  14 0
P    3  12  100  0 1
P    4  16  100  0 1
P    10  21  0  11 0
P    4  1  500  0 1
P    4  6  0  49 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  47 0
P    7  12  0  10 0
P    7  18  0  3 0
P    13  6  0  28 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  23 0
P    10  29  0  8 0
P    11  18  0  7 0
P    13  11  0  23 0
P    13  19  0  26 0
P    8  20  0  16 0
S    1  1 0 0 1
S    1  29 0 58 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 46 0
R    2  15 0 0 1
R    12  15 0 46 0
A    1  12 0 0 1
A    13  17 0 0 1
A    13  25 0 47 0
A    9  12 0 43 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 44 0

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 101

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX.............P..D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  17  3  2  2  0  2900  0  a
1  1  8  22  3  3  1  0  1275  0  a
2  2  4  7  4  3  1  0  2875  0  a
3  3  4  24  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    10  13  0  13 0
P    3  12  100  0 1
P    4  16  100  0 1
P    10  21  0  10 0
P    4  1  500  0 1
P    4  6  0  48 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  46 0
P    7  12  0  9 0
P    7  18  0  2 0
P    13  6  0  27 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  22 0
P    10  29  0  7 0
P    11  18  0  6 0
P    13  11  0  22 0
P    13  19  0  25 0
P    8  20  0  15 0
S    1  1 0 0 1
S    1  29 0 57 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 45 0
R    2  15 0 0 1
R    12  15 0 45 0
A    1  12 0 0 1
A    13  17 0 54 0
A    13  25 0 46 0
A    9  12 0 42 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 43 0

actions_asked
player action direction
0 m l
1 m b
2 m r
3 m l

actions_done
player action direction
0 m l
1 m b
2 m r
3 m l


round 102

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX.............P..D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  16  3  2  2  0  2900  0  a
1  1  9  22  3  3  1  0  1275  0  a
2  2  4  8  4  3  1  0  2875  0  a
3  3  4  23  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    10  13  0  12 0
P    3  12  100  0 1
P    4  16  100  0 1
P    10  21  0  9 0
P    4  1  500  0 1
P    4  6  0  47 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  45 0
P    7  12  0  8 0
P    7  18  0  1 0
P    13  6  0  26 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  21 0
P    10  29  0  6 0
P    11  18  0  5 0
P    13  11  0  21 0
P    13  19  0  24 0
P    8  20  0  14 0
S    1  1 0 0 1
S    1  29 0 56 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 44 0
R    2  15 0 0 1
R    12  15 0 44 0
A    1  12 0 0 1
A    13  17 0 53 0
A    13  25 0 45 0
A    9  12 0 41 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 42 0

actions_asked
player action direction
0 m l
1 m b
2 m r
3 m l

actions_done
player action direction
0 m l
1 m b
2 m r
3 m l


round 103

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX.............P..D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  15  3  2  2  0  2900  0  a
1  1  10  22  3  3  1  0  1275  0  a
2  2  4  9  4  3  1  0  2875  0  a
3  3  4  22  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    10  13  0  11 0
P    3  12  100  0 1
P    4  16  100  0 1
P    10  21  0  8 0
P    4  1  500  0 1
P    4  6  0  46 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  44 0
P    7  12  0  7 0
P    10  11  300  0 1
P    13  6  0  25 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  20 0
P    10  29  0  5 0
P    11  18  0  4 0
P    13  11  0  20 0
P    13  19  0  23 0
P    8  20  0  13 0
S    1  1 0 0 1
S    1  29 0 55 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 43 0
R    2  15 0 0 1
R    12  15 0 43 0
A    1  12 0 0 1
A    13  17 0 52 0
A    13  25 0 44 0
A    9  12 0 40 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 41 0

actions_asked
player action direction
0 m l
1 m l
2 m r
3 m l

actions_done
player action direction
0 m l
1 m l
2 m r
3 m l


round 104

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX.............P..D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXXD..XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  14  3  2  2  0  2900  0  a
1  1  10  21  3  3  1  0  1275  0  a
2  2  4  10  4  3  1  0  2875  0  a
3  3  4  21  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    10  13  0  10 0
P    3  12  100  0 1
P    4  16  100  0 1
P    10  21  0  7 0
P    4  1  500  0 1
P    4  6  0  45 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  43 0
P    7  12  0  6 0
P    10  11  300  0 1
P    13  6  0  24 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  19 0
P    10  29  0  4 0
P    11  18  0  3 0
P    13  11  0  19 0
P    13  19  0  22 0
P    8  20  0  12 0
S    1  1 0 0 1
S    1  29 0 54 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 42 0
R    2  15 0 0 1
R    12  15 0 42 0
A    1  12 0 0 1
A    13  17 0 51 0
A    13  25 0 43 0
A    9  12 0 39 0
D    11  7 0 0 1
D    12  14 0 0 1
D    4  19 0 0 1
D    13  22 0 40 0

actions_asked
player action direction
0 m t
1 m l
2 m r
3 m l

actions_done
player action direction
0 m t
1 m l
2 m r
3 m l


round 105

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX.............P..D........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  12  14  3  3  2  0  2900  0  a
1  1  10  20  3  3  1  0  1275  0  a
2  2  4  11  4  3  1  0  2875  0  a
3  3  4  20  5  5  3  2  1100  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    10  13  0  9 0
P    3  12  100  0 1
P    4  16  100  0 1
P    10  21  0  6 0
P    4  1  500  0 1
P    4  6  0  44 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  42 0
P    7  12  0  5 0
P    10  11  300  0 1
P    13  6  0  23 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  18 0
P    10  29  0  3 0
P    11  18  0  2 0
P    13  11  0  18 0
P    13  19  0  21 0
P    8  20  0  11 0
S    1  1 0 0 1
S    1  29 0 53 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 41 0
R    2  15 0 0 1
R    12  15 0 41 0
A    1  12 0 0 1
A    13  17 0 50 0
A    13  25 0 42 0
A    9  12 0 38 0
D    11  7 0 0 1
D    12  14 0 54 0
D    4  19 0 0 1
D    13  22 0 39 0

actions_asked
player action direction
0 m b
1 m l
2 m r
3 m l

actions_done
player action direction
0 m b
1 m l
2 m r
3 m l


round 106

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X.PXXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX.PX.......X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  14  3  3  2  0  2900  0  a
1  1  10  19  3  3  1  0  1275  0  a
2  2  4  12  4  3  1  0  2875  0  a
3  3  4  19  5  6  3  2  1100  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    10  13  0  8 0
P    3  12  100  0 1
P    4  16  100  0 1
P    10  21  0  5 0
P    4  1  500  0 1
P    4  6  0  43 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  41 0
P    7  12  0  4 0
P    10  11  300  0 1
P    13  6  0  22 0
P    11  19  400  0 1
P    9  15  300  0 1
P    13  22  0  17 0
P    10  29  0  2 0
P    11  18  0  1 0
P    13  11  0  17 0
P    13  19  0  20 0
P    8  20  0  10 0
S    1  1 0 0 1
S    1  29 0 52 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 40 0
R    2  15 0 0 1
R    12  15 0 40 0
A    1  12 0 0 1
A    13  17 0 49 0
A    13  25 0 41 0
A    9  12 0 37 0
D    11  7 0 0 1
D    12  14 0 53 0
D    4  19 0 54 0
D    13  22 0 38 0

actions_asked
player action direction
0 m l
1 m b
2 m t
3 m l

actions_done
player action direction
0 m l
1 m b
2 m t
3 m l


round 107

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  13  3  3  2  0  2900  0  a
1  1  11  19  3  3  1  0  1675  0  a
2  2  3  12  4  3  1  0  2975  0  a
3  3  4  18  5  6  3  2  1100  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    10  13  0  7 0
P    3  12  0  49 0
P    4  16  100  0 1
P    10  21  0  4 0
P    4  1  500  0 1
P    4  6  0  42 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  40 0
P    7  12  0  3 0
P    10  11  300  0 1
P    13  6  0  21 0
P    11  19  0  49 0
P    9  15  300  0 1
P    13  22  0  16 0
P    10  29  0  1 0
P    12  25  300  0 1
P    13  11  0  16 0
P    13  19  0  19 0
P    8  20  0  9 0
S    1  1 0 0 1
S    1  29 0 51 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 39 0
R    2  15 0 0 1
R    12  15 0 39 0
A    1  12 0 0 1
A    13  17 0 48 0
A    13  25 0 40 0
A    9  12 0 36 0
D    11  7 0 0 1
D    12  14 0 52 0
D    4  19 0 53 0
D    13  22 0 37 0

actions_asked
player action direction
0 m l
1 m l
2 m b
3 m l

actions_done
player action direction
0 m l
1 m l
2 m b
3 m l


round 108

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  12  3  3  2  0  2900  0  a
1  1  11  18  3  3  1  0  1675  0  a
2  2  4  12  4  3  1  0  2975  0  a
3  3  4  17  5  6  3  2  1100  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    10  13  0  6 0
P    3  12  0  48 0
P    4  16  100  0 1
P    10  21  0  3 0
P    4  1  500  0 1
P    4  6  0  41 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  39 0
P    7  12  0  2 0
P    10  11  300  0 1
P    13  6  0  20 0
P    11  19  0  48 0
P    9  15  300  0 1
P    13  22  0  15 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  15 0
P    13  19  0  18 0
P    8  20  0  8 0
S    1  1 0 0 1
S    1  29 0 50 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 38 0
R    2  15 0 0 1
R    12  15 0 38 0
A    1  12 0 0 1
A    13  17 0 47 0
A    13  25 0 39 0
A    9  12 0 35 0
D    11  7 0 0 1
D    12  14 0 51 0
D    4  19 0 52 0
D    13  22 0 36 0

actions_asked
player action direction
0 m l
1 m t
2 m b
3 m l

actions_done
player action direction
0 m l
1 m t
2 m b
3 m l


round 109

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  11  3  3  2  0  2900  0  a
1  1  10  18  3  3  1  0  1675  0  a
2  2  5  12  4  3  1  0  2975  0  a
3  3  4  16  5  6  3  2  1200  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    10  13  0  5 0
P    3  12  0  47 0
P    4  16  0  49 0
P    10  21  0  2 0
P    4  1  500  0 1
P    4  6  0  40 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  38 0
P    7  12  0  1 0
P    10  11  300  0 1
P    13  6  0  19 0
P    11  19  0  47 0
P    9  15  300  0 1
P    13  22  0  14 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  14 0
P    13  19  0  17 0
P    8  20  0  7 0
S    1  1 0 0 1
S    1  29 0 49 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 37 0
R    2  15 0 0 1
R    12  15 0 37 0
A    1  12 0 0 1
A    13  17 0 46 0
A    13  25 0 38 0
A    9  12 0 34 0
D    11  7 0 0 1
D    12  14 0 50 0
D    4  19 0 51 0
D    13  22 0 35 0

actions_asked
player action direction
0 m l
1 m t
2 m b
3 m r

actions_done
player action direction
0 m l
1 m t
2 m b
3 m r


round 110

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  10  3  3  2  0  2900  0  a
1  1  9  18  3  3  1  0  1675  0  a
2  2  6  12  4  3  1  0  2975  0  a
3  3  4  17  5  6  3  2  1200  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    10  13  0  4 0
P    3  12  0  46 0
P    4  16  0  48 0
P    10  21  0  1 0
P    4  1  500  0 1
P    4  6  0  39 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  37 0
P    3  8  400  0 1
P    10  11  300  0 1
P    13  6  0  18 0
P    11  19  0  46 0
P    9  15  300  0 1
P    13  22  0  13 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  13 0
P    13  19  0  16 0
P    8  20  0  6 0
S    1  1 0 0 1
S    1  29 0 48 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 36 0
R    2  15 0 0 1
R    12  15 0 36 0
A    1  12 0 0 1
A    13  17 0 45 0
A    13  25 0 37 0
A    9  12 0 33 0
D    11  7 0 0 1
D    12  14 0 49 0
D    4  19 0 50 0
D    13  22 0 34 0

actions_asked
player action direction
0 m l
1 m t
2 m b
3 m r

actions_done
player action direction
0 m l
1 m t
2 m b
3 m r


round 111

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  9  3  3  2  0  2900  0  a
1  1  8  18  3  3  1  0  1675  0  a
2  2  7  12  4  3  1  0  2975  0  a
3  3  4  18  5  6  3  2  1200  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    10  13  0  3 0
P    3  12  0  45 0
P    4  16  0  47 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  38 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  36 0
P    3  8  400  0 1
P    10  11  300  0 1
P    13  6  0  17 0
P    11  19  0  45 0
P    9  15  300  0 1
P    13  22  0  12 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  12 0
P    13  19  0  15 0
P    8  20  0  5 0
S    1  1 0 0 1
S    1  29 0 47 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 35 0
R    2  15 0 0 1
R    12  15 0 35 0
A    1  12 0 0 1
A    13  17 0 44 0
A    13  25 0 36 0
A    9  12 0 32 0
D    11  7 0 0 1
D    12  14 0 48 0
D    4  19 0 49 0
D    13  22 0 33 0

actions_asked
player action direction
0 m l
1 m t
2 m b
3 m b

actions_done
player action direction
0 m l
1 m t
2 m b
3 m b


round 112

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.XPX.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  8  3  3  2  0  2900  0  a
1  1  7  18  3  3  1  0  1675  0  a
2  2  8  12  4  3  1  0  2975  0  a
3  3  5  18  5  6  3  2  1200  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    10  13  0  2 0
P    3  12  0  44 0
P    4  16  0  46 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  37 0
P    5  15  300  0 1
P    6  18  500  0 1
P    4  27  0  35 0
P    3  8  400  0 1
P    10  11  300  0 1
P    13  6  0  16 0
P    11  19  0  44 0
P    9  15  300  0 1
P    13  22  0  11 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  11 0
P    13  19  0  14 0
P    8  20  0  4 0
S    1  1 0 0 1
S    1  29 0 46 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 34 0
R    2  15 0 0 1
R    12  15 0 34 0
A    1  12 0 0 1
A    13  17 0 43 0
A    13  25 0 35 0
A    9  12 0 31 0
D    11  7 0 0 1
D    12  14 0 47 0
D    4  19 0 48 0
D    13  22 0 32 0

actions_asked
player action direction
0 m l
1 m t
2 m b
3 m b

actions_done
player action direction
0 m l
1 m t
2 m b
3 u n


round 113

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  13  7  3  3  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  0  a
2  2  9  12  4  3  1  0  2975  0  a
3  3  5  18  5  6  3  2  1200  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    10  13  0  1 0
P    3  12  0  43 0
P    4  16  0  45 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  36 0
P    5  15  300  0 1
P    6  18  0  49 0
P    4  27  0  34 0
P    3  8  400  0 1
P    10  11  300  0 1
P    13  6  0  15 0
P    11  19  0  43 0
P    9  15  300  0 1
P    13  22  0  10 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  10 0
P    13  19  0  13 0
P    8  20  0  3 0
S    1  1 0 0 1
S    1  29 0 45 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 33 0
R    2  15 0 0 1
R    12  15 0 33 0
A    1  12 0 0 1
A    13  17 0 42 0
A    13  25 0 34 0
A    9  12 0 30 0
D    11  7 0 0 1
D    12  14 0 46 0
D    4  19 0 47 0
D    13  22 0 31 0

actions_asked
player action direction
0 m t
1 m b
2 m b
3 a b

actions_done
player action direction
0 m t
1 u n
2 m b
3 a b


round 114

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P................X.X
11 X.X....D..X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  12  7  3  3  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  39  d
2  2  10  12  4  3  1  0  2975  0  a
3  3  5  18  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  42 0
P    4  16  0  44 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  35 0
P    5  15  300  0 1
P    6  18  0  48 0
P    4  27  0  33 0
P    3  8  400  0 1
P    10  11  300  0 1
P    13  6  0  14 0
P    11  19  0  42 0
P    9  15  300  0 1
P    13  22  0  9 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  9 0
P    13  19  0  12 0
P    8  20  0  2 0
S    1  1 0 0 1
S    1  29 0 44 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 32 0
R    2  15 0 0 1
R    12  15 0 32 0
A    1  12 0 0 1
A    13  17 0 41 0
A    13  25 0 33 0
A    9  12 0 29 0
D    11  7 0 0 1
D    12  14 0 45 0
D    4  19 0 46 0
D    13  22 0 30 0

actions_asked
player action direction
0 m t
1 u n
2 m l
3 m t

actions_done
player action direction
0 m t
1 u n
2 m l
3 m t


round 115

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  7  3  4  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  38  d
2  2  10  11  4  3  1  0  3275  0  a
3  3  4  18  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  41 0
P    4  16  0  43 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  34 0
P    5  15  300  0 1
P    6  18  0  47 0
P    4  27  0  32 0
P    3  8  400  0 1
P    10  11  0  49 0
P    13  6  0  13 0
P    11  19  0  41 0
P    9  15  300  0 1
P    13  22  0  8 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  8 0
P    13  19  0  11 0
P    8  20  0  1 0
S    1  1 0 0 1
S    1  29 0 43 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 31 0
R    2  15 0 0 1
R    12  15 0 31 0
A    1  12 0 0 1
A    13  17 0 40 0
A    13  25 0 32 0
A    9  12 0 28 0
D    11  7 0 54 0
D    12  14 0 44 0
D    4  19 0 45 0
D    13  22 0 29 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m r

actions_done
player action direction
0 m l
1 u n
2 m r
3 m r


round 116

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  6  3  4  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  37  d
2  2  10  12  4  3  1  0  3275  0  a
3  3  4  19  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  40 0
P    4  16  0  42 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  33 0
P    5  15  300  0 1
P    6  18  0  46 0
P    4  27  0  31 0
P    3  8  400  0 1
P    10  11  0  48 0
P    13  6  0  12 0
P    11  19  0  40 0
P    9  15  300  0 1
P    13  22  0  7 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  7 0
P    13  19  0  10 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 42 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 30 0
R    2  15 0 0 1
R    12  15 0 30 0
A    1  12 0 0 1
A    13  17 0 39 0
A    13  25 0 31 0
A    9  12 0 27 0
D    11  7 0 53 0
D    12  14 0 43 0
D    4  19 0 44 0
D    13  22 0 28 0

actions_asked
player action direction
0 m l
1 u n
2 m t
3 m r

actions_done
player action direction
0 m l
1 u n
2 m t
3 m r


round 117

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  5  3  4  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  36  d
2  2  9  12  4  3  1  0  3275  0  a
3  3  4  20  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  39 0
P    4  16  0  41 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  32 0
P    5  15  300  0 1
P    6  18  0  45 0
P    4  27  0  30 0
P    3  8  400  0 1
P    10  11  0  47 0
P    13  6  0  11 0
P    11  19  0  39 0
P    9  15  300  0 1
P    13  22  0  6 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  6 0
P    13  19  0  9 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 41 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 29 0
R    2  15 0 0 1
R    12  15 0 29 0
A    1  12 0 0 1
A    13  17 0 38 0
A    13  25 0 30 0
A    9  12 0 26 0
D    11  7 0 52 0
D    12  14 0 42 0
D    4  19 0 43 0
D    13  22 0 27 0

actions_asked
player action direction
0 m l
1 u n
2 m t
3 m r

actions_done
player action direction
0 m l
1 u n
2 m t
3 m r


round 118

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  4  3  4  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  35  d
2  2  8  12  4  3  1  0  3275  0  a
3  3  4  21  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  38 0
P    4  16  0  40 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  31 0
P    5  15  300  0 1
P    6  18  0  44 0
P    4  27  0  29 0
P    3  8  400  0 1
P    10  11  0  46 0
P    13  6  0  10 0
P    11  19  0  38 0
P    9  15  300  0 1
P    13  22  0  5 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  5 0
P    13  19  0  8 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 40 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 28 0
R    2  15 0 0 1
R    12  15 0 28 0
A    1  12 0 0 1
A    13  17 0 37 0
A    13  25 0 29 0
A    9  12 0 25 0
D    11  7 0 51 0
D    12  14 0 41 0
D    4  19 0 42 0
D    13  22 0 26 0

actions_asked
player action direction
0 m l
1 u n
2 m t
3 m r

actions_done
player action direction
0 m l
1 u n
2 m t
3 m r


round 119

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  11  3  3  4  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  34  d
2  2  7  12  4  3  1  0  3275  0  a
3  3  4  22  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  37 0
P    4  16  0  39 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  30 0
P    5  15  300  0 1
P    6  18  0  43 0
P    4  27  0  28 0
P    3  8  400  0 1
P    10  11  0  45 0
P    13  6  0  9 0
P    11  19  0  37 0
P    9  15  300  0 1
P    13  22  0  4 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  4 0
P    13  19  0  7 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 39 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 27 0
R    2  15 0 0 1
R    12  15 0 27 0
A    1  12 0 0 1
A    13  17 0 36 0
A    13  25 0 28 0
A    9  12 0 24 0
D    11  7 0 50 0
D    12  14 0 40 0
D    4  19 0 41 0
D    13  22 0 25 0

actions_asked
player action direction
0 m t
1 u n
2 m t
3 m r

actions_done
player action direction
0 m t
1 u n
2 m t
3 m r


round 120

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  3  3  4  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  33  d
2  2  6  12  4  3  1  0  3275  0  a
3  3  4  23  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  36 0
P    4  16  0  38 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  29 0
P    5  15  300  0 1
P    6  18  0  42 0
P    4  27  0  27 0
P    3  8  400  0 1
P    10  11  0  44 0
P    13  6  0  8 0
P    11  19  0  36 0
P    9  15  300  0 1
P    13  22  0  3 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  3 0
P    13  19  0  6 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 38 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 26 0
R    2  15 0 0 1
R    12  15 0 26 0
A    1  12 0 0 1
A    13  17 0 35 0
A    13  25 0 27 0
A    9  12 0 23 0
D    11  7 0 49 0
D    12  14 0 39 0
D    4  19 0 40 0
D    13  22 0 24 0

actions_asked
player action direction
0 m t
1 u n
2 m t
3 m t

actions_done
player action direction
0 m t
1 u n
2 m t
3 m t


round 121

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.XP.X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  9  3  3  4  2  0  2900  0  a
1  1  6  18  3  3  1  0  2175  32  d
2  2  5  12  4  3  1  0  3275  0  a
3  3  3  23  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  35 0
P    4  16  0  37 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  28 0
P    5  15  300  0 1
P    6  18  0  41 0
P    4  27  0  26 0
P    3  8  400  0 1
P    10  11  0  43 0
P    13  6  0  7 0
P    11  19  0  35 0
P    9  15  300  0 1
P    13  22  0  2 0
P    8  3  300  0 1
P    12  25  300  0 1
P    13  11  0  2 0
P    13  19  0  5 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 37 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 25 0
R    2  15 0 0 1
R    12  15 0 25 0
A    1  12 0 0 1
A    13  17 0 34 0
A    13  25 0 26 0
A    9  12 0 22 0
D    11  7 0 48 0
D    12  14 0 38 0
D    4  19 0 39 0
D    13  22 0 23 0

actions_asked
player action direction
0 m t
1 u n
2 m t
3 m t

actions_done
player action direction
0 m t
1 u n
2 m t
3 m t


round 122

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A..........P....X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X.PXXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  3  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  31  d
2  2  4  12  4  3  1  0  3275  0  a
3  3  2  23  4  6  3  2  1526  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  23  500  0 1
P    3  12  0  34 0
P    4  16  0  36 0
P    3  12  300  0 1
P    4  1  500  0 1
P    4  6  0  27 0
P    5  15  300  0 1
P    6  18  0  40 0
P    4  27  0  25 0
P    3  8  400  0 1
P    10  11  0  42 0
P    13  6  0  6 0
P    11  19  0  34 0
P    9  15  300  0 1
P    13  22  0  1 0
P    8  3  0  49 0
P    12  25  300  0 1
P    13  11  0  1 0
P    13  19  0  4 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 36 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 24 0
R    2  15 0 0 1
R    12  15 0 24 0
A    1  12 0 0 1
A    13  17 0 33 0
A    13  25 0 25 0
A    9  12 0 21 0
D    11  7 0 47 0
D    12  14 0 37 0
D    4  19 0 38 0
D    13  22 0 22 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m t

actions_done
player action direction
0 m r
1 u n
2 m t
3 m t


round 123

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X..XXMXX..X.......X.X
04 XPX.................P.......X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  4  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  30  d
2  2  3  12  4  3  1  0  3575  0  a
3  3  1  23  4  6  3  2  2026  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  23  0  49 0
P    3  12  0  33 0
P    4  16  0  35 0
P    3  12  0  49 0
P    4  1  500  0 1
P    4  6  0  26 0
P    5  15  300  0 1
P    6  18  0  39 0
P    4  27  0  24 0
P    3  8  400  0 1
P    10  11  0  41 0
P    13  6  0  5 0
P    11  19  0  33 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  48 0
P    12  25  300  0 1
P    4  20  400  0 1
P    13  19  0  3 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 35 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 23 0
R    2  15 0 0 1
R    12  15 0 23 0
A    1  12 0 0 1
A    13  17 0 32 0
A    13  25 0 24 0
A    9  12 0 20 0
D    11  7 0 46 0
D    12  14 0 36 0
D    4  19 0 37 0
D    13  22 0 21 0

actions_asked
player action direction
0 m b
1 u n
2 m l
3 m l

actions_done
player action direction
0 m b
1 u n
2 m l
3 m l


round 124

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X..XXMXX..X.......X.X
04 XPX.................P.......X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  9  4  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  29  d
2  2  3  11  4  3  1  0  3575  0  a
3  3  1  22  4  6  3  2  2026  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  23  0  48 0
P    3  12  0  32 0
P    4  16  0  34 0
P    3  12  0  48 0
P    4  1  500  0 1
P    4  6  0  25 0
P    5  15  300  0 1
P    6  18  0  38 0
P    4  27  0  23 0
P    3  8  400  0 1
P    10  11  0  40 0
P    13  6  0  4 0
P    11  19  0  32 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  47 0
P    12  25  300  0 1
P    4  20  400  0 1
P    13  19  0  2 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 34 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 22 0
R    2  15 0 0 1
R    12  15 0 22 0
A    1  12 0 0 1
A    13  17 0 31 0
A    13  25 0 23 0
A    9  12 0 19 0
D    11  7 0 45 0
D    12  14 0 35 0
D    4  19 0 36 0
D    13  22 0 20 0

actions_asked
player action direction
0 m b
1 u n
2 m b
3 m l

actions_done
player action direction
0 m b
1 u n
2 m b
3 m l


round 125

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X..XXMXX..X.......X.X
04 XPX.................P.......X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  4  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  28  d
2  2  4  11  4  3  1  0  3575  0  a
3  3  1  21  4  6  3  2  2026  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  23  0  47 0
P    3  12  0  31 0
P    4  16  0  33 0
P    3  12  0  47 0
P    4  1  500  0 1
P    4  6  0  24 0
P    5  15  300  0 1
P    6  18  0  37 0
P    4  27  0  22 0
P    3  8  400  0 1
P    10  11  0  39 0
P    13  6  0  3 0
P    11  19  0  31 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  46 0
P    12  25  300  0 1
P    4  20  400  0 1
P    13  19  0  1 0
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 33 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 21 0
R    2  15 0 0 1
R    12  15 0 21 0
A    1  12 0 0 1
A    13  17 0 30 0
A    13  25 0 22 0
A    9  12 0 18 0
D    11  7 0 44 0
D    12  14 0 34 0
D    4  19 0 35 0
D    13  22 0 19 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m b

actions_done
player action direction
0 m r
1 u n
2 m l
3 m b


round 126

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X..XXMXX..X.......X.X
04 XPX.................P.......X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  5  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  27  d
2  2  4  10  4  3  1  0  3575  0  a
3  3  2  21  4  6  3  2  2026  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  23  0  46 0
P    3  12  0  30 0
P    4  16  0  32 0
P    3  12  0  46 0
P    4  1  500  0 1
P    4  6  0  23 0
P    5  15  300  0 1
P    6  18  0  36 0
P    4  27  0  21 0
P    3  8  400  0 1
P    10  11  0  38 0
P    13  6  0  2 0
P    11  19  0  30 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  45 0
P    12  25  300  0 1
P    4  20  400  0 1
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 32 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 20 0
R    2  15 0 0 1
R    12  15 0 20 0
A    1  12 0 0 1
A    13  17 0 29 0
A    13  25 0 21 0
A    9  12 0 17 0
D    11  7 0 43 0
D    12  14 0 33 0
D    4  19 0 34 0
D    13  22 0 18 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m b

actions_done
player action direction
0 m r
1 u n
2 m l
3 m b


round 127

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.....P.X..XXMXX..X.......X.X
04 XPX.................P.......X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  6  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  26  d
2  2  4  9  4  3  1  0  3575  0  a
3  3  3  21  4  6  3  2  2026  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  23  0  45 0
P    3  12  0  29 0
P    4  16  0  31 0
P    3  12  0  45 0
P    4  1  500  0 1
P    4  6  0  22 0
P    5  15  300  0 1
P    6  18  0  35 0
P    4  27  0  20 0
P    3  8  400  0 1
P    10  11  0  37 0
P    13  6  0  1 0
P    11  19  0  29 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  44 0
P    12  25  300  0 1
P    4  20  400  0 1
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 31 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 19 0
R    2  15 0 0 1
R    12  15 0 19 0
A    1  12 0 0 1
A    13  17 0 28 0
A    13  25 0 20 0
A    9  12 0 16 0
D    11  7 0 42 0
D    12  14 0 32 0
D    4  19 0 33 0
D    13  22 0 17 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m b

actions_done
player action direction
0 m r
1 u n
2 m l
3 m b


round 128

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.....P.X..XXMXX..X.......X.X
04 XPX.................P.......X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  7  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  25  d
2  2  4  8  4  3  1  0  3575  0  a
3  3  4  21  4  6  3  2  2026  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  23  0  44 0
P    3  12  0  28 0
P    4  16  0  30 0
P    3  12  0  44 0
P    4  1  500  0 1
P    4  6  0  21 0
P    5  15  300  0 1
P    6  18  0  34 0
P    4  27  0  19 0
P    3  8  400  0 1
P    10  11  0  36 0
P    3  1  500  0 1
P    11  19  0  28 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  43 0
P    12  25  300  0 1
P    4  20  400  0 1
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 30 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 18 0
R    2  15 0 0 1
R    12  15 0 18 0
A    1  12 0 0 1
A    13  17 0 27 0
A    13  25 0 19 0
A    9  12 0 15 0
D    11  7 0 41 0
D    12  14 0 31 0
D    4  19 0 32 0
D    13  22 0 16 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m l

actions_done
player action direction
0 m r
1 u n
2 m t
3 m l


round 129

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  8  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  24  d
2  2  3  8  4  3  1  0  3975  0  a
3  3  4  20  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  23  0  43 0
P    3  12  0  27 0
P    4  16  0  29 0
P    3  12  0  43 0
P    4  1  500  0 1
P    4  6  0  20 0
P    5  15  300  0 1
P    6  18  0  33 0
P    4  27  0  18 0
P    3  8  0  49 0
P    10  11  0  35 0
P    3  1  500  0 1
P    11  19  0  27 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  42 0
P    12  25  300  0 1
P    4  20  0  49 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 29 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 17 0
R    2  15 0 0 1
R    12  15 0 17 0
A    1  12 0 0 1
A    13  17 0 26 0
A    13  25 0 18 0
A    9  12 0 14 0
D    11  7 0 40 0
D    12  14 0 30 0
D    4  19 0 31 0
D    13  22 0 15 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 130

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  9  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  23  d
2  2  3  9  4  3  1  0  3975  0  a
3  3  4  21  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  23  0  42 0
P    3  12  0  26 0
P    4  16  0  28 0
P    3  12  0  42 0
P    4  1  500  0 1
P    4  6  0  19 0
P    5  15  300  0 1
P    6  18  0  32 0
P    4  27  0  17 0
P    3  8  0  48 0
P    10  11  0  34 0
P    3  1  500  0 1
P    11  19  0  26 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  41 0
P    12  25  300  0 1
P    4  20  0  48 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 28 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 16 0
R    2  15 0 0 1
R    12  15 0 16 0
A    1  12 0 0 1
A    13  17 0 25 0
A    13  25 0 17 0
A    9  12 0 13 0
D    11  7 0 39 0
D    12  14 0 29 0
D    4  19 0 30 0
D    13  22 0 14 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m r

actions_done
player action direction
0 m r
1 u n
2 m t
3 m r


round 131

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X........P.............P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  10  3  4  2  0  3200  0  a
1  1  6  18  3  3  1  0  2175  22  d
2  2  2  9  4  3  1  0  3975  0  a
3  3  4  22  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  23  0  41 0
P    3  12  0  25 0
P    4  16  0  27 0
P    3  12  0  41 0
P    4  1  500  0 1
P    4  6  0  18 0
P    5  15  300  0 1
P    6  18  0  31 0
P    4  27  0  16 0
P    3  8  0  47 0
P    10  11  0  33 0
P    3  1  500  0 1
P    11  19  0  25 0
P    9  15  300  0 1
P    10  11  300  0 1
P    8  3  0  40 0
P    12  25  300  0 1
P    4  20  0  47 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 27 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 15 0
R    2  15 0 0 1
R    12  15 0 15 0
A    1  12 0 0 1
A    13  17 0 24 0
A    13  25 0 16 0
A    9  12 0 12 0
D    11  7 0 38 0
D    12  14 0 28 0
D    4  19 0 29 0
D    13  22 0 13 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m r

actions_done
player action direction
0 m r
1 u n
2 m t
3 m r


round 132

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  11  3  4  2  0  3500  0  a
1  1  6  18  3  3  1  0  2175  21  d
2  2  1  9  4  3  1  0  3975  0  a
3  3  4  23  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  23  0  40 0
P    3  12  0  24 0
P    4  16  0  26 0
P    3  12  0  40 0
P    4  1  500  0 1
P    4  6  0  17 0
P    5  15  300  0 1
P    6  18  0  30 0
P    4  27  0  15 0
P    3  8  0  46 0
P    10  11  0  32 0
P    3  1  500  0 1
P    11  19  0  24 0
P    9  15  300  0 1
P    10  11  0  49 0
P    8  3  0  39 0
P    12  25  300  0 1
P    4  20  0  46 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 26 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 14 0
R    2  15 0 0 1
R    12  15 0 14 0
A    1  12 0 0 1
A    13  17 0 23 0
A    13  25 0 15 0
A    9  12 0 11 0
D    11  7 0 37 0
D    12  14 0 27 0
D    4  19 0 28 0
D    13  22 0 12 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 133

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  12  3  4  2  0  3500  0  a
1  1  6  18  3  3  1  0  2175  20  d
2  2  1  10  4  3  1  0  3975  0  a
3  3  4  24  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  23  0  39 0
P    3  12  0  23 0
P    4  16  0  25 0
P    3  12  0  39 0
P    4  1  500  0 1
P    4  6  0  16 0
P    5  15  300  0 1
P    6  18  0  29 0
P    4  27  0  14 0
P    3  8  0  45 0
P    10  11  0  31 0
P    3  1  500  0 1
P    11  19  0  23 0
P    9  15  300  0 1
P    10  11  0  48 0
P    8  3  0  38 0
P    12  25  300  0 1
P    4  20  0  45 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 25 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 13 0
R    2  15 0 0 1
R    12  15 0 13 0
A    1  12 0 0 1
A    13  17 0 22 0
A    13  25 0 14 0
A    9  12 0 10 0
D    11  7 0 36 0
D    12  14 0 26 0
D    4  19 0 27 0
D    13  22 0 11 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 134

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........A...............X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  13  3  4  2  0  3500  0  a
1  1  6  18  3  3  1  0  2175  19  d
2  2  1  11  4  3  1  0  3975  0  a
3  3  4  25  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  23  0  38 0
P    3  12  0  22 0
P    4  16  0  24 0
P    3  12  0  38 0
P    4  1  500  0 1
P    4  6  0  15 0
P    5  15  300  0 1
P    6  18  0  28 0
P    4  27  0  13 0
P    3  8  0  44 0
P    10  11  0  30 0
P    3  1  500  0 1
P    11  19  0  22 0
P    9  15  300  0 1
P    10  11  0  47 0
P    8  3  0  37 0
P    12  25  300  0 1
P    4  20  0  44 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 24 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 12 0
R    2  15 0 0 1
R    12  15 0 12 0
A    1  12 0 0 1
A    13  17 0 21 0
A    13  25 0 13 0
A    9  12 0 9 0
D    11  7 0 35 0
D    12  14 0 25 0
D    4  19 0 26 0
D    13  22 0 10 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 135

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  14  3  4  2  0  3500  0  a
1  1  6  18  3  3  1  0  2175  18  d
2  2  1  12  5  3  1  0  3975  0  a
3  3  4  26  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  23  0  37 0
P    3  12  0  21 0
P    4  16  0  23 0
P    3  12  0  37 0
P    4  1  500  0 1
P    4  6  0  14 0
P    5  15  300  0 1
P    6  18  0  27 0
P    4  27  0  12 0
P    3  8  0  43 0
P    10  11  0  29 0
P    3  1  500  0 1
P    11  19  0  21 0
P    9  15  300  0 1
P    10  11  0  46 0
P    8  3  0  36 0
P    12  25  300  0 1
P    4  20  0  43 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 23 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 11 0
R    2  15 0 0 1
R    12  15 0 11 0
A    1  12 0 54 0
A    13  17 0 20 0
A    13  25 0 12 0
A    9  12 0 8 0
D    11  7 0 34 0
D    12  14 0 24 0
D    4  19 0 25 0
D    13  22 0 9 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m b

actions_done
player action direction
0 m r
1 u n
2 m l
3 m b


round 136

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX...P...XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  10  15  3  4  2  0  3500  0  a
1  1  6  18  3  3  1  0  2175  17  d
2  2  1  11  5  3  1  0  3975  0  a
3  3  5  26  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  23  0  36 0
P    3  12  0  20 0
P    4  16  0  22 0
P    3  12  0  36 0
P    4  1  500  0 1
P    4  6  0  13 0
P    5  15  300  0 1
P    6  18  0  26 0
P    4  27  0  11 0
P    3  8  0  42 0
P    10  11  0  28 0
P    3  1  500  0 1
P    11  19  0  20 0
P    9  15  300  0 1
P    10  11  0  45 0
P    8  3  0  35 0
P    12  25  300  0 1
P    4  20  0  42 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 22 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 10 0
R    2  15 0 0 1
R    12  15 0 10 0
A    1  12 0 53 0
A    13  17 0 19 0
A    13  25 0 11 0
A    9  12 0 7 0
D    11  7 0 33 0
D    12  14 0 23 0
D    4  19 0 24 0
D    13  22 0 8 0

actions_asked
player action direction
0 m t
1 u n
2 m l
3 m b

actions_done
player action direction
0 m t
1 u n
2 m l
3 m b


round 137

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  9  15  3  4  2  0  3800  0  a
1  1  6  18  3  3  1  0  2175  16  d
2  2  1  10  5  3  1  0  3975  0  a
3  3  6  26  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  23  0  35 0
P    3  12  0  19 0
P    4  16  0  21 0
P    3  12  0  35 0
P    4  1  500  0 1
P    4  6  0  12 0
P    5  15  300  0 1
P    6  18  0  25 0
P    4  27  0  10 0
P    3  8  0  41 0
P    10  11  0  27 0
P    3  1  500  0 1
P    11  19  0  19 0
P    9  15  0  49 0
P    10  11  0  44 0
P    8  3  0  34 0
P    12  25  300  0 1
P    4  20  0  41 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 21 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 9 0
R    2  15 0 0 1
R    12  15 0 9 0
A    1  12 0 52 0
A    13  17 0 18 0
A    13  25 0 10 0
A    9  12 0 6 0
D    11  7 0 32 0
D    12  14 0 22 0
D    4  19 0 23 0
D    13  22 0 7 0

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m b

actions_done
player action direction
0 m r
1 u n
2 u n
3 m b


round 138

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  9  16  3  4  2  0  3800  0  a
1  1  6  18  3  3  1  0  2175  15  d
2  2  1  10  5  3  1  0  3975  0  a
3  3  7  26  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  23  0  34 0
P    3  12  0  18 0
P    4  16  0  20 0
P    3  12  0  34 0
P    4  1  500  0 1
P    4  6  0  11 0
P    5  15  300  0 1
P    6  18  0  24 0
P    4  27  0  9 0
P    3  8  0  40 0
P    10  11  0  26 0
P    3  1  500  0 1
P    11  19  0  18 0
P    9  15  0  48 0
P    10  11  0  43 0
P    8  3  0  33 0
P    12  25  300  0 1
P    4  20  0  40 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 20 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 8 0
R    2  15 0 0 1
R    12  15 0 8 0
A    1  12 0 51 0
A    13  17 0 17 0
A    13  25 0 9 0
A    9  12 0 5 0
D    11  7 0 31 0
D    12  14 0 21 0
D    4  19 0 22 0
D    13  22 0 6 0

actions_asked
player action direction
0 m t
1 u n
2 m r
3 m b

actions_done
player action direction
0 m t
1 u n
2 m r
3 m b


round 139

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  16  3  4  2  0  3800  0  a
1  1  6  18  3  3  1  0  2175  14  d
2  2  1  11  5  3  1  0  3975  0  a
3  3  8  26  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  23  0  33 0
P    3  12  0  17 0
P    4  16  0  19 0
P    3  12  0  33 0
P    4  1  500  0 1
P    4  6  0  10 0
P    5  15  300  0 1
P    6  18  0  23 0
P    4  27  0  8 0
P    3  8  0  39 0
P    10  11  0  25 0
P    3  1  500  0 1
P    11  19  0  17 0
P    9  15  0  47 0
P    10  11  0  42 0
P    8  3  0  32 0
P    12  25  300  0 1
P    4  20  0  39 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 19 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 7 0
R    2  15 0 0 1
R    12  15 0 7 0
A    1  12 0 50 0
A    13  17 0 16 0
A    13  25 0 8 0
A    9  12 0 4 0
D    11  7 0 30 0
D    12  14 0 20 0
D    4  19 0 21 0
D    13  22 0 5 0

actions_asked
player action direction
0 m t
1 u n
2 m l
3 m b

actions_done
player action direction
0 m t
1 u n
2 m l
3 m b


round 140

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X.S.X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  16  3  4  2  0  3800  0  a
1  1  6  18  3  3  1  0  2175  13  d
2  2  1  10  5  3  1  0  3975  0  a
3  3  9  26  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  23  0  32 0
P    3  12  0  16 0
P    4  16  0  18 0
P    3  12  0  32 0
P    4  1  500  0 1
P    4  6  0  9 0
P    5  15  300  0 1
P    6  18  0  22 0
P    4  27  0  7 0
P    3  8  0  38 0
P    10  11  0  24 0
P    3  1  500  0 1
P    11  19  0  16 0
P    9  15  0  46 0
P    10  11  0  41 0
P    8  3  0  31 0
P    12  25  300  0 1
P    4  20  0  38 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 18 0
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 6 0
R    2  15 0 0 1
R    12  15 0 6 0
A    1  12 0 49 0
A    13  17 0 15 0
A    13  25 0 7 0
A    9  12 0 3 0
D    11  7 0 29 0
D    12  14 0 19 0
D    4  19 0 20 0
D    13  22 0 4 0

actions_asked
player action direction
0 m l
1 u n
2 m b
3 m b

actions_done
player action direction
0 m l
1 u n
2 u n
3 m b


round 141

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X......................P..X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  15  5  6  3  1  3800  0  a
1  1  6  18  3  3  1  0  2175  12  d
2  2  1  10  5  3  1  0  3975  0  a
3  3  10  26  4  6  3  2  2426  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  23  0  31 0
P    3  12  0  15 0
P    4  16  0  17 0
P    3  12  0  31 0
P    4  1  500  0 1
P    4  6  0  8 0
P    5  15  300  0 1
P    6  18  0  21 0
P    4  27  0  6 0
P    3  8  0  37 0
P    10  11  0  23 0
P    3  1  500  0 1
P    11  19  0  15 0
P    9  15  0  45 0
P    10  11  0  40 0
P    8  3  0  30 0
P    12  25  300  0 1
P    4  20  0  37 0
P    7  1  500  0 1
P    10  25  100  0 1
S    1  1 0 0 1
S    1  29 0 17 0
S    7  15 0 84 0
S    13  1 0 0 1
S    13  29 0 5 0
R    2  15 0 0 1
R    12  15 0 5 0
A    1  12 0 48 0
A    13  17 0 14 0
A    13  25 0 6 0
A    9  12 0 2 0
D    11  7 0 28 0
D    12  14 0 18 0
D    4  19 0 19 0
D    13  22 0 3 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m l

actions_done
player action direction
0 m r
1 u n
2 m l
3 m l


round 142

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  16  5  6  3  1  3800  0  a
1  1  6  18  3  3  1  0  2175  11  d
2  2  1  9  5  3  1  0  3975  0  a
3  3  10  25  4  6  3  2  2526  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  23  0  30 0
P    3  12  0  14 0
P    4  16  0  16 0
P    3  12  0  30 0
P    4  1  500  0 1
P    4  6  0  7 0
P    5  15  300  0 1
P    6  18  0  20 0
P    4  27  0  5 0
P    3  8  0  36 0
P    10  11  0  22 0
P    3  1  500  0 1
P    11  19  0  14 0
P    9  15  0  44 0
P    10  11  0  39 0
P    8  3  0  29 0
P    12  25  300  0 1
P    4  20  0  36 0
P    7  1  500  0 1
P    10  25  0  49 0
S    1  1 0 0 1
S    1  29 0 16 0
S    7  15 0 83 0
S    13  1 0 0 1
S    13  29 0 4 0
R    2  15 0 0 1
R    12  15 0 4 0
A    1  12 0 47 0
A    13  17 0 13 0
A    13  25 0 5 0
A    9  12 0 1 0
D    11  7 0 27 0
D    12  14 0 17 0
D    4  19 0 18 0
D    13  22 0 2 0

actions_asked
player action direction
0 m t
1 u n
2 m l
3 m b

actions_done
player action direction
0 m t
1 u n
2 m l
3 m b


round 143

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.A.....X.X
12 X.X.......XXXX...XXXX....P..X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  16  5  6  3  1  3800  0  a
1  1  6  18  3  3  1  0  2175  10  d
2  2  1  8  5  3  1  0  3975  0  a
3  3  11  25  4  6  3  2  2526  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  23  0  29 0
P    3  12  0  13 0
P    4  16  0  15 0
P    3  12  0  29 0
P    4  1  500  0 1
P    4  6  0  6 0
P    5  15  300  0 1
P    6  18  0  19 0
P    4  27  0  4 0
P    3  8  0  35 0
P    10  11  0  21 0
P    3  1  500  0 1
P    11  19  0  13 0
P    9  15  0  43 0
P    10  11  0  38 0
P    8  3  0  28 0
P    12  25  300  0 1
P    4  20  0  35 0
P    7  1  500  0 1
P    10  25  0  48 0
S    1  1 0 0 1
S    1  29 0 15 0
S    7  15 0 82 0
S    13  1 0 0 1
S    13  29 0 3 0
R    2  15 0 0 1
R    12  15 0 3 0
A    1  12 0 46 0
A    13  17 0 12 0
A    13  25 0 4 0
A    11  22 0 0 1
D    11  7 0 26 0
D    12  14 0 16 0
D    4  19 0 17 0
D    13  22 0 1 0

actions_asked
player action direction
0 m t
1 u n
2 m l
3 m b

actions_done
player action direction
0 m t
1 u n
2 m l
3 m b


round 144

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X.A.....X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  16  5  6  3  1  3800  0  a
1  1  6  18  3  3  1  0  2175  9  d
2  2  1  7  5  3  1  0  3975  0  a
3  3  12  25  4  6  3  2  2826  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  23  0  28 0
P    3  12  0  12 0
P    4  16  0  14 0
P    3  12  0  28 0
P    4  1  500  0 1
P    4  6  0  5 0
P    5  15  300  0 1
P    6  18  0  18 0
P    4  27  0  3 0
P    3  8  0  34 0
P    10  11  0  20 0
P    3  1  500  0 1
P    11  19  0  12 0
P    9  15  0  42 0
P    10  11  0  37 0
P    8  3  0  27 0
P    12  25  0  49 0
P    4  20  0  34 0
P    7  1  500  0 1
P    10  25  0  47 0
S    1  1 0 0 1
S    1  29 0 14 0
S    7  15 0 81 0
S    13  1 0 0 1
S    13  29 0 2 0
R    2  15 0 0 1
R    12  15 0 2 0
A    1  12 0 45 0
A    13  17 0 11 0
A    13  25 0 3 0
A    11  22 0 0 1
D    11  7 0 25 0
D    12  14 0 15 0
D    4  19 0 16 0
D    11  7 0 0 1

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 145

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X.A.....X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................X.X
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  15  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  8  d
2  2  1  6  5  3  1  0  3975  0  a
3  3  12  24  4  6  3  2  2826  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  23  0  27 0
P    3  12  0  11 0
P    4  16  0  13 0
P    3  12  0  27 0
P    4  1  500  0 1
P    4  6  0  4 0
P    5  15  0  49 0
P    6  18  0  17 0
P    4  27  0  2 0
P    3  8  0  33 0
P    10  11  0  19 0
P    3  1  500  0 1
P    11  19  0  11 0
P    9  15  0  41 0
P    10  11  0  36 0
P    8  3  0  26 0
P    12  25  0  48 0
P    4  20  0  33 0
P    7  1  500  0 1
P    10  25  0  46 0
S    1  1 0 0 1
S    1  29 0 13 0
S    7  15 0 80 0
S    13  1 0 0 1
S    13  29 0 1 0
R    2  15 0 0 1
R    12  15 0 1 0
A    1  12 0 44 0
A    13  17 0 10 0
A    13  25 0 2 0
A    11  22 0 0 1
D    11  7 0 24 0
D    12  14 0 14 0
D    4  19 0 15 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m l

actions_done
player action direction
0 m r
1 u n
2 m l
3 m l


round 146

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...R.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X.A.....X.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  16  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  7  d
2  2  1  5  5  3  1  0  3975  0  a
3  3  12  23  4  6  3  2  2826  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  23  0  26 0
P    3  12  0  10 0
P    4  16  0  12 0
P    3  12  0  26 0
P    4  1  500  0 1
P    4  6  0  3 0
P    5  15  0  48 0
P    6  18  0  16 0
P    4  27  0  1 0
P    3  8  0  32 0
P    10  11  0  18 0
P    3  1  500  0 1
P    11  19  0  10 0
P    9  15  0  40 0
P    10  11  0  35 0
P    8  3  0  25 0
P    12  25  0  47 0
P    4  20  0  32 0
P    7  1  500  0 1
P    10  25  0  45 0
S    1  1 0 0 1
S    1  29 0 12 0
S    7  15 0 79 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 0 1
A    1  12 0 43 0
A    13  17 0 9 0
A    13  25 0 1 0
A    11  22 0 0 1
D    11  7 0 23 0
D    12  14 0 13 0
D    4  19 0 14 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m l

actions_done
player action direction
0 m r
1 u n
2 m l
3 m l


round 147

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...R.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X.A....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  17  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  6  d
2  2  1  4  5  3  1  0  3975  0  a
3  3  12  22  4  6  3  2  2826  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  23  0  25 0
P    3  12  0  9 0
P    4  16  0  11 0
P    3  12  0  25 0
P    4  1  500  0 1
P    4  6  0  2 0
P    5  15  0  47 0
P    6  18  0  15 0
P    11  27  300  0 1
P    3  8  0  31 0
P    10  11  0  17 0
P    3  1  500  0 1
P    11  19  0  9 0
P    9  15  0  39 0
P    10  11  0  34 0
P    8  3  0  24 0
P    12  25  0  46 0
P    4  20  0  31 0
P    7  1  500  0 1
P    10  25  0  44 0
S    1  1 0 0 1
S    1  29 0 11 0
S    7  15 0 78 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 0 1
A    1  12 0 42 0
A    13  17 0 8 0
A    2  23 0 0 1
A    11  22 0 0 1
D    11  7 0 22 0
D    12  14 0 12 0
D    4  19 0 13 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m t

actions_done
player action direction
0 m r
1 u n
2 m b
3 m t


round 148

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...R.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  18  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  5  d
2  2  2  4  5  3  1  0  3975  0  a
3  3  11  22  5  6  3  2  2826  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  23  0  24 0
P    3  12  0  8 0
P    4  16  0  10 0
P    3  12  0  24 0
P    4  1  500  0 1
P    4  6  0  1 0
P    5  15  0  46 0
P    6  18  0  14 0
P    11  27  300  0 1
P    3  8  0  30 0
P    10  11  0  16 0
P    3  1  500  0 1
P    11  19  0  8 0
P    9  15  0  38 0
P    10  11  0  33 0
P    8  3  0  23 0
P    12  25  0  45 0
P    4  20  0  30 0
P    7  1  500  0 1
P    10  25  0  43 0
S    1  1 0 0 1
S    1  29 0 10 0
S    7  15 0 77 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 0 1
A    1  12 0 41 0
A    13  17 0 7 0
A    2  23 0 0 1
A    11  22 0 54 0
D    11  7 0 21 0
D    12  14 0 11 0
D    4  19 0 12 0
D    11  7 0 0 1

actions_asked
player action direction
0 m t
1 u n
2 m b
3 m t

actions_done
player action direction
0 m t
1 u n
2 m b
3 m t


round 149

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.....P.X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...R.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  18  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  4  d
2  2  3  4  5  3  1  0  3975  0  a
3  3  10  22  5  6  3  2  2826  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  23  0  23 0
P    3  12  0  7 0
P    4  16  0  9 0
P    3  12  0  23 0
P    4  1  500  0 1
P    3  8  100  0 1
P    5  15  0  45 0
P    6  18  0  13 0
P    11  27  300  0 1
P    3  8  0  29 0
P    10  11  0  15 0
P    3  1  500  0 1
P    11  19  0  7 0
P    9  15  0  37 0
P    10  11  0  32 0
P    8  3  0  22 0
P    12  25  0  44 0
P    4  20  0  29 0
P    7  1  500  0 1
P    10  25  0  42 0
S    1  1 0 0 1
S    1  29 0 9 0
S    7  15 0 76 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 0 1
A    1  12 0 40 0
A    13  17 0 6 0
A    2  23 0 0 1
A    11  22 0 53 0
D    11  7 0 20 0
D    12  14 0 10 0
D    4  19 0 11 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m t

actions_done
player action direction
0 m r
1 u n
2 m r
3 m t


round 150

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.....P.X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...R.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  19  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  3  d
2  2  3  5  5  3  1  0  3975  0  a
3  3  9  22  5  6  3  2  2826  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  23  0  22 0
P    3  12  0  6 0
P    4  16  0  8 0
P    3  12  0  22 0
P    4  1  500  0 1
P    3  8  100  0 1
P    5  15  0  44 0
P    6  18  0  12 0
P    11  27  300  0 1
P    3  8  0  28 0
P    10  11  0  14 0
P    3  1  500  0 1
P    11  19  0  6 0
P    9  15  0  36 0
P    10  11  0  31 0
P    8  3  0  21 0
P    12  25  0  43 0
P    4  20  0  28 0
P    7  1  500  0 1
P    10  25  0  41 0
S    1  1 0 0 1
S    1  29 0 8 0
S    7  15 0 75 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 0 1
A    1  12 0 39 0
A    13  17 0 5 0
A    2  23 0 0 1
A    11  22 0 52 0
D    11  7 0 19 0
D    12  14 0 9 0
D    4  19 0 10 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m t

actions_done
player action direction
0 m r
1 u n
2 m r
3 m t


round 151

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.....P.X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...R.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  20  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  2  d
2  2  3  6  5  3  1  0  3975  0  a
3  3  8  22  5  6  3  2  2826  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  23  0  21 0
P    3  12  0  5 0
P    4  16  0  7 0
P    3  12  0  21 0
P    4  1  500  0 1
P    3  8  100  0 1
P    5  15  0  43 0
P    6  18  0  11 0
P    11  27  300  0 1
P    3  8  0  27 0
P    10  11  0  13 0
P    3  1  500  0 1
P    11  19  0  5 0
P    9  15  0  35 0
P    10  11  0  30 0
P    8  3  0  20 0
P    12  25  0  42 0
P    4  20  0  27 0
P    7  1  500  0 1
P    10  25  0  40 0
S    1  1 0 0 1
S    1  29 0 7 0
S    7  15 0 74 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 0 1
A    1  12 0 38 0
A    13  17 0 4 0
A    2  23 0 0 1
A    11  22 0 51 0
D    11  7 0 18 0
D    12  14 0 8 0
D    4  19 0 9 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 152

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.....P.X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  21  5  6  3  1  4100  0  a
1  1  6  18  3  3  1  0  2175  1  d
2  2  3  7  5  3  1  0  3975  0  a
3  3  8  23  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  23  0  20 0
P    3  12  0  4 0
P    4  16  0  6 0
P    3  12  0  20 0
P    4  1  500  0 1
P    3  8  100  0 1
P    5  15  0  42 0
P    6  18  0  10 0
P    11  27  300  0 1
P    3  8  0  26 0
P    10  11  0  12 0
P    3  1  500  0 1
P    11  19  0  4 0
P    9  15  0  34 0
P    10  11  0  29 0
P    8  3  0  19 0
P    12  25  0  41 0
P    4  20  0  26 0
P    7  1  500  0 1
P    10  25  0  39 0
S    1  1 0 0 1
S    1  29 0 6 0
S    7  15 0 73 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 64 0
A    1  12 0 37 0
A    13  17 0 3 0
A    2  23 0 0 1
A    11  22 0 50 0
D    11  7 0 17 0
D    12  14 0 7 0
D    4  19 0 8 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m l

actions_done
player action direction
0 m r
1 u n
2 m r
3 m l


round 153

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  22  5  6  3  1  4100  0  a
1  1  1  25  3  3  1  0  2175  0  a
2  2  3  8  5  3  1  0  4075  0  a
3  3  8  22  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  23  0  19 0
P    3  12  0  3 0
P    4  16  0  5 0
P    3  12  0  19 0
P    4  1  500  0 1
P    3  8  0  49 0
P    5  15  0  41 0
P    6  18  0  9 0
P    11  27  300  0 1
P    3  8  0  25 0
P    10  11  0  11 0
P    3  1  500  0 1
P    11  19  0  3 0
P    9  15  0  33 0
P    10  11  0  28 0
P    8  3  0  18 0
P    12  25  0  40 0
P    4  20  0  25 0
P    7  1  500  0 1
P    10  25  0  38 0
S    1  1 0 0 1
S    1  29 0 5 0
S    7  15 0 72 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 63 0
A    1  12 0 36 0
A    13  17 0 2 0
A    2  23 0 0 1
A    11  22 0 49 0
D    11  7 0 16 0
D    12  14 0 6 0
D    4  19 0 7 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 154

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  23  5  6  3  1  4100  0  a
1  1  1  24  3  3  1  0  2175  0  a
2  2  4  8  5  3  1  0  4075  0  a
3  3  8  23  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  23  0  18 0
P    3  12  0  2 0
P    4  16  0  4 0
P    3  12  0  18 0
P    4  1  500  0 1
P    3  8  0  48 0
P    5  15  0  40 0
P    6  18  0  8 0
P    11  27  300  0 1
P    3  8  0  24 0
P    10  11  0  10 0
P    3  1  500  0 1
P    11  19  0  2 0
P    9  15  0  32 0
P    10  11  0  27 0
P    8  3  0  17 0
P    12  25  0  39 0
P    4  20  0  24 0
P    7  1  500  0 1
P    10  25  0  37 0
S    1  1 0 0 1
S    1  29 0 4 0
S    7  15 0 71 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 62 0
A    1  12 0 35 0
A    13  17 0 1 0
A    2  23 0 0 1
A    11  22 0 48 0
D    11  7 0 15 0
D    12  14 0 5 0
D    4  19 0 6 0
D    11  7 0 0 1

actions_asked
player action direction
0 m t
1 m l
2 m b
3 m l

actions_done
player action direction
0 m t
1 m l
2 m b
3 m l


round 155

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX..A....X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X.....A.XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  23  5  6  3  1  4100  0  a
1  1  1  23  3  3  1  0  2175  0  a
2  2  5  8  5  3  1  0  4075  0  a
3  3  8  22  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  23  0  17 0
P    3  12  0  1 0
P    4  16  0  3 0
P    3  12  0  17 0
P    4  1  500  0 1
P    3  8  0  47 0
P    5  15  0  39 0
P    6  18  0  7 0
P    11  27  300  0 1
P    3  8  0  23 0
P    10  11  0  9 0
P    3  1  500  0 1
P    11  19  0  1 0
P    9  15  0  31 0
P    10  11  0  26 0
P    8  3  0  16 0
P    12  25  0  38 0
P    4  20  0  23 0
P    7  1  500  0 1
P    10  25  0  36 0
S    1  1 0 0 1
S    1  29 0 3 0
S    7  15 0 70 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 61 0
A    1  12 0 34 0
A    12  8 0 0 1
A    2  23 0 0 1
A    11  22 0 47 0
D    11  7 0 14 0
D    12  14 0 4 0
D    4  19 0 5 0
D    11  7 0 0 1

actions_asked
player action direction
0 m t
1 m b
2 m b
3 m t

actions_done
player action direction
0 m t
1 u n
2 m b
3 m t


round 156

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.......................P.X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  23  6  6  3  1  4100  0  a
1  1  1  23  3  3  1  0  2175  0  a
2  2  6  8  5  3  1  0  4075  0  a
3  3  7  22  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  23  0  16 0
P    12  5  400  0 1
P    4  16  0  2 0
P    3  12  0  16 0
P    4  1  500  0 1
P    3  8  0  46 0
P    5  15  0  38 0
P    6  18  0  6 0
P    11  27  300  0 1
P    3  8  0  22 0
P    10  11  0  8 0
P    3  1  500  0 1
P    4  26  500  0 1
P    9  15  0  30 0
P    10  11  0  25 0
P    8  3  0  15 0
P    12  25  0  37 0
P    4  20  0  22 0
P    7  1  500  0 1
P    10  25  0  35 0
S    1  1 0 0 1
S    1  29 0 2 0
S    7  15 0 69 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 60 0
A    1  12 0 33 0
A    12  8 0 0 1
A    2  23 0 54 0
A    11  22 0 46 0
D    11  7 0 13 0
D    12  14 0 3 0
D    4  19 0 4 0
D    11  7 0 0 1

actions_asked
player action direction
0 a t
1 m r
2 m b
3 m t

actions_done
player action direction
0 u n
1 m r
2 m b
3 m t


round 157

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.......................P.X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX.........................XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  23  6  6  3  1  4100  0  a
1  1  1  24  3  3  1  0  2175  0  a
2  2  7  8  5  3  1  0  4075  0  a
3  3  6  22  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  23  0  15 0
P    12  5  400  0 1
P    4  16  0  1 0
P    3  12  0  15 0
P    4  1  500  0 1
P    3  8  0  45 0
P    5  15  0  37 0
P    6  18  0  5 0
P    11  27  300  0 1
P    3  8  0  21 0
P    10  11  0  7 0
P    3  1  500  0 1
P    4  26  500  0 1
P    9  15  0  29 0
P    10  11  0  24 0
P    8  3  0  14 0
P    12  25  0  36 0
P    4  20  0  21 0
P    7  1  500  0 1
P    10  25  0  34 0
S    1  1 0 0 1
S    1  29 0 1 0
S    7  15 0 68 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 59 0
A    1  12 0 32 0
A    12  8 0 0 1
A    2  23 0 53 0
A    11  22 0 45 0
D    11  7 0 12 0
D    12  14 0 2 0
D    4  19 0 3 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m b
3 m t

actions_done
player action direction
0 m r
1 m r
2 m b
3 m t


round 158

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.......................P.X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX............S............XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  24  6  6  3  1  4100  0  a
1  1  1  25  3  3  1  0  2175  0  a
2  2  8  8  5  3  1  0  4075  0  a
3  3  5  22  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  23  0  14 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  14 0
P    4  1  500  0 1
P    3  8  0  44 0
P    5  15  0  36 0
P    6  18  0  4 0
P    11  27  300  0 1
P    3  8  0  20 0
P    10  11  0  6 0
P    3  1  500  0 1
P    4  26  500  0 1
P    9  15  0  28 0
P    10  11  0  23 0
P    8  3  0  13 0
P    12  25  0  35 0
P    4  20  0  20 0
P    7  1  500  0 1
P    10  25  0  33 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 67 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 58 0
A    1  12 0 31 0
A    12  8 0 0 1
A    2  23 0 52 0
A    11  22 0 44 0
D    11  7 0 11 0
D    12  14 0 1 0
D    4  19 0 2 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m b
3 m t

actions_done
player action direction
0 m r
1 m r
2 m b
3 m t


round 159

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.......................P.X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X.....X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  25  6  6  3  1  4100  0  a
1  1  1  26  3  3  1  0  2175  0  a
2  2  9  8  5  3  1  0  4075  0  a
3  3  4  22  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  23  0  13 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  13 0
P    4  1  500  0 1
P    3  8  0  43 0
P    5  15  0  35 0
P    6  18  0  3 0
P    11  27  300  0 1
P    3  8  0  19 0
P    10  11  0  5 0
P    3  1  500  0 1
P    4  26  500  0 1
P    9  15  0  27 0
P    10  11  0  22 0
P    8  3  0  12 0
P    12  25  0  34 0
P    4  20  0  19 0
P    7  1  500  0 1
P    10  25  0  32 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 66 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 57 0
A    1  12 0 30 0
A    12  8 0 0 1
A    2  23 0 51 0
A    11  22 0 43 0
D    11  7 0 10 0
D    13  26 0 0 1
D    4  19 0 1 0
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 m b
2 m b
3 m r

actions_done
player action direction
0 u n
1 m b
2 m b
3 m r


round 160

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.......................P.X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  25  6  6  3  1  4100  0  a
1  1  2  26  3  3  1  0  2175  0  a
2  2  10  8  5  3  1  0  4075  0  a
3  3  4  23  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  23  0  12 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  12 0
P    4  1  500  0 1
P    3  8  0  42 0
P    5  15  0  34 0
P    6  18  0  2 0
P    11  27  300  0 1
P    3  8  0  18 0
P    10  11  0  4 0
P    3  1  500  0 1
P    4  26  500  0 1
P    9  15  0  26 0
P    10  11  0  21 0
P    8  3  0  11 0
P    12  25  0  33 0
P    4  20  0  18 0
P    7  1  500  0 1
P    10  25  0  31 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 65 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 56 0
A    1  12 0 29 0
A    12  8 0 0 1
A    2  23 0 50 0
A    11  22 0 42 0
D    11  7 0 9 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 0 1

actions_asked
player action direction
0 a r
1 m r
2 m l
3 m r

actions_done
player action direction
0 a r
1 m r
2 m l
3 m r


round 161

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX.......................P.X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X....D..X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  25  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  39  d
2  2  10  7  5  3  1  0  4075  0  a
3  3  4  24  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  23  0  11 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  11 0
P    4  1  500  0 1
P    3  8  0  41 0
P    5  15  0  33 0
P    6  18  0  1 0
P    11  27  300  0 1
P    3  8  0  17 0
P    10  11  0  3 0
P    3  1  500  0 1
P    4  26  500  0 1
P    9  15  0  25 0
P    10  11  0  20 0
P    8  3  0  10 0
P    12  25  0  32 0
P    4  20  0  17 0
P    7  1  500  0 1
P    10  25  0  30 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 64 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 55 0
A    1  12 0 28 0
A    12  8 0 0 1
A    2  23 0 49 0
A    11  22 0 41 0
D    11  7 0 8 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 0 1

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m r

actions_done
player action direction
0 m r
1 u n
2 m b
3 m r


round 162

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.......................P.X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  38  d
2  2  11  7  5  4  1  0  4075  0  a
3  3  4  25  5  6  4  2  2826  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  23  0  10 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  10 0
P    4  1  500  0 1
P    3  8  0  40 0
P    5  15  0  32 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  16 0
P    10  11  0  2 0
P    3  1  500  0 1
P    4  26  500  0 1
P    9  15  0  24 0
P    10  11  0  19 0
P    8  3  0  9 0
P    12  25  0  31 0
P    4  20  0  16 0
P    7  1  500  0 1
P    10  25  0  29 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 63 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 54 0
A    1  12 0 27 0
A    12  8 0 0 1
A    2  23 0 48 0
A    11  22 0 40 0
D    11  7 0 7 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 54 0

actions_asked
player action direction
0 m b
1 u n
2 m r
3 m r

actions_done
player action direction
0 m b
1 u n
2 m r
3 m r


round 163

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X..P..A.XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  37  d
2  2  11  8  5  4  1  0  4075  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  23  0  9 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  9 0
P    4  1  500  0 1
P    3  8  0  39 0
P    5  15  0  31 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  15 0
P    10  11  0  1 0
P    3  1  500  0 1
P    4  26  0  49 0
P    9  15  0  23 0
P    10  11  0  18 0
P    8  3  0  8 0
P    12  25  0  30 0
P    4  20  0  15 0
P    7  1  500  0 1
P    10  25  0  28 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 62 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 53 0
A    1  12 0 26 0
A    12  8 0 0 1
A    2  23 0 47 0
A    11  22 0 39 0
D    11  7 0 6 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 53 0

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m r

actions_done
player action direction
0 m r
1 u n
2 m b
3 m r


round 164

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X..P....XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  36  d
2  2  12  8  6  4  1  0  4075  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  23  0  8 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  8 0
P    4  1  500  0 1
P    3  8  0  38 0
P    5  15  0  30 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  14 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  48 0
P    9  15  0  22 0
P    10  11  0  17 0
P    8  3  0  7 0
P    12  25  0  29 0
P    4  20  0  14 0
P    7  1  500  0 1
P    10  25  0  27 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 61 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 52 0
A    1  12 0 25 0
A    12  8 0 54 0
A    2  23 0 46 0
A    11  22 0 38 0
D    11  7 0 5 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 52 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 165

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X..P....XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  35  d
2  2  12  7  6  4  1  0  4075  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  23  0  7 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  7 0
P    4  1  500  0 1
P    3  8  0  37 0
P    5  15  0  29 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  13 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  47 0
P    9  15  0  21 0
P    10  11  0  16 0
P    8  3  0  6 0
P    12  25  0  28 0
P    4  20  0  13 0
P    7  1  500  0 1
P    10  25  0  26 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 60 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 51 0
A    1  12 0 24 0
A    12  8 0 53 0
A    2  23 0 45 0
A    11  22 0 37 0
D    11  7 0 4 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 51 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m r

actions_done
player action direction
0 m r
1 u n
2 m l
3 m r


round 166

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X..P....XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  34  d
2  2  12  6  6  4  1  0  4075  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  23  0  6 0
P    12  5  400  0 1
P    6  3  400  0 1
P    3  12  0  6 0
P    4  1  500  0 1
P    3  8  0  36 0
P    5  15  0  28 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  12 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  46 0
P    9  15  0  20 0
P    10  11  0  15 0
P    8  3  0  5 0
P    12  25  0  27 0
P    4  20  0  12 0
P    7  1  500  0 1
P    10  25  0  25 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 59 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 50 0
A    1  12 0 23 0
A    12  8 0 52 0
A    2  23 0 44 0
A    11  22 0 36 0
D    11  7 0 3 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 50 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 167

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  33  d
2  2  12  5  6  4  1  0  4475  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  23  0  5 0
P    12  5  0  49 0
P    6  3  400  0 1
P    3  12  0  5 0
P    4  1  500  0 1
P    3  8  0  35 0
P    5  15  0  27 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  11 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  45 0
P    9  15  0  19 0
P    10  11  0  14 0
P    8  3  0  4 0
P    12  25  0  26 0
P    4  20  0  11 0
P    7  1  500  0 1
P    10  25  0  24 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 58 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 49 0
A    1  12 0 22 0
A    12  8 0 51 0
A    2  23 0 43 0
A    11  22 0 35 0
D    11  7 0 2 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 49 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 168

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.........................X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  32  d
2  2  12  6  6  4  1  0  4475  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  23  0  4 0
P    12  5  0  48 0
P    6  3  400  0 1
P    3  12  0  4 0
P    4  1  500  0 1
P    3  8  0  34 0
P    5  15  0  26 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  10 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  44 0
P    9  15  0  18 0
P    10  11  0  13 0
P    8  3  0  3 0
P    12  25  0  25 0
P    4  20  0  10 0
P    7  1  500  0 1
P    10  25  0  23 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 57 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 48 0
A    1  12 0 21 0
A    12  8 0 50 0
A    2  23 0 42 0
A    11  22 0 34 0
D    11  7 0 1 0
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 48 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 m l


round 169

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.....................D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  31  d
2  2  12  7  6  4  1  0  4475  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  23  0  3 0
P    12  5  0  47 0
P    6  3  400  0 1
P    3  12  0  3 0
P    4  1  500  0 1
P    3  8  0  33 0
P    5  15  0  25 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  9 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  43 0
P    9  15  0  17 0
P    10  11  0  12 0
P    8  3  0  2 0
P    12  25  0  24 0
P    4  20  0  9 0
P    7  1  500  0 1
P    10  25  0  22 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 56 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 47 0
A    1  12 0 20 0
A    12  8 0 49 0
A    2  23 0 41 0
A    11  22 0 33 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 47 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 170

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.....................D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X......PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  30  d
2  2  12  8  6  4  1  0  4475  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  23  0  2 0
P    12  5  0  46 0
P    6  3  400  0 1
P    3  12  0  2 0
P    4  1  500  0 1
P    3  8  0  32 0
P    5  15  0  24 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  8 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  42 0
P    9  15  0  16 0
P    10  11  0  11 0
P    8  3  0  1 0
P    12  25  0  23 0
P    4  20  0  8 0
P    7  1  500  0 1
P    10  25  0  21 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 55 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 46 0
A    1  12 0 19 0
A    12  8 0 48 0
A    2  23 0 40 0
A    11  22 0 32 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 46 0

actions_asked
player action direction
0 m l
1 u n
2 m t
3 m l

actions_done
player action direction
0 m l
1 u n
2 m t
3 m l


round 171

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.....................D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  29  d
2  2  11  8  6  4  1  0  4475  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  23  0  1 0
P    12  5  0  45 0
P    6  3  400  0 1
P    3  12  0  1 0
P    4  1  500  0 1
P    3  8  0  31 0
P    5  15  0  23 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  7 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  41 0
P    9  15  0  15 0
P    10  11  0  10 0
P    11  22  300  0 1
P    12  25  0  22 0
P    4  20  0  7 0
P    7  1  500  0 1
P    10  25  0  20 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 54 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 45 0
A    1  12 0 18 0
A    12  8 0 47 0
A    2  23 0 39 0
A    11  22 0 31 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 45 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m r

actions_done
player action direction
0 m r
1 u n
2 m t
3 m r


round 172

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  28  d
2  2  10  8  6  4  1  0  4475  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  44 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  30 0
P    5  15  0  22 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  6 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  40 0
P    9  15  0  14 0
P    10  11  0  9 0
P    11  22  300  0 1
P    12  25  0  21 0
P    4  20  0  6 0
P    7  1  500  0 1
P    10  25  0  19 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 53 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 44 0
A    1  12 0 17 0
A    12  8 0 46 0
A    2  23 0 38 0
A    11  22 0 30 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 44 0

actions_asked
player action direction
0 m l
1 u n
2 m t
3 m l

actions_done
player action direction
0 m l
1 u n
2 m t
3 m l


round 173

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  27  d
2  2  9  8  6  4  1  0  4475  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  43 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  29 0
P    5  15  0  21 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  5 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  39 0
P    9  15  0  13 0
P    10  11  0  8 0
P    11  22  300  0 1
P    12  25  0  20 0
P    4  20  0  5 0
P    7  1  500  0 1
P    10  25  0  18 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 52 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 43 0
A    1  12 0 16 0
A    12  8 0 45 0
A    2  23 0 37 0
A    11  22 0 29 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 43 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m r

actions_done
player action direction
0 m r
1 u n
2 m t
3 m r


round 174

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X...P.X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  26  d
2  2  8  8  6  4  1  0  4475  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  42 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  28 0
P    5  15  0  20 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  4 0
P    8  9  100  0 1
P    3  1  500  0 1
P    4  26  0  38 0
P    9  15  0  12 0
P    10  11  0  7 0
P    11  22  300  0 1
P    12  25  0  19 0
P    4  20  0  4 0
P    7  1  500  0 1
P    10  25  0  17 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 51 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 42 0
A    1  12 0 15 0
A    12  8 0 44 0
A    2  23 0 36 0
A    11  22 0 28 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 42 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 m l


round 175

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  25  d
2  2  8  9  6  4  1  0  4575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  41 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  27 0
P    5  15  0  19 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  3 0
P    8  9  0  49 0
P    3  1  500  0 1
P    4  26  0  37 0
P    9  15  0  11 0
P    10  11  0  6 0
P    11  22  300  0 1
P    12  25  0  18 0
P    4  20  0  3 0
P    7  1  500  0 1
P    10  25  0  16 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 50 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 41 0
A    1  12 0 14 0
A    12  8 0 43 0
A    2  23 0 35 0
A    11  22 0 27 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 41 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m r

actions_done
player action direction
0 m r
1 u n
2 m l
3 m r


round 176

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  24  d
2  2  8  8  6  4  1  0  4575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  40 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  26 0
P    5  15  0  18 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  2 0
P    8  9  0  48 0
P    3  1  500  0 1
P    4  26  0  36 0
P    9  15  0  10 0
P    10  11  0  5 0
P    11  22  300  0 1
P    12  25  0  17 0
P    4  20  0  2 0
P    7  1  500  0 1
P    10  25  0  15 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 49 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 40 0
A    1  12 0 13 0
A    12  8 0 42 0
A    2  23 0 34 0
A    11  22 0 26 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 40 0

actions_asked
player action direction
0 m l
1 u n
2 m b
3 m l

actions_done
player action direction
0 m l
1 u n
2 m b
3 m l


round 177

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  23  d
2  2  9  8  6  4  1  0  4575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  39 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  25 0
P    5  15  0  17 0
P    3  21  200  0 1
P    11  27  300  0 1
P    3  8  0  1 0
P    8  9  0  47 0
P    3  1  500  0 1
P    4  26  0  35 0
P    9  15  0  9 0
P    10  11  0  4 0
P    11  22  300  0 1
P    12  25  0  16 0
P    4  20  0  1 0
P    7  1  500  0 1
P    10  25  0  14 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 48 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 39 0
A    1  12 0 12 0
A    12  8 0 41 0
A    2  23 0 33 0
A    11  22 0 25 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 39 0

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m r

actions_done
player action direction
0 m r
1 u n
2 m b
3 m r


round 178

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  22  d
2  2  10  8  6  4  1  0  4575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  38 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  24 0
P    5  15  0  16 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  46 0
P    3  1  500  0 1
P    4  26  0  34 0
P    9  15  0  8 0
P    10  11  0  3 0
P    11  22  300  0 1
P    12  25  0  15 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  13 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 47 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 38 0
A    1  12 0 11 0
A    12  8 0 40 0
A    2  23 0 32 0
A    11  22 0 24 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 38 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 m l


round 179

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  21  d
2  2  10  9  6  4  1  0  4575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  37 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  23 0
P    5  15  0  15 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  45 0
P    3  1  500  0 1
P    4  26  0  33 0
P    9  15  0  7 0
P    10  11  0  2 0
P    11  22  300  0 1
P    12  25  0  14 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  12 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 46 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 37 0
A    1  12 0 10 0
A    12  8 0 39 0
A    2  23 0 31 0
A    11  22 0 23 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 37 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 180

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S..........D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  20  d
2  2  10  10  6  4  1  0  4575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  36 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  22 0
P    5  15  0  14 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  44 0
P    3  1  500  0 1
P    4  26  0  32 0
P    9  15  0  6 0
P    10  11  0  1 0
P    11  22  300  0 1
P    12  25  0  13 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  11 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 45 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 36 0
A    1  12 0 9 0
A    12  8 0 38 0
A    2  23 0 30 0
A    11  22 0 22 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 36 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 m l


round 181

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........P...............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  19  d
2  2  10  11  6  4  1  0  4575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    10  12  200  0 1
P    12  5  0  35 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  21 0
P    5  15  0  13 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  43 0
P    3  1  500  0 1
P    4  26  0  31 0
P    9  15  0  5 0
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  12 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  10 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 44 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 35 0
A    1  12 0 8 0
A    12  8 0 37 0
A    2  23 0 29 0
A    11  22 0 21 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 35 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 182

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  18  d
2  2  10  12  6  4  1  0  4775  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    10  12  0  49 0
P    12  5  0  34 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  20 0
P    5  15  0  12 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  42 0
P    3  1  500  0 1
P    4  26  0  30 0
P    9  15  0  4 0
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  11 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  9 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 43 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 34 0
A    1  12 0 7 0
A    12  8 0 36 0
A    2  23 0 28 0
A    11  22 0 20 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 34 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 m l


round 183

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  17  d
2  2  10  13  6  4  1  0  4775  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    10  12  0  48 0
P    12  5  0  33 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  19 0
P    5  15  0  11 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  41 0
P    3  1  500  0 1
P    4  26  0  29 0
P    9  15  0  3 0
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  10 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  8 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 42 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 33 0
A    1  12 0 6 0
A    12  8 0 35 0
A    2  23 0 27 0
A    11  22 0 19 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 33 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 184

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  16  d
2  2  10  14  6  4  1  0  4775  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    10  12  0  47 0
P    12  5  0  32 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  18 0
P    5  15  0  10 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  40 0
P    3  1  500  0 1
P    4  26  0  28 0
P    9  15  0  2 0
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  9 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  7 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 41 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 32 0
A    1  12 0 5 0
A    12  8 0 34 0
A    2  23 0 26 0
A    11  22 0 18 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 32 0

actions_asked
player action direction
0 m l
1 u n
2 m t
3 m l

actions_done
player action direction
0 m l
1 u n
2 m t
3 m l


round 185

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.......X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  15  d
2  2  9  14  6  4  1  0  4775  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    10  12  0  46 0
P    12  5  0  31 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  17 0
P    5  15  0  9 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  39 0
P    3  1  500  0 1
P    4  26  0  27 0
P    9  15  0  1 0
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  8 0
P    8  14  100  0 1
P    7  1  500  0 1
P    10  25  0  6 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 40 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 31 0
A    1  12 0 4 0
A    12  8 0 33 0
A    2  23 0 25 0
A    11  22 0 17 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 31 0

actions_asked
player action direction
0 m r
1 u n
2 m t
3 m r

actions_done
player action direction
0 m r
1 u n
2 m t
3 m r


round 186

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  14  d
2  2  8  14  6  4  1  0  4875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    10  12  0  45 0
P    12  5  0  30 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  16 0
P    5  15  0  8 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  38 0
P    3  1  500  0 1
P    4  26  0  26 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  7 0
P    8  14  0  49 0
P    7  1  500  0 1
P    10  25  0  5 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 39 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 30 0
A    1  12 0 3 0
A    12  8 0 32 0
A    2  23 0 24 0
A    11  22 0 16 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 30 0

actions_asked
player action direction
0 m l
1 u n
2 m b
3 m l

actions_done
player action direction
0 m l
1 u n
2 m b
3 m l


round 187

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  13  d
2  2  9  14  6  4  1  0  4875  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    10  12  0  44 0
P    12  5  0  29 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  15 0
P    5  15  0  7 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  37 0
P    3  1  500  0 1
P    4  26  0  25 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  6 0
P    8  14  0  48 0
P    7  1  500  0 1
P    10  25  0  4 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 38 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 29 0
A    1  12 0 2 0
A    12  8 0 31 0
A    2  23 0 23 0
A    11  22 0 15 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 29 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m r

actions_done
player action direction
0 m r
1 u n
2 m l
3 m r


round 188

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XP......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  12  d
2  2  9  13  6  4  1  0  4875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    10  12  0  43 0
P    12  5  0  28 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  14 0
P    5  15  0  6 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  36 0
P    3  1  500  0 1
P    4  26  0  24 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  5 0
P    8  14  0  47 0
P    7  1  500  0 1
P    10  25  0  3 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 37 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 28 0
A    1  12 0 1 0
A    12  8 0 30 0
A    2  23 0 22 0
A    11  22 0 14 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 28 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 189

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  11  d
2  2  9  12  6  4  1  0  4875  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    10  12  0  42 0
P    12  5  0  27 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  13 0
P    5  15  0  5 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  35 0
P    3  1  500  0 1
P    4  26  0  23 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  4 0
P    8  14  0  46 0
P    7  1  500  0 1
P    10  25  0  2 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 36 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 27 0
A    3  22 0 0 1
A    12  8 0 29 0
A    2  23 0 21 0
A    11  22 0 13 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 27 0

actions_asked
player action direction
0 m r
1 u n
2 m b
3 m r

actions_done
player action direction
0 m r
1 u n
2 m b
3 m r


round 190

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX.........................X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  10  d
2  2  10  12  6  4  1  0  4875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    10  12  0  41 0
P    12  5  0  26 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  12 0
P    5  15  0  4 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  34 0
P    3  1  500  0 1
P    4  26  0  22 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  3 0
P    8  14  0  45 0
P    7  1  500  0 1
P    10  25  0  1 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 35 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 26 0
A    3  22 0 0 1
A    12  8 0 28 0
A    2  23 0 20 0
A    11  22 0 12 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 26 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 191

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX.........................X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  9  d
2  2  10  11  6  4  1  0  4875  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    10  12  0  40 0
P    12  5  0  25 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  11 0
P    5  15  0  3 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  33 0
P    3  1  500  0 1
P    4  26  0  21 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  2 0
P    8  14  0  44 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 34 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 25 0
A    3  22 0 0 1
A    12  8 0 27 0
A    2  23 0 19 0
A    11  22 0 11 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 25 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m r

actions_done
player action direction
0 m r
1 u n
2 m l
3 m r


round 192

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX.........................X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  8  d
2  2  10  10  6  4  1  0  4875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    10  12  0  39 0
P    12  5  0  24 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  10 0
P    5  15  0  2 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  32 0
P    3  1  500  0 1
P    4  26  0  20 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    12  25  0  1 0
P    8  14  0  43 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 33 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 24 0
A    3  22 0 0 1
A    12  8 0 26 0
A    2  23 0 18 0
A    11  22 0 10 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 24 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 193

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX.........................X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X..........P..............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  7  d
2  2  10  9  6  4  1  0  4875  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    10  12  0  38 0
P    12  5  0  23 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  9 0
P    5  15  0  1 0
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  31 0
P    3  1  500  0 1
P    4  26  0  19 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  500  0 1
P    8  14  0  42 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 32 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 23 0
A    3  22 0 0 1
A    12  8 0 25 0
A    2  23 0 17 0
A    11  22 0 9 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 23 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 194

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X..........P..............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  6  d
2  2  10  10  6  4  1  0  4875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    10  12  0  37 0
P    12  5  0  22 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  8 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  30 0
P    3  1  500  0 1
P    4  26  0  18 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  500  0 1
P    8  14  0  41 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 31 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 22 0
A    3  22 0 0 1
A    12  8 0 24 0
A    2  23 0 16 0
A    11  22 0 8 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 22 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 m l


round 195

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X..........P..............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  5  d
2  2  10  11  6  4  1  0  4875  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    10  12  0  36 0
P    12  5  0  21 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  7 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  29 0
P    3  1  500  0 1
P    4  26  0  17 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  500  0 1
P    8  14  0  40 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 30 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 21 0
A    3  22 0 0 1
A    12  8 0 23 0
A    2  23 0 15 0
A    11  22 0 7 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 21 0

actions_asked
player action direction
0 m r
1 u n
2 m r
3 m r

actions_done
player action direction
0 m r
1 u n
2 m r
3 m r


round 196

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X..........P..............X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  4  d
2  2  10  12  6  4  1  0  4875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    10  12  0  35 0
P    12  5  0  20 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  6 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  28 0
P    3  1  500  0 1
P    4  26  0  16 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  500  0 1
P    8  14  0  39 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 29 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 20 0
A    3  22 0 0 1
A    12  8 0 22 0
A    2  23 0 14 0
A    11  22 0 6 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 20 0

actions_asked
player action direction
0 m l
1 u n
2 m r
3 m l

actions_done
player action direction
0 m l
1 u n
2 m r
3 m l


round 197

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  3  d
2  2  10  13  6  4  1  0  5375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    10  12  0  34 0
P    12  5  0  19 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  5 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  27 0
P    3  1  500  0 1
P    4  26  0  15 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  49 0
P    8  14  0  38 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 28 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 19 0
A    3  22 0 0 1
A    12  8 0 21 0
A    2  23 0 13 0
A    11  22 0 5 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 19 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m r

actions_done
player action direction
0 m r
1 u n
2 m l
3 m r


round 198

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  2  d
2  2  10  12  6  4  1  0  5375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    10  12  0  33 0
P    12  5  0  18 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  4 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  26 0
P    3  1  500  0 1
P    4  26  0  14 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  48 0
P    8  14  0  37 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 27 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 18 0
A    3  22 0 0 1
A    12  8 0 20 0
A    2  23 0 12 0
A    11  22 0 4 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 18 0

actions_asked
player action direction
0 m l
1 u n
2 m l
3 m l

actions_done
player action direction
0 m l
1 u n
2 m l
3 m l


round 199

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  2  27  3  3  1  0  2175  1  d
2  2  10  11  6  4  1  0  5375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    10  12  0  32 0
P    12  5  0  17 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  3 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  25 0
P    3  1  500  0 1
P    4  26  0  13 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  47 0
P    8  14  0  36 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 26 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 17 0
A    3  22 0 0 1
A    12  8 0 19 0
A    2  23 0 11 0
A    11  22 0 3 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 17 0

actions_asked
player action direction
0 m r
1 u n
2 m l
3 m r

actions_done
player action direction
0 m r
1 u n
2 m l
3 m r


round 200

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  10  13  3  3  1  0  2175  0  a
2  2  10  10  6  4  1  0  5375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    10  12  0  31 0
P    12  5  0  16 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  2 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  24 0
P    3  1  500  0 1
P    4  26  0  12 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  46 0
P    8  14  0  35 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 25 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 16 0
A    3  22 0 0 1
A    12  8 0 18 0
A    2  23 0 10 0
A    11  22 0 2 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 16 0

actions_asked
player action direction
0 m l
1 m r
2 a r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 201

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.......XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  10  14  3  3  1  0  2175  0  a
2  2  10  10  6  4  1  0  5375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    10  12  0  30 0
P    12  5  0  15 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    3  8  0  1 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  23 0
P    3  1  500  0 1
P    4  26  0  11 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  45 0
P    8  14  0  34 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 24 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 15 0
A    3  22 0 0 1
A    12  8 0 17 0
A    2  23 0 9 0
A    11  22 0 1 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 15 0

actions_asked
player action direction
0 m r
1 m r
2 a r
3 m r

actions_done
player action direction
0 m r
1 m r
2 u n
3 m r


round 202

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  10  15  3  3  1  0  2175  0  a
2  2  10  10  6  4  1  0  5375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    10  12  0  29 0
P    12  5  0  14 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  22 0
P    3  1  500  0 1
P    4  26  0  10 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  44 0
P    8  14  0  33 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 23 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 14 0
A    3  22 0 0 1
A    12  8 0 16 0
A    2  23 0 8 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 14 0

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 203

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  10  16  3  3  1  0  2175  0  a
2  2  10  9  6  4  1  0  5375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    10  12  0  28 0
P    12  5  0  13 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  21 0
P    3  1  500  0 1
P    4  26  0  9 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  43 0
P    8  14  0  32 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 22 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 13 0
A    3  22 0 0 1
A    12  8 0 15 0
A    2  23 0 7 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 13 0

actions_asked
player action direction
0 m r
1 m r
2 m l
3 m r

actions_done
player action direction
0 m r
1 m r
2 m l
3 m r


round 204

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  10  17  3  3  1  0  2175  0  a
2  2  10  8  6  4  1  0  5375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    10  12  0  27 0
P    12  5  0  12 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  20 0
P    3  1  500  0 1
P    4  26  0  8 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  42 0
P    8  14  0  31 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 21 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 12 0
A    3  22 0 0 1
A    12  8 0 14 0
A    2  23 0 6 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 12 0

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 205

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  10  18  3  3  1  0  2175  0  a
2  2  10  7  6  4  1  0  5375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    10  12  0  26 0
P    12  5  0  11 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  19 0
P    3  1  500  0 1
P    4  26  0  7 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  41 0
P    8  14  0  30 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 20 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 11 0
A    3  22 0 0 1
A    12  8 0 13 0
A    2  23 0 5 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 11 0

actions_asked
player action direction
0 m r
1 m t
2 m l
3 m r

actions_done
player action direction
0 m r
1 m t
2 m l
3 m r


round 206

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  9  18  3  3  1  0  2175  0  a
2  2  10  6  6  4  1  0  5375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    10  12  0  25 0
P    12  5  0  10 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  18 0
P    3  1  500  0 1
P    4  26  0  6 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  40 0
P    8  14  0  29 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 19 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 10 0
A    3  22 0 0 1
A    12  8 0 12 0
A    2  23 0 4 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 10 0

actions_asked
player action direction
0 m l
1 m t
2 m l
3 m l

actions_done
player action direction
0 m l
1 m t
2 m l
3 m l


round 207

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  8  18  3  3  1  0  2175  0  a
2  2  10  5  6  4  1  0  5375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    10  12  0  24 0
P    12  5  0  9 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  17 0
P    3  1  500  0 1
P    4  26  0  5 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  39 0
P    8  14  0  28 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 18 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 9 0
A    3  22 0 0 1
A    12  8 0 11 0
A    2  23 0 3 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 9 0

actions_asked
player action direction
0 m r
1 m t
2 m l
3 m r

actions_done
player action direction
0 m r
1 m t
2 m l
3 m r


round 208

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.XPX.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  7  18  3  3  1  0  2175  0  a
2  2  10  4  6  4  1  0  5375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    10  12  0  23 0
P    12  5  0  8 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  300  0 1
P    8  9  0  16 0
P    3  1  500  0 1
P    4  26  0  4 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  38 0
P    8  14  0  27 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 17 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 8 0
A    3  22 0 0 1
A    12  8 0 10 0
A    2  23 0 2 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 8 0

actions_asked
player action direction
0 m l
1 m t
2 m b
3 m l

actions_done
player action direction
0 m l
1 m t
2 m b
3 m l


round 209

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXX.R.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX......PXXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  6  18  3  3  1  0  2475  0  a
2  2  11  4  6  4  1  0  5375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    10  12  0  22 0
P    12  5  0  7 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  0  49 0
P    8  9  0  15 0
P    3  1  500  0 1
P    4  26  0  3 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  37 0
P    8  14  0  26 0
P    7  1  500  0 1
P    5  18  500  0 1
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 16 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 7 0
A    3  22 0 0 1
A    12  8 0 9 0
A    2  23 0 1 0
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 7 0

actions_asked
player action direction
0 m r
1 m t
2 m b
3 m r

actions_done
player action direction
0 m r
1 m t
2 m b
3 m r


round 210

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX.P..........S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  5  18  3  3  1  0  2975  0  a
2  2  12  4  6  4  1  0  5375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    10  12  0  21 0
P    12  5  0  6 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  300  0 1
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  0  48 0
P    8  9  0  14 0
P    3  1  500  0 1
P    4  26  0  2 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  36 0
P    8  14  0  25 0
P    7  1  500  0 1
P    5  18  0  49 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 15 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 6 0
A    3  22 0 0 1
A    12  8 0 8 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 6 0

actions_asked
player action direction
0 m l
1 m t
2 m b
3 m l

actions_done
player action direction
0 m l
1 m t
2 m b
3 m l


round 211

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  4  18  3  3  1  0  2975  0  a
2  2  13  4  6  4  1  0  5675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    10  12  0  20 0
P    12  5  0  5 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  49 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  0  47 0
P    8  9  0  13 0
P    3  1  500  0 1
P    4  26  0  1 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  35 0
P    8  14  0  24 0
P    7  1  500  0 1
P    5  18  0  48 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 14 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 5 0
A    3  22 0 0 1
A    12  8 0 7 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 5 0

actions_asked
player action direction
0 m r
1 m r
2 m l
3 m r

actions_done
player action direction
0 m r
1 m r
2 m l
3 m r


round 212

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  4  19  3  3  1  0  2975  0  a
2  2  13  3  6  4  1  0  5675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    10  12  0  19 0
P    12  5  0  4 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  48 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  0  46 0
P    8  9  0  12 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  34 0
P    8  14  0  23 0
P    7  1  500  0 1
P    5  18  0  47 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 13 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 4 0
A    3  22 0 0 1
A    12  8 0 6 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 4 0

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 213

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  4  20  3  3  1  0  2975  0  a
2  2  12  3  6  4  1  0  5675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    10  12  0  18 0
P    12  5  0  3 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  47 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  0  45 0
P    8  9  0  11 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  33 0
P    8  14  0  22 0
P    7  1  500  0 1
P    5  18  0  46 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 12 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 3 0
A    3  22 0 0 1
A    12  8 0 5 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 3 0

actions_asked
player action direction
0 m r
1 m r
2 m t
3 m r

actions_done
player action direction
0 m r
1 m r
2 m t
3 m r


round 214

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..XPA.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  4  21  3  3  1  0  2975  0  a
2  2  11  3  6  4  1  0  5675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    10  12  0  17 0
P    12  5  0  2 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  46 0
P    4  15  200  0 1
P    3  21  200  0 1
P    11  27  300  0 1
P    6  18  0  44 0
P    8  9  0  10 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  32 0
P    8  14  0  21 0
P    7  1  500  0 1
P    5  18  0  45 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 11 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 2 0
A    3  22 0 0 1
A    12  8 0 4 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 2 0

actions_asked
player action direction
0 m l
1 m t
2 m t
3 m l

actions_done
player action direction
0 m l
1 m t
2 m t
3 m l


round 215

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.A.....X.X
04 XPX............P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.........................X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  21  3  3  1  0  3175  0  a
2  2  10  3  6  4  1  0  5675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    10  12  0  16 0
P    12  5  0  1 0
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  45 0
P    4  15  200  0 1
P    3  21  0  49 0
P    11  27  300  0 1
P    6  18  0  43 0
P    8  9  0  9 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  31 0
P    8  14  0  20 0
P    7  1  500  0 1
P    5  18  0  44 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 10 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    8  23 0 1 0
A    3  22 0 0 1
A    12  8 0 3 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    11  7 0 1 0

actions_asked
player action direction
0 m r
1 m r
2 m t
3 m r

actions_done
player action direction
0 m r
1 m r
2 m t
3 m r


round 216

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  3  6  4  1  0  5675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    10  12  0  15 0
P    4  6  400  0 1
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  44 0
P    4  15  200  0 1
P    3  21  0  48 0
P    11  27  300  0 1
P    6  18  0  42 0
P    8  9  0  8 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  30 0
P    8  14  0  19 0
P    7  1  500  0 1
P    5  18  0  43 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 9 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 54 0
A    12  8 0 2 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 217

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..X.X
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  8  3  6  4  1  0  5675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    10  12  0  14 0
P    4  6  400  0 1
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  43 0
P    4  15  200  0 1
P    3  21  0  47 0
P    11  27  300  0 1
P    6  18  0  41 0
P    8  9  0  7 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  29 0
P    8  14  0  18 0
P    7  1  500  0 1
P    5  18  0  42 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 8 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 53 0
A    12  8 0 1 0
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 218

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.XP.X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  3  6  4  1  0  5675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    10  12  0  13 0
P    4  6  400  0 1
P    6  3  400  0 1
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  42 0
P    4  15  200  0 1
P    3  21  0  46 0
P    11  27  300  0 1
P    6  18  0  40 0
P    8  9  0  6 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  28 0
P    8  14  0  17 0
P    7  1  500  0 1
P    5  18  0  41 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 7 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 52 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 219

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  3  6  4  1  0  6075  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    10  12  0  12 0
P    4  6  400  0 1
P    6  3  0  49 0
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  41 0
P    4  15  200  0 1
P    3  21  0  45 0
P    11  27  300  0 1
P    6  18  0  39 0
P    8  9  0  5 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  27 0
P    8  14  0  16 0
P    7  1  500  0 1
P    5  18  0  40 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 6 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 51 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 220

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  3  6  4  1  0  6075  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    10  12  0  11 0
P    4  6  400  0 1
P    6  3  0  48 0
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  40 0
P    4  15  200  0 1
P    3  21  0  44 0
P    11  27  300  0 1
P    6  18  0  38 0
P    8  9  0  4 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  26 0
P    8  14  0  15 0
P    7  1  500  0 1
P    5  18  0  39 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 5 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 50 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 221

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 XPM..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  2  6  4  1  0  6075  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    10  12  0  10 0
P    4  6  400  0 1
P    6  3  0  47 0
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  39 0
P    4  15  200  0 1
P    3  21  0  43 0
P    11  27  300  0 1
P    6  18  0  37 0
P    8  9  0  3 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  25 0
P    8  14  0  14 0
P    7  1  500  0 1
P    5  18  0  38 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 4 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 49 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 222

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  1  6  4  1  0  6575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    10  12  0  9 0
P    4  6  400  0 1
P    6  3  0  46 0
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  38 0
P    4  15  200  0 1
P    3  21  0  42 0
P    11  27  300  0 1
P    6  18  0  36 0
P    8  9  0  2 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  24 0
P    8  14  0  13 0
P    7  1  0  49 0
P    5  18  0  37 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 3 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 48 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 223

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.....P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  1  6  4  1  0  6575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    10  12  0  8 0
P    4  6  400  0 1
P    6  3  0  45 0
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  37 0
P    4  15  200  0 1
P    3  21  0  41 0
P    11  27  300  0 1
P    6  18  0  35 0
P    8  9  0  1 0
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  23 0
P    8  14  0  12 0
P    7  1  0  48 0
P    5  18  0  36 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 2 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 47 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 224

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X..XXMXX..X.......X.X
04 XPX...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  1  6  4  1  0  6575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    10  12  0  7 0
P    4  6  400  0 1
P    6  3  0  44 0
P    1  16  300  0 1
P    4  1  500  0 1
P    13  4  0  36 0
P    4  15  200  0 1
P    3  21  0  40 0
P    11  27  300  0 1
P    6  18  0  34 0
P    12  22  200  0 1
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  22 0
P    8  14  0  11 0
P    7  1  0  47 0
P    5  18  0  35 0
S    1  1 0 0 1
S    13  15 0 0 1
S    7  15 0 1 0
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 46 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 225

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 XPX.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  1  6  4  1  0  7075  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    10  12  0  6 0
P    4  6  400  0 1
P    6  3  0  43 0
P    1  16  300  0 1
P    4  1  0  49 0
P    13  4  0  35 0
P    4  15  200  0 1
P    3  21  0  39 0
P    11  27  300  0 1
P    6  18  0  33 0
P    12  22  200  0 1
P    3  1  500  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  21 0
P    8  14  0  10 0
P    7  1  0  46 0
P    5  18  0  34 0
S    1  1 0 0 1
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 45 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 226

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  3  1  6  4  1  0  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    10  12  0  5 0
P    4  6  400  0 1
P    6  3  0  42 0
P    1  16  300  0 1
P    4  1  0  48 0
P    13  4  0  34 0
P    4  15  200  0 1
P    3  21  0  38 0
P    11  27  300  0 1
P    6  18  0  32 0
P    12  22  200  0 1
P    3  1  0  49 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  20 0
P    8  14  0  9 0
P    7  1  0  45 0
P    5  18  0  33 0
S    1  1 0 0 1
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 44 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 227

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSX.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  2  1  6  4  1  0  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    10  12  0  4 0
P    4  6  400  0 1
P    6  3  0  41 0
P    1  16  300  0 1
P    4  1  0  47 0
P    13  4  0  33 0
P    4  15  200  0 1
P    3  21  0  37 0
P    11  27  300  0 1
P    6  18  0  31 0
P    12  22  200  0 1
P    3  1  0  48 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  19 0
P    8  14  0  8 0
P    7  1  0  44 0
P    5  18  0  32 0
S    1  1 0 0 1
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 43 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 228

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  1  1  8  6  2  1  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    10  12  0  3 0
P    4  6  400  0 1
P    6  3  0  40 0
P    1  16  300  0 1
P    4  1  0  46 0
P    13  4  0  32 0
P    4  15  200  0 1
P    3  21  0  36 0
P    11  27  300  0 1
P    6  18  0  30 0
P    12  22  200  0 1
P    3  1  0  47 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  18 0
P    8  14  0  7 0
P    7  1  0  43 0
P    5  18  0  31 0
S    1  1 0 84 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 42 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 229

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  2  1  8  6  2  1  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    10  12  0  2 0
P    4  6  400  0 1
P    6  3  0  39 0
P    1  16  300  0 1
P    4  1  0  45 0
P    13  4  0  31 0
P    4  15  200  0 1
P    3  21  0  35 0
P    11  27  300  0 1
P    6  18  0  29 0
P    12  22  200  0 1
P    3  1  0  46 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  17 0
P    8  14  0  6 0
P    7  1  0  42 0
P    5  18  0  30 0
S    1  1 0 83 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 41 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 230

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....D.XSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  3  1  8  6  2  1  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    10  12  0  1 0
P    4  6  400  0 1
P    6  3  0  38 0
P    1  16  300  0 1
P    4  1  0  44 0
P    13  4  0  30 0
P    4  15  200  0 1
P    3  21  0  34 0
P    11  27  300  0 1
P    6  18  0  28 0
P    12  22  200  0 1
P    3  1  0  45 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  16 0
P    8  14  0  5 0
P    7  1  0  41 0
P    5  18  0  29 0
S    1  1 0 82 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 40 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 231

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  1  8  6  2  1  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  37 0
P    1  16  300  0 1
P    4  1  0  43 0
P    13  4  0  29 0
P    4  15  200  0 1
P    3  21  0  33 0
P    11  27  300  0 1
P    6  18  0  27 0
P    12  22  200  0 1
P    3  1  0  44 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  15 0
P    8  14  0  4 0
P    7  1  0  40 0
P    5  18  0  28 0
S    1  1 0 81 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 39 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 232

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  1  8  6  2  1  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  36 0
P    1  16  300  0 1
P    4  1  0  42 0
P    13  4  0  28 0
P    4  15  200  0 1
P    3  21  0  32 0
P    11  27  300  0 1
P    6  18  0  26 0
P    12  22  200  0 1
P    3  1  0  43 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  14 0
P    8  14  0  3 0
P    7  1  0  39 0
P    5  18  0  27 0
S    1  1 0 80 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 38 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 233

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  1  8  6  2  1  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  35 0
P    1  16  300  0 1
P    4  1  0  41 0
P    13  4  0  27 0
P    4  15  200  0 1
P    3  21  0  31 0
P    11  27  300  0 1
P    6  18  0  25 0
P    12  22  200  0 1
P    3  1  0  42 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  13 0
P    8  14  0  2 0
P    7  1  0  38 0
P    5  18  0  26 0
S    1  1 0 79 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 37 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 234

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX.......XXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  1  8  6  2  1  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  34 0
P    1  16  300  0 1
P    4  1  0  40 0
P    13  4  0  26 0
P    4  15  200  0 1
P    3  21  0  30 0
P    11  27  300  0 1
P    6  18  0  24 0
P    12  22  200  0 1
P    3  1  0  41 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  12 0
P    8  14  0  1 0
P    7  1  0  37 0
P    5  18  0  25 0
S    1  1 0 78 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 36 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 235

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  8  1  8  6  2  1  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  33 0
P    1  16  300  0 1
P    4  1  0  39 0
P    13  4  0  25 0
P    4  15  200  0 1
P    3  21  0  29 0
P    11  27  300  0 1
P    6  18  0  23 0
P    12  22  200  0 1
P    3  1  0  40 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  11 0
P    9  18  100  0 1
P    7  1  0  36 0
P    5  18  0  24 0
S    1  1 0 77 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 35 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 236

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  1  8  6  2  1  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  32 0
P    1  16  300  0 1
P    4  1  0  38 0
P    13  4  0  24 0
P    4  15  200  0 1
P    3  21  0  28 0
P    11  27  300  0 1
P    6  18  0  22 0
P    12  22  200  0 1
P    3  1  0  39 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  10 0
P    9  18  100  0 1
P    7  1  0  35 0
P    5  18  0  23 0
S    1  1 0 76 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 34 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 237

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  1  8  6  2  1  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  31 0
P    1  16  300  0 1
P    4  1  0  37 0
P    13  4  0  23 0
P    4  15  200  0 1
P    3  21  0  27 0
P    11  27  300  0 1
P    6  18  0  21 0
P    12  22  200  0 1
P    3  1  0  38 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  9 0
P    9  18  100  0 1
P    7  1  0  34 0
P    5  18  0  22 0
S    1  1 0 75 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 33 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 238

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  11  1  8  6  2  1  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  30 0
P    1  16  300  0 1
P    4  1  0  36 0
P    13  4  0  22 0
P    4  15  200  0 1
P    3  21  0  26 0
P    11  27  300  0 1
P    6  18  0  20 0
P    12  22  200  0 1
P    3  1  0  37 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  8 0
P    9  18  100  0 1
P    7  1  0  33 0
P    5  18  0  21 0
S    1  1 0 74 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 32 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 239

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 XSX............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  8  6  2  1  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  29 0
P    1  16  300  0 1
P    4  1  0  35 0
P    13  4  0  21 0
P    4  15  200  0 1
P    3  21  0  25 0
P    11  27  300  0 1
P    6  18  0  19 0
P    12  22  200  0 1
P    3  1  0  36 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  7 0
P    9  18  100  0 1
P    7  1  0  32 0
P    5  18  0  20 0
S    1  1 0 73 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 31 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 240

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  28 0
P    1  16  300  0 1
P    4  1  0  34 0
P    13  4  0  20 0
P    4  15  200  0 1
P    3  21  0  24 0
P    11  27  300  0 1
P    6  18  0  18 0
P    12  22  200  0 1
P    3  1  0  35 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  6 0
P    9  18  100  0 1
P    7  1  0  31 0
P    5  18  0  19 0
S    1  1 0 72 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 84 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 30 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 241

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  27 0
P    1  16  300  0 1
P    4  1  0  33 0
P    13  4  0  19 0
P    4  15  200  0 1
P    3  21  0  23 0
P    11  27  300  0 1
P    6  18  0  17 0
P    12  22  200  0 1
P    3  1  0  34 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  5 0
P    9  18  100  0 1
P    7  1  0  30 0
P    5  18  0  18 0
S    1  1 0 71 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 83 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 29 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 242

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  26 0
P    1  16  300  0 1
P    4  1  0  32 0
P    13  4  0  18 0
P    4  15  200  0 1
P    3  21  0  22 0
P    11  27  300  0 1
P    6  18  0  16 0
P    12  22  200  0 1
P    3  1  0  33 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  4 0
P    9  18  100  0 1
P    7  1  0  29 0
P    5  18  0  17 0
S    1  1 0 70 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 82 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 28 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 243

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  25 0
P    1  16  300  0 1
P    4  1  0  31 0
P    13  4  0  17 0
P    4  15  200  0 1
P    3  21  0  21 0
P    11  27  300  0 1
P    6  18  0  15 0
P    12  22  200  0 1
P    3  1  0  32 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  3 0
P    9  18  100  0 1
P    7  1  0  28 0
P    5  18  0  16 0
S    1  1 0 69 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 81 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 27 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 244

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  24 0
P    1  16  300  0 1
P    4  1  0  30 0
P    13  4  0  16 0
P    4  15  200  0 1
P    3  21  0  20 0
P    11  27  300  0 1
P    6  18  0  14 0
P    12  22  200  0 1
P    3  1  0  31 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  2 0
P    9  18  100  0 1
P    7  1  0  27 0
P    5  18  0  15 0
S    1  1 0 68 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 80 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 26 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 245

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P.....DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  23 0
P    1  16  300  0 1
P    4  1  0  29 0
P    13  4  0  15 0
P    4  15  200  0 1
P    3  21  0  19 0
P    11  27  300  0 1
P    6  18  0  13 0
P    12  22  200  0 1
P    3  1  0  30 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    10  13  0  1 0
P    9  18  100  0 1
P    7  1  0  26 0
P    5  18  0  14 0
S    1  1 0 67 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 79 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 25 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 246

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  11  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  22 0
P    1  16  300  0 1
P    4  1  0  28 0
P    13  4  0  14 0
P    4  15  200  0 1
P    3  21  0  18 0
P    11  27  300  0 1
P    6  18  0  12 0
P    12  22  200  0 1
P    3  1  0  29 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  25 0
P    5  18  0  13 0
S    1  1 0 66 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 78 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 24 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 247

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  21 0
P    1  16  300  0 1
P    4  1  0  27 0
P    13  4  0  13 0
P    4  15  200  0 1
P    3  21  0  17 0
P    11  27  300  0 1
P    6  18  0  11 0
P    12  22  200  0 1
P    3  1  0  28 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  24 0
P    5  18  0  12 0
S    1  1 0 65 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 77 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 23 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 248

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  20 0
P    1  16  300  0 1
P    4  1  0  26 0
P    13  4  0  12 0
P    4  15  200  0 1
P    3  21  0  16 0
P    11  27  300  0 1
P    6  18  0  10 0
P    12  22  200  0 1
P    3  1  0  27 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  23 0
P    5  18  0  11 0
S    1  1 0 64 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 76 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 22 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 249

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  19 0
P    1  16  300  0 1
P    4  1  0  25 0
P    13  4  0  11 0
P    4  15  200  0 1
P    3  21  0  15 0
P    11  27  300  0 1
P    6  18  0  9 0
P    12  22  200  0 1
P    3  1  0  26 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  22 0
P    5  18  0  10 0
S    1  1 0 63 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 75 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 21 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 250

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  18 0
P    1  16  300  0 1
P    4  1  0  24 0
P    13  4  0  10 0
P    4  15  200  0 1
P    3  21  0  14 0
P    11  27  300  0 1
P    6  18  0  8 0
P    12  22  200  0 1
P    3  1  0  25 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  21 0
P    5  18  0  9 0
S    1  1 0 62 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 74 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 20 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 251

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  17 0
P    1  16  300  0 1
P    4  1  0  23 0
P    13  4  0  9 0
P    4  15  200  0 1
P    3  21  0  13 0
P    11  27  300  0 1
P    6  18  0  7 0
P    12  22  200  0 1
P    3  1  0  24 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  20 0
P    5  18  0  8 0
S    1  1 0 61 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 73 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 19 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 252

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  16 0
P    1  16  300  0 1
P    4  1  0  22 0
P    13  4  0  8 0
P    4  15  200  0 1
P    3  21  0  12 0
P    11  27  300  0 1
P    6  18  0  6 0
P    12  22  200  0 1
P    3  1  0  23 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  19 0
P    5  18  0  7 0
S    1  1 0 60 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 72 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 18 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 253

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  15 0
P    1  16  300  0 1
P    4  1  0  21 0
P    13  4  0  7 0
P    4  15  200  0 1
P    3  21  0  11 0
P    11  27  300  0 1
P    6  18  0  5 0
P    12  22  200  0 1
P    3  1  0  22 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  18 0
P    5  18  0  6 0
S    1  1 0 59 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 71 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 17 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 254

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  14 0
P    1  16  300  0 1
P    4  1  0  20 0
P    13  4  0  6 0
P    4  15  200  0 1
P    3  21  0  10 0
P    11  27  300  0 1
P    6  18  0  4 0
P    12  22  200  0 1
P    3  1  0  21 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  17 0
P    5  18  0  5 0
S    1  1 0 58 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 70 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 16 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 255

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  13 0
P    1  16  300  0 1
P    4  1  0  19 0
P    13  4  0  5 0
P    4  15  200  0 1
P    3  21  0  9 0
P    11  27  300  0 1
P    6  18  0  3 0
P    12  22  200  0 1
P    3  1  0  20 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  16 0
P    5  18  0  4 0
S    1  1 0 57 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 69 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 15 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 256

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  12 0
P    1  16  300  0 1
P    4  1  0  18 0
P    13  4  0  4 0
P    4  15  200  0 1
P    3  21  0  8 0
P    11  27  300  0 1
P    6  18  0  2 0
P    12  22  200  0 1
P    3  1  0  19 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  15 0
P    5  18  0  3 0
S    1  1 0 56 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 68 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 14 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 257

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P............X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  11 0
P    1  16  300  0 1
P    4  1  0  17 0
P    13  4  0  3 0
P    4  15  200  0 1
P    3  21  0  7 0
P    11  27  300  0 1
P    6  18  0  1 0
P    12  22  200  0 1
P    3  1  0  18 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  14 0
P    5  18  0  2 0
S    1  1 0 55 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 67 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 13 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 258

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............D...........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  10 0
P    1  16  300  0 1
P    4  1  0  16 0
P    13  4  0  2 0
P    4  15  200  0 1
P    3  21  0  6 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  17 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  13 0
P    5  18  0  1 0
S    1  1 0 54 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 66 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 12 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 259

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  9 0
P    1  16  300  0 1
P    4  1  0  15 0
P    13  4  0  1 0
P    4  15  200  0 1
P    3  21  0  5 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  16 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  12 0
P    10  17  500  0 1
S    1  1 0 53 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 65 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 11 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 260

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P.........S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  8 0
P    1  16  300  0 1
P    4  1  0  14 0
P    13  5  300  0 1
P    4  15  200  0 1
P    3  21  0  4 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  15 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  11 0
P    10  17  500  0 1
S    1  1 0 52 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 64 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 10 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 261

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P.........S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  7 0
P    1  16  300  0 1
P    4  1  0  13 0
P    13  5  300  0 1
P    4  15  200  0 1
P    3  21  0  3 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  14 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  10 0
P    10  17  500  0 1
S    1  1 0 51 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 63 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 9 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 262

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P.........S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  6 0
P    1  16  300  0 1
P    4  1  0  12 0
P    13  5  300  0 1
P    4  15  200  0 1
P    3  21  0  2 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  13 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  9 0
P    10  17  500  0 1
S    1  1 0 50 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 62 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 8 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 263

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P.........S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  5 0
P    1  16  300  0 1
P    4  1  0  11 0
P    13  5  300  0 1
P    4  15  200  0 1
P    3  21  0  1 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  12 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  8 0
P    10  17  500  0 1
S    1  1 0 49 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 61 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 7 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 264

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P..P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  11  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  4 0
P    1  16  300  0 1
P    4  1  0  10 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  11 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  7 0
P    10  17  500  0 1
S    1  1 0 48 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 60 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 6 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 265

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P..P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  3 0
P    1  16  300  0 1
P    4  1  0  9 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  10 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  6 0
P    10  17  500  0 1
S    1  1 0 47 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 59 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 5 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 266

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P..P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  2 0
P    1  16  300  0 1
P    4  1  0  8 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  9 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  5 0
P    10  17  500  0 1
S    1  1 0 46 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 58 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 4 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 267

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP..........X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P..P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  11  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    6  3  0  1 0
P    1  16  300  0 1
P    4  1  0  7 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  8 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  4 0
P    10  17  500  0 1
S    1  1 0 45 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 57 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 3 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 268

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P..P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    4  1  0  6 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  7 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  3 0
P    10  17  500  0 1
S    1  1 0 44 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 56 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 2 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 269

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..P..P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    4  1  0  5 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  6 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  2 0
P    10  17  500  0 1
S    1  1 0 43 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 55 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    3  22 0 1 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 270

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P...P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    4  1  0  4 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  5 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    7  1  0  1 0
P    10  17  500  0 1
S    1  1 0 42 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 54 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 271

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    4  1  0  3 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  4 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 41 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 53 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 272

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  8  1  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    4  1  0  2 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  3 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 40 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 52 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 273

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.............P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  1  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    4  1  0  1 0
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  2 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 39 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 51 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 274

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  2  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    3  1  0  1 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 38 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 50 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 275

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  3  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 37 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 49 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 276

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  4  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 36 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 48 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 277

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  4  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 35 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 47 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 278

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  4  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 34 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 46 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 279

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  4  10  8  3  2  7575  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 33 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 45 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 280

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X...P.....R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  5  10  8  3  2  7575  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  400  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 32 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 44 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 281

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  6  10  8  3  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  49 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 31 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 43 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 282

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  5  10  8  3  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  48 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 30 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 42 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 283

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  4  10  8  3  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  47 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 29 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 41 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 284

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.A.....XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  3  4  10  8  3  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  46 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 28 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 40 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 285

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  2  4  11  8  3  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  45 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 27 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 39 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 54 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 286

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  2  5  11  8  3  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  44 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 26 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 38 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 53 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 287

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  2  6  11  8  3  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  43 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 25 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 37 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 52 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 288

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  2  7  11  8  3  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  42 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 24 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 36 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 51 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 289

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  2  8  11  8  3  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  41 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 23 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 35 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 50 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 290

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  2  9  11  8  3  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  40 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 22 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 34 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 49 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 291

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  3  9  11  8  3  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  39 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 21 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 33 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 48 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 292

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  9  11  8  3  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  38 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 20 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 32 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 47 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 293

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  10  11  8  3  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  37 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 19 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 31 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 46 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 294

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X.........R..P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  11  11  8  3  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  36 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 18 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 30 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 0 1
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 45 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 295

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X.SXXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  12  11  8  4  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  35 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 17 0
S    13  15 0 0 1
S    3  12 0 0 1
S    13  1 0 29 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 64 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 44 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 296

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  3  12  11  8  4  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  34 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 16 0
S    13  15 0 0 1
S    3  12 0 84 0
S    13  1 0 28 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 63 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 43 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 297

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  12  11  8  4  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  33 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 15 0
S    13  15 0 0 1
S    3  12 0 83 0
S    13  1 0 27 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 62 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 42 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 298

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  13  11  8  4  2  7975  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  32 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 14 0
S    13  15 0 0 1
S    3  12 0 82 0
S    13  1 0 26 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 61 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 41 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 299

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX..P....XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  14  11  8  4  2  7975  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  31 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  400  0 1
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 13 0
S    13  15 0 0 1
S    3  12 0 81 0
S    13  1 0 25 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 60 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 40 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 300

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  14  11  8  4  2  8375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  30 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  49 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 12 0
S    13  15 0 0 1
S    3  12 0 80 0
S    13  1 0 24 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 59 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 39 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 301

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  14  11  8  4  2  8375  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  29 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  48 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 11 0
S    13  15 0 0 1
S    3  12 0 79 0
S    13  1 0 23 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 58 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 38 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 302

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.XPX.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  14  11  8  4  2  8375  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  28 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  47 0
P    8  14  300  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 10 0
S    13  15 0 0 1
S    3  12 0 78 0
S    13  1 0 22 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 57 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 37 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 303

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  8  14  11  8  4  2  8675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  27 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  46 0
P    8  14  0  49 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 9 0
S    13  15 0 0 1
S    3  12 0 77 0
S    13  1 0 21 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 56 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 36 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 304

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  14  11  8  4  2  8675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  26 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  45 0
P    8  14  0  48 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 8 0
S    13  15 0 0 1
S    3  12 0 76 0
S    13  1 0 20 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 55 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 35 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 305

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  13  11  8  4  2  8675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  25 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  44 0
P    8  14  0  47 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 7 0
S    13  15 0 0 1
S    3  12 0 75 0
S    13  1 0 19 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 54 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 34 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 306

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  12  11  8  4  2  8675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  24 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  43 0
P    8  14  0  46 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 6 0
S    13  15 0 0 1
S    3  12 0 74 0
S    13  1 0 18 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 53 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 33 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 307

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  12  11  8  4  2  8675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  23 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  42 0
P    8  14  0  45 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 5 0
S    13  15 0 0 1
S    3  12 0 73 0
S    13  1 0 17 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 52 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 32 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 308

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  11  11  8  4  2  8675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  22 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  41 0
P    8  14  0  44 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 4 0
S    13  15 0 0 1
S    3  12 0 72 0
S    13  1 0 16 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 51 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 31 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 309

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  10  11  8  4  2  8675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  21 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  40 0
P    8  14  0  43 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 3 0
S    13  15 0 0 1
S    3  12 0 71 0
S    13  1 0 15 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 50 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 30 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 310

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  9  11  8  4  2  8675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  20 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  39 0
P    8  14  0  42 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 2 0
S    13  15 0 0 1
S    3  12 0 70 0
S    13  1 0 14 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 49 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 29 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 311

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  8  11  8  4  2  8675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  19 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  38 0
P    8  14  0  41 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    1  1 0 1 0
S    13  15 0 0 1
S    3  12 0 69 0
S    13  1 0 13 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 48 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 28 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 312

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  11  8  11  8  4  2  8675  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  18 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  37 0
P    8  14  0  40 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 68 0
S    13  1 0 12 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 47 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 27 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 313

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA.P......S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  12  8  11  8  4  2  8675  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  17 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  200  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  36 0
P    8  14  0  39 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 67 0
S    13  1 0 11 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 46 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 26 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 314

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA........S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  8  11  8  4  2  8875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  16 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  0  49 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  35 0
P    8  14  0  38 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 66 0
S    13  1 0 10 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 45 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 25 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 315

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..PA........S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  7  11  8  4  2  8875  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  15 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  0  48 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  34 0
P    8  14  0  37 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 65 0
S    13  1 0 9 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 44 0
A    13  6 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 24 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 316

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X..P.........S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  6  12  8  4  2  8875  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  14 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  300  0 1
P    4  15  200  0 1
P    13  8  0  47 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  33 0
P    8  14  0  36 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 64 0
S    13  1 0 8 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 43 0
A    13  6 0 54 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 23 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 317

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  5  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  13 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  49 0
P    4  15  200  0 1
P    13  8  0  46 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  32 0
P    8  14  0  35 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 63 0
S    13  1 0 7 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 42 0
A    13  6 0 53 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 22 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 318

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  6  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  12 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  48 0
P    4  15  200  0 1
P    13  8  0  45 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  31 0
P    8  14  0  34 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 62 0
S    13  1 0 6 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 41 0
A    13  6 0 52 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 21 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 319

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  7  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  11 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  47 0
P    4  15  200  0 1
P    13  8  0  44 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  30 0
P    8  14  0  33 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 61 0
S    13  1 0 5 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 40 0
A    13  6 0 51 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 20 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 320

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  13  8  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  10 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  46 0
P    4  15  200  0 1
P    13  8  0  43 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  29 0
P    8  14  0  32 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 60 0
S    13  1 0 4 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 39 0
A    13  6 0 50 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 19 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 321

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  9  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  9 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  45 0
P    4  15  200  0 1
P    13  8  0  42 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  28 0
P    8  14  0  31 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 59 0
S    13  1 0 3 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 38 0
A    13  6 0 49 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 18 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 322

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  12  9  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  8 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  44 0
P    4  15  200  0 1
P    13  8  0  41 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  27 0
P    8  14  0  30 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 58 0
S    13  1 0 2 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 37 0
A    13  6 0 48 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 17 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 323

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.X...D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  11  9  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  7 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  43 0
P    4  15  200  0 1
P    13  8  0  40 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  26 0
P    8  14  0  29 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 57 0
S    13  1 0 1 0
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 36 0
A    13  6 0 47 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 16 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 324

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  9  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  6 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  42 0
P    4  15  200  0 1
P    13  8  0  39 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  25 0
P    8  14  0  28 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 56 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 35 0
A    13  6 0 46 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 15 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 325

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  10  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  5 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  41 0
P    4  15  200  0 1
P    13  8  0  38 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  24 0
P    8  14  0  27 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 55 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 34 0
A    13  6 0 45 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 14 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 326

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  11  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  4 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  40 0
P    4  15  200  0 1
P    13  8  0  37 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  23 0
P    8  14  0  26 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 54 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 33 0
A    13  6 0 44 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 13 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 327

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  12  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  3 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  39 0
P    4  15  200  0 1
P    13  8  0  36 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  22 0
P    8  14  0  25 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 53 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 32 0
A    13  6 0 43 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 12 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 328

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  13  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  2 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  38 0
P    4  15  200  0 1
P    13  8  0  35 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  21 0
P    8  14  0  24 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 52 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 31 0
A    13  6 0 42 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 11 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 329

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  14  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    4  6  0  1 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  37 0
P    4  15  200  0 1
P    13  8  0  34 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  20 0
P    8  14  0  23 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 51 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 30 0
A    13  6 0 41 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 10 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 330

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  14  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  36 0
P    4  15  200  0 1
P    13  8  0  33 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  19 0
P    8  14  0  22 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 50 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 29 0
A    13  6 0 40 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 9 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 331

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  8  14  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  35 0
P    4  15  200  0 1
P    13  8  0  32 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  18 0
P    8  14  0  21 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 49 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 28 0
A    13  6 0 39 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 8 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 332

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.XSX.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  14  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  34 0
P    4  15  200  0 1
P    13  8  0  31 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  17 0
P    8  14  0  20 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 0 1
S    13  15 0 0 1
S    3  12 0 48 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 27 0
A    13  6 0 38 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 7 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 333

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  14  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  33 0
P    4  15  200  0 1
P    13  8  0  30 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  16 0
P    8  14  0  19 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 84 0
S    13  15 0 0 1
S    3  12 0 47 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 26 0
A    13  6 0 37 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 6 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 334

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  14  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  32 0
P    4  15  200  0 1
P    13  8  0  29 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  15 0
P    8  14  0  18 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 83 0
S    13  15 0 0 1
S    3  12 0 46 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 25 0
A    13  6 0 36 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 5 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 335

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  5  13  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  31 0
P    4  15  200  0 1
P    13  8  0  28 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  14 0
P    8  14  0  17 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 82 0
S    13  15 0 0 1
S    3  12 0 45 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 24 0
A    13  6 0 35 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 4 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 336

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  12  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  30 0
P    4  15  200  0 1
P    13  8  0  27 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  13 0
P    8  14  0  16 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 81 0
S    13  15 0 0 1
S    3  12 0 44 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 23 0
A    13  6 0 34 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 3 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 337

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  12  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  29 0
P    4  15  200  0 1
P    13  8  0  26 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  12 0
P    8  14  0  15 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 80 0
S    13  15 0 0 1
S    3  12 0 43 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 22 0
A    13  6 0 33 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 2 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 338

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  11  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  28 0
P    4  15  200  0 1
P    13  8  0  25 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  11 0
P    8  14  0  14 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 79 0
S    13  15 0 0 1
S    3  12 0 42 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 21 0
A    13  6 0 32 0
A    8  29 0 0 1
A    2  14 0 0 1
A    2  4 0 1 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 339

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.XA...........P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  10  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  27 0
P    4  15  200  0 1
P    13  8  0  24 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  10 0
P    8  14  0  13 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 78 0
S    13  15 0 0 1
S    3  12 0 41 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 20 0
A    13  6 0 31 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 340

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.XA...........P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  9  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  26 0
P    4  15  200  0 1
P    13  8  0  23 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  9 0
P    8  14  0  12 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 77 0
S    13  15 0 0 1
S    3  12 0 40 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 19 0
A    13  6 0 30 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 341

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.XA...........P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  8  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  25 0
P    4  15  200  0 1
P    13  8  0  22 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  8 0
P    8  14  0  11 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 76 0
S    13  15 0 0 1
S    3  12 0 39 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 18 0
A    13  6 0 29 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 342

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.XA...........P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  7  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  24 0
P    4  15  200  0 1
P    13  8  0  21 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  7 0
P    8  14  0  10 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 75 0
S    13  15 0 0 1
S    3  12 0 38 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 17 0
A    13  6 0 28 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 343

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.XA...........P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  6  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  23 0
P    4  15  200  0 1
P    13  8  0  20 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  6 0
P    8  14  0  9 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 74 0
S    13  15 0 0 1
S    3  12 0 37 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 16 0
A    13  6 0 27 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 344

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.XA...........P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  5  12  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  22 0
P    4  15  200  0 1
P    13  8  0  19 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  5 0
P    8  14  0  8 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 73 0
S    13  15 0 0 1
S    3  12 0 36 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 15 0
A    13  6 0 26 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 345

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.XA...........P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  4  4  12  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  21 0
P    4  15  200  0 1
P    13  8  0  18 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  4 0
P    8  14  0  7 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 72 0
S    13  15 0 0 1
S    3  12 0 35 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 14 0
A    13  6 0 25 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 346

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  4  3  13  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  20 0
P    4  15  200  0 1
P    13  8  0  17 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  3 0
P    8  14  0  6 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 71 0
S    13  15 0 0 1
S    3  12 0 34 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 13 0
A    13  6 0 24 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 54 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 347

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  5  3  13  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  19 0
P    4  15  200  0 1
P    13  8  0  16 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  2 0
P    8  14  0  5 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 70 0
S    13  15 0 0 1
S    3  12 0 33 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 12 0
A    13  6 0 23 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 53 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 348

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...........P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  6  3  13  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  18 0
P    4  15  200  0 1
P    13  8  0  15 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    5  14  0  1 0
P    8  14  0  4 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 69 0
S    13  15 0 0 1
S    3  12 0 32 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 11 0
A    13  6 0 22 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 52 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 349

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  3  13  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  17 0
P    4  15  200  0 1
P    13  8  0  14 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    8  14  0  3 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 68 0
S    13  15 0 0 1
S    3  12 0 31 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 10 0
A    13  6 0 21 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 51 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 m l
3 m r


round 350

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  2  13  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  16 0
P    4  15  200  0 1
P    13  8  0  13 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    8  14  0  2 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 67 0
S    13  15 0 0 1
S    3  12 0 30 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 9 0
A    13  6 0 20 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 50 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 m l
3 m l


round 351

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X.......XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  15 0
P    4  15  200  0 1
P    13  8  0  12 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    8  14  0  1 0
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 66 0
S    13  15 0 0 1
S    3  12 0 29 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 8 0
A    13  6 0 19 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 49 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 352

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  8  1  13  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  14 0
P    4  15  200  0 1
P    13  8  0  11 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 65 0
S    13  15 0 0 1
S    3  12 0 28 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 7 0
A    13  6 0 18 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 48 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 353

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  13 0
P    4  15  200  0 1
P    13  8  0  10 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 64 0
S    13  15 0 0 1
S    3  12 0 27 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 6 0
A    13  6 0 17 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 47 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 354

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  1  13  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  12 0
P    4  15  200  0 1
P    13  8  0  9 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 63 0
S    13  15 0 0 1
S    3  12 0 26 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 5 0
A    13  6 0 16 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 46 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 355

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  11  1  13  8  4  2  9175  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  11 0
P    4  15  200  0 1
P    13  8  0  8 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 62 0
S    13  15 0 0 1
S    3  12 0 25 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 4 0
A    13  6 0 15 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 45 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 356

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 XPX............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  12  1  13  8  4  2  9175  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  100  0 1
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  10 0
P    4  15  200  0 1
P    13  8  0  7 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 61 0
S    13  15 0 0 1
S    3  12 0 24 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 3 0
A    13  6 0 14 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 44 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 357

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  13  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  49 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  9 0
P    4  15  200  0 1
P    13  8  0  6 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 60 0
S    13  15 0 0 1
S    3  12 0 23 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 2 0
A    13  6 0 13 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 43 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 358

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P.......P.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  12  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  48 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  8 0
P    4  15  200  0 1
P    13  8  0  5 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 59 0
S    13  15 0 0 1
S    3  12 0 22 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    4  12 0 1 0
A    13  6 0 12 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 42 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 359

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  11  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  47 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  7 0
P    4  15  200  0 1
P    13  8  0  4 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 58 0
S    13  15 0 0 1
S    3  12 0 21 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 11 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 41 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 360

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  46 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  6 0
P    4  15  200  0 1
P    13  8  0  3 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 57 0
S    13  15 0 0 1
S    3  12 0 20 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 10 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 40 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 361

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  45 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  5 0
P    4  15  200  0 1
P    13  8  0  2 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 56 0
S    13  15 0 0 1
S    3  12 0 19 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 9 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 39 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 362

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXX......PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  44 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  4 0
P    4  15  200  0 1
P    13  8  0  1 0
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 55 0
S    13  15 0 0 1
S    3  12 0 18 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 8 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 38 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 363

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  43 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  3 0
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 54 0
S    13  15 0 0 1
S    3  12 0 17 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 7 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 37 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 364

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  42 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  2 0
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 53 0
S    13  15 0 0 1
S    3  12 0 16 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 6 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 36 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 365

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  41 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    13  5  0  1 0
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 52 0
S    13  15 0 0 1
S    3  12 0 15 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 5 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 35 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 366

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  40 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 51 0
S    13  15 0 0 1
S    3  12 0 14 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 4 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 34 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 367

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  39 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 50 0
S    13  15 0 0 1
S    3  12 0 13 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 3 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 33 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 368

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  38 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 49 0
S    13  15 0 0 1
S    3  12 0 12 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 2 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 32 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 369

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X.....X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  37 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 48 0
S    13  15 0 0 1
S    3  12 0 11 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    13  6 0 1 0
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 31 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 370

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  36 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 47 0
S    13  15 0 0 1
S    3  12 0 10 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 30 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 371

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  35 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 46 0
S    13  15 0 0 1
S    3  12 0 9 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 29 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 372

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  8  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  34 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 45 0
S    13  15 0 0 1
S    3  12 0 8 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 28 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m l
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 373

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  8  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  33 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 44 0
S    13  15 0 0 1
S    3  12 0 7 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 27 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 374

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  32 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 43 0
S    13  15 0 0 1
S    3  12 0 6 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 26 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 375

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  31 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 42 0
S    13  15 0 0 1
S    3  12 0 5 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 25 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 376

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  30 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 41 0
S    13  15 0 0 1
S    3  12 0 4 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 24 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 377

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  29 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 40 0
S    13  15 0 0 1
S    3  12 0 3 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 23 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 378

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  28 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 39 0
S    13  15 0 0 1
S    3  12 0 2 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 22 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 379

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X....P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  27 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 38 0
S    13  15 0 0 1
S    3  12 0 1 0
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 21 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 380

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  26 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 37 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 20 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 381

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  25 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 36 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 19 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 382

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  24 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 35 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 18 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 383

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  23 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 34 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 17 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m l
3 m r

actions_done
player action direction
0 m r
1 m l
2 u n
3 m r


round 384

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  22 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 33 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 16 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m t
3 m l

actions_done
player action direction
0 m l
1 m r
2 m t
3 m l


round 385

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  5  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  21 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 32 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 15 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 386

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  20 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 31 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 14 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 u n
3 m l


round 387

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  19 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 30 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 13 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 388

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  18 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 29 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 12 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 389

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  17 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 28 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 11 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m t
3 m r

actions_done
player action direction
0 m r
1 m l
2 m t
3 m r


round 390

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  5  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  16 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 27 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 10 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 391

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  6  1  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  15 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 26 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 9 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 392

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  1  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  14 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 25 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 8 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 393

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  2  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  13 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 24 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 7 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m r
3 m r

actions_done
player action direction
0 m r
1 m l
2 m r
3 m r


round 394

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  7  3  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  12 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 23 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 6 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 395

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  7  4  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  11 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 22 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 5 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 396

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  8  4  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  10 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 21 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 4 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m b
3 m l

actions_done
player action direction
0 m l
1 m r
2 m b
3 m l


round 397

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  9  4  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  9 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 20 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 3 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 398

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  10  4  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  8 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 19 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 2 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 m l

actions_done
player action direction
0 m l
1 m r
2 m r
3 m l


round 399

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X...P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  26  5  6  3  1  4426  0  a
1  1  3  23  4  3  1  0  3175  0  a
2  2  10  5  13  8  4  2  9275  0  a
3  3  4  26  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  7 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 18 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    4  3 0 1 0
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

actions_asked
player action direction
0 m r
1 m l
2 m b
3 m r

actions_done
player action direction
0 m r
1 m l
2 m b
3 m r


round 400

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 X.X.A.P......RP.P.......D...X.X
02 X.X.......XXXXAR.XXXX.......X.X
03 X.X.......X..XXMXX..X.......XPX
04 X.X............P...P........X.X
05 X.X..XXX.XXX.......XXX.XXX..X.X
06 X.X..X.....X.X.X.X.X...A.X..X.X
07 X.M..XXXMXXX.X...X.XXXMXXX..M.X
08 X.X..X.....X.X.X.X.XS..D.X..XAX
09 X.X..XXX.XXXP.....PXXX.XXX..X.X
10 X.X.............DP....P.....X.X
11 X.X.......X..XXMXX..X.P....PX.X
12 X.X..S.P..XXXX...XXXX.P.P.P.X.X
13 X.X............S....P...P.DPXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  27  5  6  3  1  4426  0  a
1  1  3  22  4  3  1  0  3175  0  a
2  2  11  5  13  8  4  2  9275  0  a
3  3  4  27  5  6  4  2  3326  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    13  27  100  0 1
P    13  1  0  6 0
P    10  22  400  0 1
P    1  16  300  0 1
P    1  14  500  0 1
P    3  29  400  0 1
P    4  15  200  0 1
P    9  12  100  0 1
P    11  27  300  0 1
P    4  19  200  0 1
P    12  22  200  0 1
P    1  6  500  0 1
P    12  7  200  0 1
P    12  26  500  0 1
P    13  20  400  0 1
P    11  22  300  0 1
P    13  24  400  0 1
P    9  18  100  0 1
P    12  24  400  0 1
P    10  17  500  0 1
S    6  14 0 17 0
S    13  15 0 0 1
S    12  5 0 0 1
S    8  20 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    1  13 0 0 1
A    6  23 0 0 1
A    8  29 0 0 1
A    2  14 0 0 1
A    1  4 0 0 1
D    1  24 0 0 1
D    13  26 0 0 1
D    8  23 0 0 1
D    10  16 0 0 1

