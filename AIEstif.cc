#include "Player.hh"

using namespace std;
#define PLAYER_NAME Estif

typedef vector<vector<int> > Matrix;
typedef vector<vector<Pos> > Matrix2;

struct PLAYER_NAME : public Player {

    /**
     * No tocar
     */
    static Player* factory () {
        return new PLAYER_NAME;
    }
    
    /**
     * Attributes for your player can be defined here.
     */    
	const int MAX_DIST = 10000;

    Dir num2dir (int i, int j) {
		
		Dir ret = Top;
		if(i == -1) ret = Top;
		else if(j == -1) ret = Left; 
		else if(j == 1) ret = Right;
		else if(i == 1) ret = Bottom;   
		else cerr << "Error: num2dir" << endl;
		return ret;
	}
	
    virtual void play () {
	const Poquemon& p = poquemon(me());
    
	unsigned int Distances[rows()][cols()];
	for(int i= 0; i < rows(); i++){
	    for(int j= 0; j < cols(); j++){
		Distances[i][j] = MAX_DIST;
	}
	}
	Matrix2 Path (rows(), vector<Pos>(cols()));
	vector<Pos> Pokemons(3);
	vector<Pos> Stones(nb_stone());
	vector<Pos> Points(nb_point());
	vector<Pos> Attacks(nb_attack());
	vector<Pos> Defenses(nb_defense());
	vector<Pos> Scopes(nb_scope());
	

	unsigned char pok_i, sto_i, poi_i, att_i, def_i, sco_i;
	pok_i = sto_i = poi_i = att_i = def_i = sco_i = 0;
	

	//BFS_Map
	
	queue<Pos> Q;
	vector<vector<bool>> enc(rows(), vector<bool>(cols(), false)); 
	Q.push(p.pos);
	enc[p.pos.i][p.pos.j]=true;
	Distances[p.pos.i][p.pos.j]=0;
	Path[p.pos.i][p.pos.j]=Pos(0,0);
	while(not Q.empty()){
		Pos posAct = Q.front();
		Q.pop();
		//top
		if(pos_ok(posAct,Top) and (cell_type(Pos(posAct.i-1, posAct.j)) != Wall)) {
			int idCell = cell_id(Pos(posAct.i-1, posAct.j));
			if(idCell == -1){ //No pokemon or not seen
				if(Distances[posAct.i-1][posAct.j] == MAX_DIST){
					Path[posAct.i-1][posAct.j] = posAct;
					Distances[posAct.i-1][posAct.j] = Distances[posAct.i][posAct.j]+1;
					Q.push(Pos(posAct.i-1, posAct.j));
					if(not enc[posAct.i-1][posAct.j]){
    					 enc[posAct.i-1][posAct.j] = true;
    					 if(cell_type(Pos(posAct.i-1, posAct.j)) == Stone) {Stones[sto_i] = Pos(posAct.i-1, posAct.j);++sto_i;}
    					 else if(cell_type(Pos(posAct.i-1, posAct.j)) == Point){ Points[poi_i] = Pos(posAct.i-1, posAct.j);++poi_i;}
    					 else if(cell_type(Pos(posAct.i-1, posAct.j)) == Attack){ Attacks[att_i] = Pos(posAct.i-1, posAct.j);++att_i;}
    					 else if(cell_type(Pos(posAct.i-1, posAct.j)) == Defense){Defenses[def_i] = Pos(posAct.i-1, posAct.j);++def_i;}
    					 else if(cell_type(Pos(posAct.i-1, posAct.j)) == Scope) {Scopes[sco_i] = Pos(posAct.i-1, posAct.j);++sco_i;}
				    }
				}	
			}
			else{ //Will pokemon appeared !
				if(not enc[posAct.i-1][posAct.j]){
    					 enc[posAct.i-1][posAct.j] = true;
					 Pokemons[pok_i] = Pos(posAct.i-1, posAct.j);
					 ++pok_i;	
				}
			}
		}
		//right

		if(pos_ok(posAct,Right) and (cell_type(Pos(posAct.i, posAct.j+1)) != Wall)) {
			int idCell = cell_id(Pos(posAct.i, posAct.j+1));
			if(idCell == -1){ //No pokemon or not seen
				//if(Distances[posAct.i][posAct.j+1] > Distances[posAct.i][posAct.j]+1){
				  if(Distances[posAct.i][posAct.j+1] == MAX_DIST){
					Path[posAct.i][posAct.j+1] = posAct;
					Distances[posAct.i][posAct.j+1] = Distances[posAct.i][posAct.j]+1;
					Q.push(Pos(posAct.i, posAct.j+1));
					if(not enc[posAct.i][posAct.j+1]){
    					 enc[posAct.i][posAct.j+1] = true;
					 
    					 if(cell_type(Pos(posAct.i, posAct.j+1)) == Stone) {Stones[sto_i] = Pos(posAct.i, posAct.j+1);++sto_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j+1)) == Point){ Points[poi_i] = Pos(posAct.i, posAct.j+1);++poi_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j+1)) == Attack){ Attacks[att_i] = Pos(posAct.i, posAct.j+1);++att_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j+1)) == Defense){Defenses[def_i] = Pos(posAct.i, posAct.j+1);++def_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j+1)) == Scope) {Scopes[sco_i] = Pos(posAct.i, posAct.j+1);++sco_i;}
    					 
				    }
				}	
			}
			
			else{ //Will pokemon appeared !
				if(not enc[posAct.i][posAct.j+1]){
    					 enc[posAct.i][posAct.j+1] = true;
					 Pokemons[pok_i] = Pos(posAct.i, posAct.j+1);
					 ++pok_i;	
				}
			}
			
		}
		//left

		if(pos_ok(posAct,Left) and (cell_type(Pos(posAct.i, posAct.j-1)) != Wall)) {
			int idCell = cell_id(Pos(posAct.i, posAct.j-1));
			if(idCell == -1){ //No pokemon or not seen
				if(Distances[posAct.i][posAct.j-1] == MAX_DIST){
					Path[posAct.i][posAct.j-1] = posAct;
					Distances[posAct.i][posAct.j-1] = Distances[posAct.i][posAct.j]+1;
					Q.push(Pos(posAct.i, posAct.j-1));
					if(not enc[posAct.i][posAct.j-1]){
    					 enc[posAct.i][posAct.j-1] = true;
    					 if(cell_type(Pos(posAct.i, posAct.j-1)) == Stone) {Stones[sto_i] = Pos(posAct.i, posAct.j-1);++sto_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j-1)) == Point){ Points[poi_i] = Pos(posAct.i, posAct.j-1);++poi_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j-1)) == Attack){ Attacks[att_i] = Pos(posAct.i, posAct.j-1);++att_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j-1)) == Defense){ Defenses[def_i] = Pos(posAct.i, posAct.j-1);++def_i;}
    					 else if(cell_type(Pos(posAct.i, posAct.j-1)) == Scope) {Scopes[sco_i] = Pos(posAct.i, posAct.j-1);++sco_i;}
				    }
				}	
			}
			else{ //Will pokemon appeared !
				if(not enc[posAct.i][posAct.j-1]){
    					 enc[posAct.i][posAct.j-1] = true;
					 Pokemons[pok_i] = Pos(posAct.i, posAct.j-1);
					 ++pok_i;	
				}
			}
		}
		
		//bottom
		if(pos_ok(posAct,Bottom) and (cell_type(Pos(posAct.i+1, posAct.j)) != Wall)) {
			int idCell = cell_id(Pos(posAct.i+1, posAct.j));
			if(idCell == -1){ //No pokemon or not seen
				if(Distances[posAct.i+1][posAct.j] == MAX_DIST){
					Path[posAct.i+1][posAct.j] = posAct;
					Distances[posAct.i+1][posAct.j] = Distances[posAct.i][posAct.j]+1;
					Q.push(Pos(posAct.i+1, posAct.j));
					if(not enc[posAct.i+1][posAct.j]){
    					 enc[posAct.i+1][posAct.j] = true;
    					 if(cell_type(Pos(posAct.i+1, posAct.j)) == Stone){ Stones[sto_i] = Pos(posAct.i+1, posAct.j);++sto_i;}
    					 else if(cell_type(Pos(posAct.i+1, posAct.j)) == Point){ Points[poi_i] = Pos(posAct.i+1, posAct.j);++poi_i;}
    					 else if(cell_type(Pos(posAct.i+1, posAct.j)) == Attack) {Attacks[att_i] = Pos(posAct.i+1, posAct.j);++att_i;}
    					 else if(cell_type(Pos(posAct.i+1, posAct.j)) == Defense) {Defenses[def_i] = Pos(posAct.i+1, posAct.j);++def_i;}
    					 else if(cell_type(Pos(posAct.i+1, posAct.j)) == Scope) {Scopes[sco_i] = Pos(posAct.i+1, posAct.j);++sco_i;}
				    }
				}	
			}
			else{ //Will pokemon appeared !
				if(not enc[posAct.i+1][posAct.j]){
    					 enc[posAct.i+1][posAct.j] = true;
					 Pokemons[pok_i] = Pos(posAct.i+1, posAct.j);
					 ++pok_i;	
				}
			}
		}
	}
	
      
	if(p.alive){
		if(round() <= nb_rounds()*0.3){
		//Estrategia coger puntos
			//Primera pasada dist < 4
			if(poi_i > 0 or att_i > 0 or sto_i > 0 or def_i > 0 ){ //Hay monedas yupi
                                Pos ir;
				Pos ir1 = Points[0];
                                Pos ir2 = Attacks[0];
                                Pos ir3 = Stones[0];
                                Pos ir4 = Defenses[0];
				int dist_ir1 = Distances[ir1.i][ir1.j];
                                int dist_ir2 = Distances[ir2.i][ir2.j];
                                int dist_ir3 = Distances[ir3.i][ir3.j];
                                int dist_ir4 = Distances[ir4.i][ir4.j];

                                //int dist_total;
                                Pos aux1, aux2,aux3,aux4;
                                
				for(int i = 1; i < poi_i; i++){
                                    cerr << "hago el for" << endl;
					aux1 = Points[i];
                                        if(Distances[aux1.i][aux1.j] < dist_ir1) ir1=aux1;
                                }
                                for(int i = 1; i < att_i; i++){
                                        aux2 = Attacks[i];
                                        if(Distances[aux2.i][aux2.j] < dist_ir2) ir2=aux2;
                                }
                                for(int i = 1; i < sto_i; i++){
                                        aux3 = Stones[i];
                                        if(Distances[aux3.i][aux3.j] < dist_ir3) ir3=aux3;
                                }
                                for(int i = 1; i < def_i; i++){
                                        aux4 = Defenses[i];
                                        if(Distances[aux4.i][aux4.j] < dist_ir4) ir4 = aux4;
                                }
                                if(Distances[ir1.i][ir1.j]>Distances[ir2.i][ir2.j]){
                                    ir=ir2;
                                }
                                else {
                                      cerr << "hago el if" << endl;
                                    ir=ir1;
                                }
                                if (Distances[ir.i][ir.j]>Distances[ir3.i][ir3.j]){
                                    ir=ir3;
                                }
                                if (Distances[ir.i][ir.j]>Distances[ir4.i][ir4.j]) {
                                    ir=ir4;
                                }
				
				cerr << "aqui llego " << endl;
				while(true){
					if(Path[ir.i][ir.j] == p.pos){
						move(num2dir(ir.i-p.pos.i, ir.j-p.pos.j));
                                                cerr << "ejecutor el break" << endl;
						break;
					}
					ir = Path[ir.i][ir.j];
			
                                }
                            }
                        }
                        
			else{
                            if(pok_i>0){
                                cerr << " BATALLA POKEMON " << endl;
				Pos ir = Pokemons[0];
                                int dist_ir = Distances[ir.i][ir.j];
                                for(int i=1; i<pok_i;i++){
                                    Pos aux=Pokemons[i];
                                    if(Distances[aux.i][aux.j]<dist_ir) ir = aux;
                                }
                                if(Distances[ir.i][ir.j]>=p.scope){
                                    while(true){
                                            //if(Path[ir.i][ir.j] == p.pos){
                                                    cerr << " ATACOOOOOOO " << endl;
                                                    attack(num2dir(ir.i-p.pos.i, ir.j-p.pos.j));
                                                    cerr << "ejecutor el break" << endl;
                                                    break;
                                            //}
                                            ir = Path[ir.i][ir.j];
                            
                                    }
                                }
                                
                            }
                            else{
                                if(poi_i > 0 or att_i > 0 or sto_i > 0 or def_i > 0 ){ //Hay monedas yupi
                                Pos ir;
				Pos ir1 = Points[0];
                                Pos ir2 = Attacks[0];
                                Pos ir3 = Stones[0];
                                Pos ir4 = Defenses[0];
				int dist_ir1 = Distances[ir1.i][ir1.j];
                                int dist_ir2 = Distances[ir2.i][ir2.j];
                                int dist_ir3 = Distances[ir3.i][ir3.j];
                                int dist_ir4 = Distances[ir4.i][ir4.j];

                                //int dist_total;
                                Pos aux1, aux2,aux3,aux4;
                                
				for(int i = 1; i < poi_i; i++){
                                    cerr << "hago el for" << endl;
					aux1 = Points[i];
                                        if(Distances[aux1.i][aux1.j] < dist_ir1) ir1=aux1;
                                }
                                for(int i = 1; i < att_i; i++){
                                        aux2 = Attacks[i];
                                        if(Distances[aux2.i][aux2.j] < dist_ir2) ir2=aux2;
                                }
                                for(int i = 1; i < sto_i; i++){
                                        aux3 = Stones[i];
                                        if(Distances[aux3.i][aux3.j] < dist_ir3) ir3=aux3;
                                }
                                for(int i = 1; i < def_i; i++){
                                        aux4 = Defenses[i];
                                        if(Distances[aux4.i][aux4.j] < dist_ir4) ir4 = aux4;
                                }
                                if(Distances[ir1.i][ir1.j]>Distances[ir2.i][ir2.j]){
                                    ir=ir2;
                                }
                                else {
                                      cerr << "hago el if" << endl;
                                    ir=ir1;
                                }
                                if (Distances[ir.i][ir.j]>Distances[ir3.i][ir3.j]){
                                    ir=ir3;
                                }
                                if (Distances[ir.i][ir.j]>Distances[ir4.i][ir4.j]) {
                                    ir=ir4;
                                }
				
				cerr << "aqui llego " << endl;
				while(true){
					if(Path[ir.i][ir.j] == p.pos){
						move(num2dir(ir.i-p.pos.i, ir.j-p.pos.j));
                                                cerr << "ejecutor el break" << endl;
						break;
					}
					ir = Path[ir.i][ir.j];
			
                                }
                                
                                
                            }
                                
                        }
		}
        }

    }
    };

//No tocar
RegisterPlayer(PLAYER_NAME);
