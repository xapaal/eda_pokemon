#include "Player.hh"
#include <math.h> 
using namespace std;


/**
 * Write the name of your player and save this file
 * with the same name and .cc extension.
 */
#define PLAYER_NAME MarzaiseCopy2

typedef int celda;
typedef vector<celda> fila;
typedef vector<fila> matriz;

typedef pair<Pos, int> P;

int incx4[4] = {0, 0, 1, -1};
int incy4[4] = {1, -1, 0, 0};



struct PLAYER_NAME : public Player {


	  /**
	   * Factory: returns a new instance of this class.
	   * Do not modify this function.
	   */
	  static Player* factory () {
		return new PLAYER_NAME;
	  }


	  /**
	   * Attributes for your player can be defined here.
	   */


	  /**
	   * Play method.
	   *
	   * This method will be invoked once per each round.
	   * You have to read the board here to place your actions
	   * for this round.
	   *
	   */
	  virtual void play () {
		// initialization

		const Poquemon& p = poquemon(me());
		Pos pIni = p.pos;

		if (p.alive) {
			/*
			Dir d = rand_dir(None);
			int dist = search(p, d);
			if (dist <= p.scope) attack(d);
			else move(d);
			*/


			
			int dir = search_item(p);
			if (dir>=0)//>0->ok, move there		-1 -> don't know where to move (rare)	-2 -> I'm attacking
				move(num2dir(dir));
			else if (dir == -1)
				move(rand_dir(None));

		}
		else{
			PRINT("DIED :(");
		}


	}


	Dir rand_dir (Dir notd) {
	  Dir DIRS[] = {Top, Bottom, Left, Right};
	  while (true) {
		Dir d = DIRS[randomize() % 4];
		if (d != notd) return d;
	  }
	}
/*
	int search(const Poquemon& p, Dir d) {
		int dist = 1;
		Pos pIni = p.pos;
		while (dist <= p.scope) {
			pIni = dest(pIni, d);
			if (cell_type(pIni) == Wall) return p.scope+1;
			int idCell = cell_id(pIni);
			if (idCell != -1) {
				Poquemon p1 = poquemon(idCell);
				if (p1.defense <= p.attack) return dist;
			}
			++dist;
		}
		return p.scope+1;
	}
*/


	int search_item(const Poquemon& p){

		Dir DIRS[] = {Top, Bottom, Left, Right};

		matriz dist(cols(), fila(rows(), -1));
		Pos pIni = p.pos;
		queue<P> Q;
		P ins = P(pIni, -1);
  		Q.push(ins);
  		dist[pIni.i][pIni.j] = 0;
  		int my_number = cell_id(pIni);

  		while(!Q.empty()){
  			P aux_pair = Q.front(); Q.pop(); 
  			Pos aux = aux_pair.first;

  			int idCell = cell_id(aux);

			if ((idCell==0 || idCell==1 || idCell == 2 || idCell == 3) && idCell != my_number){
				PRINT("======ENEMY HERE!!=========");
				PRINT("id cell "+i2s(idCell));
				PRINT(" "+i2s(aux.j)+", "+i2s(aux.i));

				Poquemon p1 = poquemon(idCell);
				PRINT("attack: "+i2s(p1.attack));
				PRINT("defense: "+i2s(p1.defense));
				PRINT("scope: "+i2s(p1.scope));
				PRINT("=========================");

/*
				if (p1.defense<=p.attack)	//if we are able to attack
					return attack_pokemon(p, p1, aux_pair.second);
				else						//else... hide with mom!
					return InvertedDirection(aux_pair.second);
					*/
			}
  			
  			/*
  			//PRINT("DEFENSE: "+i2s(p1.defense));
  			if (idCell != -1) {
				Poquemon p1 = poquemon(idCell);
				Pos pEnemy = p1.pos;
				if (p1.defense <= p.attack){
					PRINT("===============================");
					PRINT("ATTACK of the pokemon: "+i2s(p1.attack));
					PRINT("DEFENSE of the pokemon: "+i2s(p1.defense));
					PRINT("SCOPE of the pokemon: "+i2s(p1.scope));

					PRINT("X of the pokemon: "+i2s(pEnemy.i));
					PRINT("Y of the pokemon: "+i2s(pEnemy.j));
					}	
			}
			*/

  			if (cell_type(aux) == Point){ print_obj(1, aux); return aux_pair.second;}
  			if (cell_type(aux) == Stone) { print_obj(2, aux); return aux_pair.second;}
  			if (cell_type(aux) == Scope) { print_obj(3, aux); return aux_pair.second;}
  			if (cell_type(aux) == Attack) { print_obj(4, aux); return aux_pair.second;}
  			if (cell_type(aux) == Defense) { print_obj(5, aux); return aux_pair.second;}

  			for (int d = 0; d<4; ++d){//0->right	1->Left		2->Down		3->Up

  				int x = aux.i+incx4[d];
  				int y = aux.j+incy4[d];

  				if (x>=0 && x<rows() && y>=0 && y<cols() && dist[x][y] == -1 && cell_type(aux) != Wall){
  					
  					Pos pos = Pos(x, y);
  					P insert = P(pos, -1);
  					if (aux_pair.second==-1)
  						insert.second = d;
  					else
  						insert.second = aux_pair.second;

  					Q.push(insert);
  					dist[x][y] = dist[aux.i][aux.j] + 1;
  				}
  			}
  		}
		return -1;
	}

	void print_obj(int i, Pos aux){
		return;//comment this for 
		PRINT("type: "+i2s(cell_id(aux)));

		switch(i){
			case 0:
				PRINT("<><><><>POINT<><><><>"); break;
			case 1:
				PRINT("<><><><>STONE<><><><>");	break;
			case 3:
				PRINT("<><><><>SCOPE<><><><>");	break;
			case 4:
				PRINT("<><><><>ATTACK<><><><>"); break;
			case 5:
				PRINT("<><><><>DEFENS<><><><>"); break;
		}

		PRINT(" "+i2s(aux.j)+", "+i2s(aux.i));
		PRINT("<><><><><><><><><><>");
	}

	int attack_pokemon(const Poquemon& me, const Poquemon& enemy, int direction){
		PRINT("inside attack_pokemon");
		PRINT("scope of mine: "+i2s(me.scope));
		PRINT("distance in x: "+i2s(abs(me.pos.i-enemy.pos.i)));
		PRINT("pos x of mine: "+i2s(me.pos.i));
		PRINT("pos x of enemy: "+i2s(enemy.pos.i));

		PRINT("distance in x: "+i2s(abs(me.pos.j-enemy.pos.j)));
		PRINT("pos y of mine: "+i2s(me.pos.j));
		PRINT("pos y of enemy: "+i2s(enemy.pos.j));


		if ((me.scope >= abs(me.pos.j-enemy.pos.j) && enemy.pos.i == me.pos.i)//if we are in the same direction
			|| (me.scope >= abs(me.pos.i-enemy.pos.i) && enemy.pos.j == me.pos.j)){//and have enought scope...
				attack(num2dir(direction));
				PRINT("=====ATACKING!!!!!=====")
				return -2;
		}else
			return direction;
	}

	int InvertedDirection(int num){
		switch(num){
			case 0: return 1;
			case 1: return 0;
			case 2: return 3;
			case 3: return 2;
		}
		return -1;
	}


	Dir num2dir(int num){
		switch(num){
			case 0: return Right;
			case 1: return Left;
			case 2: return Bottom;
			case 3: return Top;

		}
		return Right;
	}

	int dir2num(Dir d) {
		switch(d) {
			case Top: return 0;
			case Bottom: return 1;
			case Left: return 2;
			case Right: return 3;
			case None: return -1;
		}
		return -1;
	}

};

/**
 * Do not modify the following line.
 */
RegisterPlayer(PLAYER_NAME);