Game

Seed 1

Poquemon v1

nb_players 4
nb_poquemon 1
nb_rounds 400
nb_ghost_wall 6
nb_point 20
nb_stone 5
nb_scope 2
nb_attack 4
nb_defense 4

player_regen_time 40
wall_change_time 30
point_regen_time 50
stone_regen_time 85
scope_regen_time 65
attack_regen_time 55
defense_regen_time 55
battle_reward 15

max_scope 8
max_stone 2

rows 15
cols 31

names Demo Marzaise Marzaise Null


round 0

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  6  1  1  1  0  0  0  a
1  1  2  24  1  1  1  0  0  0  a
2  2  12  6  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 m r
2 m r
3 u n


round 1

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  1  1  1  0  0  0  a
1  1  2  25  1  1  1  0  0  0  a
2  2  12  7  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 m r
2 m r
3 u n


round 2

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  6  1  1  1  0  0  0  a
1  1  2  26  1  1  1  0  0  0  a
2  2  12  8  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 m r
2 m r
3 u n


round 3

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 4

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 5

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 6

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 7

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 8

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 9

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 10

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 11

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 12

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 13

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 14

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 15

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 16

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 17

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 18

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 19

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 20

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 21

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 22

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 23

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 24

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 25

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 26

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 27

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 28

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 29

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 30

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 31

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 32

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.MP.XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  0  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 33

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  49 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 34

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  48 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 35

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  47 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 36

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  46 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 37

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  45 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 38

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  44 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 39

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  43 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 40

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  8  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  42 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 41

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  41 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 42

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  40 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 43

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  39 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 44

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  38 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 45

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  37 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 46

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  36 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 47

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  35 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 48

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  34 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 49

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  33 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 50

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  32 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 51

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  31 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 52

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  30 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 53

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  29 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 54

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  28 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 55

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  27 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 56

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  26 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 57

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  25 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 58

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  24 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 59

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  23 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 60

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  22 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 61

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  21 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 62

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  20 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 63

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  19 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 64

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  18 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 65

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  17 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 66

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  16 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 67

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  15 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 68

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  14 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 69

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  13 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 70

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  12 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 71

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  11 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 72

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  10 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 73

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  9 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 74

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  8 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 75

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  7 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 76

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  6 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 77

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  5 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 78

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  4 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 79

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  3 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 80

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  2 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 81

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......P.......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    7  3  0  1 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 82

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 83

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 84

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 85

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 86

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 87

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 88

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 89

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 90

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 91

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 92

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 93

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 94

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 95

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 96

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 97

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 98

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 99

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 100

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 101

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 102

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 103

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 104

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 105

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 106

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 107

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 108

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 109

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 110

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 111

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 112

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 113

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  7  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 114

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 115

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  7  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 116

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 117

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 118

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 119

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 120

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 121

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 122

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 123

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 124

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 125

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 126

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 127

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 128

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 129

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 130

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 131

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 132

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 133

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 134

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXXPXXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  1  1  1  0  300  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  100  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 135

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  8  1  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  49 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 136

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  8  1  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  48 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 137

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  1  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  47 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 138

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  1  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  46 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 139

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  1  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  45 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 140

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD...AX.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  1  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  44 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 141

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  43 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 54 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 142

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  42 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 53 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 143

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  41 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 52 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 144

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  40 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 51 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 145

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  39 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 50 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 146

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  38 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 49 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 147

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  37 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 48 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 148

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  36 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 47 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 149

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  35 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 46 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 150

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  34 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 45 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 151

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  33 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 44 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 152

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  32 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 43 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 153

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  31 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 42 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 154

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  30 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 41 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 155

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  29 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 40 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 156

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  28 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 39 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 157

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  27 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 38 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 158

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  7  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  26 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 37 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 159

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  25 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 36 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 160

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  24 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 35 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 161

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  23 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 34 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 162

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  22 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 33 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 163

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  21 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 32 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 164

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  20 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 31 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 165

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  19 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 30 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 166

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  18 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 29 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 167

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  17 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 28 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 168

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  16 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 27 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 169

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  15 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 26 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 170

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  14 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 25 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 171

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  13 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 24 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 172

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  12 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 23 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 173

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  11 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 22 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 174

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  10 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 21 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 175

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  9 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 20 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 176

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  8 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 19 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 177

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  7 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 18 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 178

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  6 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 17 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 179

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  6  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  5 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 16 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 180

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  4 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 15 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 181

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  3 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 14 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 182

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  2 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 13 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 183

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........................XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    5  8  0  1 0
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 12 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 184

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  5  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 11 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 185

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 10 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 186

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 9 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 187

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 8 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 188

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 7 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 189

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 6 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 190

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 5 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 191

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 4 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 192

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 3 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 193

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 2 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 194

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P...XXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    6  10 0 1 0
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 195

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 196

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 197

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 198

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 199

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 200

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 201

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 202

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 203

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 204

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 205

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 206

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 207

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 208

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 209

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 210

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 211

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 212

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 213

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 214

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 215

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 216

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 217

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 218

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 219

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 220

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 221

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 222

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 223

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 224

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 225

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 226

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 227

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 228

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 229

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 230

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 231

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 232

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 233

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 234

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 235

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 236

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 237

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 238

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 239

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 240

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 241

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 242

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 243

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 244

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 245

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 246

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 247

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 248

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 249

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 250

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 251

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 252

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 253

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 254

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 255

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 256

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 257

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 258

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 259

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 260

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 261

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 262

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  3  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 263

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 264

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  3  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 265

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 266

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 267

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 268

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 269

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 270

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 271

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 272

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 273

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 274

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 275

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 276

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 277

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  3  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 278

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 279

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  4  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 280

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 281

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  5  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 282

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 283

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 284

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 285

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 286

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 287

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 288

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 289

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 290

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 291

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 292

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 293

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 294

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 295

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 296

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 297

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 298

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 299

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 300

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 301

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 302

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 303

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 304

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 305

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 306

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 307

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 308

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 309

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 310

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 311

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 312

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 313

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 314

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 315

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 316

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 317

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 318

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 319

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 320

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 321

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 322

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 323

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 324

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 325

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 326

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 327

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 328

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 329

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 330

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 331

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 332

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 333

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 334

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 335

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 336

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 337

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 338

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 339

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 340

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 341

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 0
7  2  19 0
7  8  19 0
7  22  19 0
7  28  19 0
11  15  19 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 342

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 0
7  2  18 0
7  8  18 0
7  22  18 0
7  28  18 0
11  15  18 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 343

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 0
7  2  17 0
7  8  17 0
7  22  17 0
7  28  17 0
11  15  17 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 344

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 0
7  2  16 0
7  8  16 0
7  22  16 0
7  28  16 0
11  15  16 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 345

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP.......PP......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  400  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 0
7  2  15 0
7  8  15 0
7  22  15 0
7  28  15 0
11  15  15 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  200  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 346

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 0
7  2  14 0
7  8  14 0
7  22  14 0
7  28  14 0
11  15  14 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  49 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 347

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 0
7  2  13 0
7  8  13 0
7  22  13 0
7  28  13 0
11  15  13 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  48 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 348

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 0
7  2  12 0
7  8  12 0
7  22  12 0
7  28  12 0
11  15  12 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  47 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 349

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 0
7  2  11 0
7  8  11 0
7  22  11 0
7  28  11 0
11  15  11 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  46 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 350

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 0
7  2  10 0
7  8  10 0
7  22  10 0
7  28  10 0
11  15  10 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  45 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 351

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 0
7  2  9 0
7  8  9 0
7  22  9 0
7  28  9 0
11  15  9 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  44 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 352

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 0
7  2  8 0
7  8  8 0
7  22  8 0
7  28  8 0
11  15  8 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  43 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 353

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 0
7  2  7 0
7  8  7 0
7  22  7 0
7  28  7 0
11  15  7 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  42 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 354

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 0
7  2  6 0
7  8  6 0
7  22  6 0
7  28  6 0
11  15  6 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  41 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 355

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP........P......P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  600  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 0
7  2  5 0
7  8  5 0
7  22  5 0
7  28  5 0
11  15  5 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  40 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  300  0 1
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 356

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 0
7  2  4 0
7  8  4 0
7  22  4 0
7  28  4 0
11  15  4 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  39 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  49 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 357

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  13  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 0
7  2  3 0
7  8  3 0
7  22  3 0
7  28  3 0
11  15  3 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  38 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  48 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 358

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  13  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 0
7  2  2 0
7  8  2 0
7  22  2 0
7  28  2 0
11  15  2 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  37 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  47 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 359

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 0
7  2  1 0
7  8  1 0
7  22  1 0
7  28  1 0
11  15  1 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  36 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  46 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 360

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 1
7  2  30 1
7  8  30 1
7  22  30 1
7  28  30 1
11  15  30 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  35 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  45 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 361

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  13  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 1
7  2  29 1
7  8  29 1
7  22  29 1
7  28  29 1
11  15  29 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  34 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  44 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 362

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 1
7  2  28 1
7  8  28 1
7  22  28 1
7  28  28 1
11  15  28 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  33 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  43 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 363

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 1
7  2  27 1
7  8  27 1
7  22  27 1
7  28  27 1
11  15  27 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  32 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  42 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 364

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 1
7  2  26 1
7  8  26 1
7  22  26 1
7  28  26 1
11  15  26 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  31 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  41 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 365

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  13  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 1
7  2  25 1
7  8  25 1
7  22  25 1
7  28  25 1
11  15  25 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  30 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  40 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 366

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  13  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 1
7  2  24 1
7  8  24 1
7  22  24 1
7  28  24 1
11  15  24 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  29 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  39 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 367

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 1
7  2  23 1
7  8  23 1
7  22  23 1
7  28  23 1
11  15  23 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  28 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  38 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 368

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 1
7  2  22 1
7  8  22 1
7  22  22 1
7  28  22 1
11  15  22 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  27 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  37 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 369

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  13  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 1
7  2  21 1
7  8  21 1
7  22  21 1
7  28  21 1
11  15  21 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  26 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  36 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 370

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  13  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 1
7  2  20 1
7  8  20 1
7  22  20 1
7  28  20 1
11  15  20 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  25 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  35 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 371

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  19 1
7  2  19 1
7  8  19 1
7  22  19 1
7  28  19 1
11  15  19 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  24 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  34 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 372

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  18 1
7  2  18 1
7  8  18 1
7  22  18 1
7  28  18 1
11  15  18 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  23 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  33 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 373

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  17 1
7  2  17 1
7  8  17 1
7  22  17 1
7  28  17 1
11  15  17 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  22 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  32 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 374

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  16 1
7  2  16 1
7  8  16 1
7  22  16 1
7  28  16 1
11  15  16 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  21 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  31 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 375

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  12  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  15 1
7  2  15 1
7  8  15 1
7  22  15 1
7  28  15 1
11  15  15 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  20 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  30 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 376

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  11  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  14 1
7  2  14 1
7  8  14 1
7  22  14 1
7  28  14 1
11  15  14 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  19 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  29 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 377

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  10  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  13 1
7  2  13 1
7  8  13 1
7  22  13 1
7  28  13 1
11  15  13 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  18 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  28 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 378

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  12 1
7  2  12 1
7  8  12 1
7  22  12 1
7  28  12 1
11  15  12 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  17 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  27 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 379

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  9  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  11 1
7  2  11 1
7  8  11 1
7  22  11 1
7  28  11 1
11  15  11 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  16 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  26 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 380

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  9  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  10 1
7  2  10 1
7  8  10 1
7  22  10 1
7  28  10 1
11  15  10 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  15 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  25 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 381

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  8  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  9 1
7  2  9 1
7  8  9 1
7  22  9 1
7  28  9 1
11  15  9 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  14 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  24 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 382

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  7  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  8 1
7  2  8 1
7  8  8 1
7  22  8 1
7  28  8 1
11  15  8 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  13 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  23 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 383

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  7  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  7 1
7  2  7 1
7  8  7 1
7  22  7 1
7  28  7 1
11  15  7 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  12 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  22 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 384

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  6  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  6 1
7  2  6 1
7  8  6 1
7  22  6 1
7  28  6 1
11  15  6 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  11 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  21 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 385

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  6  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  5 1
7  2  5 1
7  8  5 1
7  22  5 1
7  28  5 1
11  15  5 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  10 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  20 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 u n
1 u n
2 u n
3 u n


round 386

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  1  6  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  4 1
7  2  4 1
7  8  4 1
7  22  4 1
7  28  4 1
11  15  4 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  9 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  19 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 387

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  6  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  3 1
7  2  3 1
7  8  3 1
7  22  3 1
7  28  3 1
11  15  3 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  8 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  18 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 388

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  2 1
7  2  2 1
7  8  2 1
7  22  2 1
7  28  2 1
11  15  2 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  7 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  17 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 389

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  1 1
7  2  1 1
7  8  1 1
7  22  1 1
7  28  1 1
11  15  1 1

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  6 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  16 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 390

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  30 0
7  2  30 0
7  8  30 0
7  22  30 0
7  28  30 0
11  15  30 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  5 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  15 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 391

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  29 0
7  2  29 0
7  8  29 0
7  22  29 0
7  28  29 0
11  15  29 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  4 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  14 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 392

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  4  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  28 0
7  2  28 0
7  8  28 0
7  22  28 0
7  28  28 0
11  15  28 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  3 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  13 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 393

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  27 0
7  2  27 0
7  8  27 0
7  22  27 0
7  28  27 0
11  15  27 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  2 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  12 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m t
1 m r
2 m r
3 u n

actions_done
player action direction
0 m t
1 u n
2 u n
3 u n


round 394

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.........................X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  26 0
7  2  26 0
7  8  26 0
7  22  26 0
7  28  26 0
11  15  26 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    1  11  0  1 0
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  11 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 395

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  2  6  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  25 0
7  2  25 0
7  8  25 0
7  22  25 0
7  28  25 0
11  15  25 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    4  16  400  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  10 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m b
1 m r
2 m r
3 u n

actions_done
player action direction
0 m b
1 u n
2 u n
3 u n


round 396

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  6  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  24 0
7  2  24 0
7  8  24 0
7  22  24 0
7  28  24 0
11  15  24 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    4  16  400  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  9 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 397

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  23 0
7  2  23 0
7  8  23 0
7  22  23 0
7  28  23 0
11  15  23 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    4  16  400  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  8 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 398

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  22 0
7  2  22 0
7  8  22 0
7  22  22 0
7  28  22 0
11  15  22 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    4  16  400  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  7 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m r
1 m r
2 m r
3 u n

actions_done
player action direction
0 m r
1 u n
2 u n
3 u n


round 399

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  5  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  21 0
7  2  21 0
7  8  21 0
7  22  21 0
7  28  21 0
11  15  21 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    4  16  400  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  6 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

actions_asked
player action direction
0 m l
1 m r
2 m r
3 u n

actions_done
player action direction
0 m l
1 u n
2 u n
3 u n


round 400

board 

   0000000000111111111122222222223
   0123456789012345678901234567890

00 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
01 XSXP...............P.......PXSX
02 X.X.......XXXX.R.XXXX.......X.X
03 X.X.......X..XXMXX..X.......X.X
04 XPX.............P...........X.X
05 X.X..XXX.XXX...P..AXXXPXXX..X.X
06 X.X..XD....X.X.X.X.XA...DX..X.X
07 X.M..XXXMXXXPX.S.XPXXXMXXX.PM.X
08 X.X..XA...DX.X.X.X.XD...AX..X.X
09 X.X..XXXPXXX...P...XXXPXXX..X.X
10 X.X.........P...............XPX
11 X.X.......X..XXMXX..X.......X.X
12 X.X.......XXXX.R.XXXX.......X.X
13 XSXP.......P.......P.......PXSX
14 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

status 0.000 0.000 0.000 0.000

poquemon
id  ply  i  j  at  def  ab  ped  pts  time  alive
0  0  3  4  2  1  1  0  900  0  a
1  1  2  27  1  1  1  0  0  0  a
2  2  12  9  1  1  1  0  0  0  a
3  3  12  24  1  1  1  0  0  0  a

walls
i  j  time  present
3  15  20 0
7  2  20 0
7  8  20 0
7  22  20 0
7  28  20 0
11  15  20 0

bonus
type  i  j  pts  time present
P    1  3  500  0 1
P    4  16  400  0 1
P    1  19  200  0 1
P    1  27  500  0 1
P    4  1  500  0 1
P    10  12  500  0 1
P    5  15  300  0 1
P    5  22  100  0 1
P    1  12  0  5 0
P    7  12  400  0 1
P    7  18  400  0 1
P    7  27  300  0 1
P    9  8  100  0 1
P    9  15  300  0 1
P    9  22  100  0 1
P    10  29  500  0 1
P    13  3  500  0 1
P    13  11  200  0 1
P    13  19  200  0 1
P    13  27  500  0 1
S    1  1 0 0 1
S    1  29 0 0 1
S    7  15 0 0 1
S    13  1 0 0 1
S    13  29 0 0 1
R    2  15 0 0 1
R    12  15 0 0 1
A    5  18 0 0 1
A    6  20 0 0 1
A    8  6 0 0 1
A    8  24 0 0 1
D    6  6 0 0 1
D    6  24 0 0 1
D    8  10 0 0 1
D    8  20 0 0 1

